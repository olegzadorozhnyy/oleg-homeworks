
$(document).ready(function() {
    //////////////////////////////////////////////////////////// Services

    $(".services").on("click", ".tabs-title",function() {
        $(this).addClass("active").siblings().removeClass("active");
        $(this).closest(".services").find(".content-tab").removeClass("active").eq($(this).index()).addClass("active");
    });

    //////////////////////////////////////////////////////////// Our work
    $(".item-work").removeClass("active").slice(0, 12).addClass("active");
    $(".our-work").on("click", "#load", function(e) {
        e.preventDefault();
        showItems();
        checkLoadButton();
    });

    $(".our-work").on("click", ".tab-work-title", function() {
        $(this).addClass("active").siblings().removeClass("active");
        $(".item-work").removeClass("active");
        showItems();
        checkLoadButton();
    });
    $(".our-work").on("mouseenter", ".item-work", function () {
        const text = $(this).data("info");
        $(this).prepend(`
            <div class="item-work-hover">
              <div class="item-work-hover-btns">
                <a class="hover-btn1"></a>
                <a class="hover-btn2"></a>
              </div>
              <p class="item-work-hover-title">Creative design</p>
              <p class="item-work-hover-text">${text}</p>
            </div>`);
    })
    $(".our-work").on("mouseleave", ".item-work", function () {
        $(this).find(".item-work-hover").remove();
    })

    $(".feedback-bottom").on("click", ".carousel-tabs", function(e) {
        e.preventDefault();
        $(this).addClass("active").siblings().removeClass("active");
        const slideLenght = $(this).index() * 1140;
        $(".slides").animate({left : -slideLenght});
    });

    $(".carousel-arrow").on("click", function (e) {
        e.preventDefault();
        let index = $(".carousel-tabs.active").index();
        if($(this).is(".left")) {
            index--;
        }
        if($(this).is(".right")) {
            index++
        }

        if( index === $(".carousel-tabs").length) {
            index=0;
        }
        if (index < 0) {
            index = $(".carousel-tabs").length - 1
        }

        $(".carousel-tabs").eq(index).addClass("active").siblings().removeClass("active");

        const slideLenght = index * 1140;
        $(".slides").animate({left : -slideLenght});

    });

    $("#gallery-load").on("click", function (e) {
        e.preventDefault();
        $(this).hide();
        $(".loader").show();
        setTimeout(showGallery, 2000);

    });

    $('.grid').masonry({
        // options
        itemSelector: '.grid-item',
        columnWidth: '.grid-item',
        percentPosition: true
    });
    $('.grid2').masonry({
        // options
        itemSelector: '.grid-item',
        columnWidth: '.two-column',
        percentPosition: true
    });

    function showGallery() {
        $(".loader").hide();
        const elems = [ getItemElement(), getItemElement(), getItemElement() , getItemElement(), getItemElement() , getItemElement(), getItemElement() ];
        const items = $( elems );
        $('.grid').append( items ).masonry( 'appended', items );
    }

    function showItems() {
        const selector = $(".tab-work-title.active").data("target");
        $(`.item-work${selector}`).not(".active").slice(0, 12).addClass("active");
    }
    function checkLoadButton() {
        const selector = $(".tab-work-title.active").data("target");
        if ($(`.item-work${selector}`).not(".active").length === 0) {
            $("#load").hide();
        }
        else {
            $("#load").show();
        }
    }
    function getItemElement() {
        const elem = document.createElement("div");
        elem.classList.add("grid-item");
        let number = getRandomInt(1,9);
        elem.innerHTML = `<img alt=\"\" src=\"images/gallery/img${number}.jpg\">`;
        return elem;
    }

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min; //Максимум не включается, минимум включается
    }


});