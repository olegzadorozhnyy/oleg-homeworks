$("a.navbar-item-link").click(function() {
    $('html, body').animate({
        scrollTop: $(`#${$(this).data("target")}`).offset().top
    }, 1000);
});

$("#toggle-example").click(function () {
    $(".posts").slideToggle();
});

const height = $(window).height();

$(window).scroll(function(){
    if ($(window).scrollTop() >= height)
    {
        $("#goTop").show();
    }
    if ($(window).scrollTop() <= height) {
        $("#goTop").hide();
    }
});

$("#goTop").click(function() {
    $('html, body').animate({scrollTop:0}, 1000);

});
