/*
1. Спросить доход за день.
2. Конвертируем полученные данные в число.
3. Спросить месяц
4. Опредилить количество рабочих дней в месяце
5. Посчитать зарплату (умножив доход за день на кол. раб. дней)
6. Вывести данные на экран


 */

let salaryPerDay = Number(prompt('Введите зарплату за один день!'));
console.log(salaryPerDay);
let count = 0;
while((isNaN(salaryPerDay) || salaryPerDay == 0) && count < 3)  {
    salaryPerDay = Number(prompt('Введите зарплату за один день!'));
    count++;
}

let month = prompt('Введите месяц');

month = month.toLowerCase();
let salaryPerMonth = 0;

count = 0;
while (!isNaN(month)) {
    month = prompt('Введите месяц');
    count++;
    if(count == 3) {
        alert("Ну и не надо!");
        break;
    }
}

switch (month){
    case 'январь':
    case 'март':
    case 'май':
    case 'июль':
    case 'август':
    case 'октябрь':
    case 'декабрь':
        salaryPerMonth = 23 * salaryPerDay;
        alert(salaryPerMonth);
        break;
    case 'февраль':
        salaryPerMonth = 20 * salaryPerDay;
        alert(salaryPerMonth);
        break;
    case 'апрель':
    case 'июнь':
    case 'сентябрь':
    case 'ноябрь':
        salaryPerMonth = 22 * salaryPerDay;
        alert(salaryPerMonth);
        break;
    default:
        month = prompt('Введите месяц корректно!');
}
