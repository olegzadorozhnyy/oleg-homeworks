const user = 'admin';

switch (user) {
  case 'admin': {
    console.log('Welcome, admin!');
    break;
  }

  case 'simple': {
    console.log('Welcome!');
    break;
  }

  default: {
    console.log('Welcome!');
  }
}
