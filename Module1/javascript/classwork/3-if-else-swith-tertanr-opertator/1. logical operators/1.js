/**
 * Логический оператор «ИЛИ» возвращает первую правду.
 */

console.log(true || true); // true
console.log(false || true); // true
console.log(true || false); // true
console.log(false || false); // false
console.log(false || false || true || false); // true
