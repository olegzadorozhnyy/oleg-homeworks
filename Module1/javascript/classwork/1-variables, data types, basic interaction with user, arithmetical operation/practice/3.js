/**
 * ЗАДАНИЕ 3
 *
 * Записать в переменную '123', вывести в консоль typeof этой переменной.
 * Преобразовать эту переменную в численный тип при помощи parseInt(), parseFloat(), унарный плюс +
 * После этого повторно вывести в консоль typeof этой переменной.
 */

let strNumber = '123';
console.log('typeof BEFORE transformation -> ', typeof strNumber);

strNumber = parseInt(strNumber);
console.log('typeof AFTER parseInt-> ', typeof strNumber);

strNumber = parseFloat(strNumber);
console.log('typeof AFTER parseFloat -> ', typeof strNumber);

strNumber = +strNumber;
console.log('typeof AFTER + -> ', typeof strNumber);
