/**
 * NaN чаще всего является результат ошибочного математического выражения,
 * с ним мы разберёмся по-подробней чуть позже.
 */
const error = NaN;
/* null — это семантическое значение для отсутствия значения (не путать с undefined!!!). */
const nullValue = null;
const notDefinedValue = undefined;

console.log(error);
console.log(nullValue);
console.log(notDefinedValue);
