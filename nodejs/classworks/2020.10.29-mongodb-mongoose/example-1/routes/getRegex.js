const regex = require("../utils/regex");

const getRegex = (app) => {
    app.get("/regex", (req, res) => {
        res.send({
            status: "Success",
            result: regex
        })
    })
}