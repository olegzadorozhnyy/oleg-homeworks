const Author = require("../../models/Author");

const getAllAuthors = (app) => {
    app.get("/authors", (req, res)=> {
       Author.find({}, (err, result) => {
            // console.log(err)
            // console.log(result)
            if(!err) {
                res.send({
                    status: "Success",
                    result
                });
            }
            else {
                res.send({
                    status: "Error",
                    message: err
                });                
            }

        })
    });
};

module.exports = getAllAuthors;
