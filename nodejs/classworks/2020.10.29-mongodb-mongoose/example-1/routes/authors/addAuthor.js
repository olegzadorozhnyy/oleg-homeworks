const Author = require("../../models/Author");

const addAuthor = (app) => {
    app.post("/authors", async (req, res)=> {

        const newAuthor = new Author(req.body);

        try {
            const result = await newAuthor.save();
            res.send({
                status: "Success",
                result
            });
        } catch (err) {
            res.send({
                status: "Error",
                message: err
            });
        }
    });
};

module.exports = addAuthor;
