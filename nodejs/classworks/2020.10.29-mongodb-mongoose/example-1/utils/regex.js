const regex = {
    isbn: {
        reg: /^(?:ISBN(?:-10)?:?\ )?(?=[0-9X]{10}$|(?=(?:[0-9]+[-\ ]){3})[-\ 0-9X]{13}$)[0-9]{1,5}[-\ ]?[0-9]+[-\ ]?[0-9]+[-\ ]?[0-9X]$/,
        errorMessage: "ISBN valid format is ISBN-236-0-1234-555"
    }
};

module.exports = regex;