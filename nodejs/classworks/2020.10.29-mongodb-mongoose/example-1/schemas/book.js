const {Schema} = require("mongoose");
const {isbn} = require("../utils/regex");

const bookSchema = new Schema({
    name: {
        required: [true, 'Name is required'],
        type: String,
        minlength: 2
    },
    isbn: {
        required: true,
        type: String,
        validate: {
            validator: (value)=> {
                return isbn.reg.test(value);
            },
            message: props => isbn.errorMessage
        },
        required: [true, 'User phone number required']
    }
});

module.exports = bookSchema;