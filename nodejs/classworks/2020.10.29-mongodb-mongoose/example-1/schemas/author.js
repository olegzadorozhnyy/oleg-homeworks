const {Schema} = require("mongoose");

const authorSchema = new Schema({
    name: {
        required: true,
        type: String,
        minlength: 2
    },
    age: {
        required: true,
        type: Number,
        min: 1,
        max: 130
    },
    status: {
        required: true,
        type: String,
        enum: ["REGULAR", "FREELANCE"]
    },
    email: {
        required: true,
        type: String,
        unique: true
    },
    createAt: {
        required: false,
        type: String,
        default: Date.now
    }
});

module.exports = authorSchema;