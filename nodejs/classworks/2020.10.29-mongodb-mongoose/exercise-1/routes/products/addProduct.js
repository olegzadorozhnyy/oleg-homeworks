const Product = require("../../models/Product");

const addProduct = (app) => {
    app.post("/products", async (req, res)=> {
        const newProduct = new Product(req.body);     
        try {
            const result = await newProduct.save()
            res.send({
                    status: "Success",
                    result
                       })
        }  catch (err) {
                    res.send({
                        status: "Error",
                        message: err.message
                    })
        }
    });
};

module.exports = addProduct;