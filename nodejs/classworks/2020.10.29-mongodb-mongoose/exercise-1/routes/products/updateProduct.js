const Product = require("../../models/Product");

const updateProduct = (app) => {

    app.put("/products/:id", async (req, res)=> {
        const {id} = req.params;
        const {body} = req;
        try {
            const result = await Product.findByIdAndUpdate(id, body, {
                new: true
              })
            res.send({
                    status: "Success",
                    result
                       })
        }  catch (err) {
                    res.send({
                        status: "Error",
                        message: err.message
                    })
        }
        })
    };


module.exports = updateProduct;