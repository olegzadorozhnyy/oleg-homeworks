const Product = require("../../models/Product");

const getAllProducts = (app) => {

    app.get("/products", async (req, res)=> {
        console.log(req.query);
        try {
            const result = await Product.find(req.query)
            res.send({
                    status: "Success",
                    result
                       })
        }  catch (err) {
                    res.send({
                        status: "Error",
                        message: err.message
                    })
        }
        })
    };


module.exports = getAllProducts;