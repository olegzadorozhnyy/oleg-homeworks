const Product = require("../../models/Product");

const getProductById = (app) => {

    app.get("/products/:id", async (req, res)=> {
        const {id} = req.params;
        try {
            const result = await Product.findById(id)
            res.send({
                    status: "Success",
                    result
                       })
        }  catch (err) {
                    res.send({
                        status: "Error",
                        message: err.message
                    })
        }
        })
    };


module.exports = getProductById;