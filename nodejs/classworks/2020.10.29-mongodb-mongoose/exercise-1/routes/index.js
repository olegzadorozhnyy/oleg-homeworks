const getAllProducts = require('./products/getAllProduts');
const addProduct = require('./products/addProduct');
const getProductById = require('./products/getProductById');
const updateProduct = require('./products/updateProduct');
const deleteProduct = require('./products/deleteProduct');

const routes = (app) => {
    getAllProducts(app);
    addProduct(app);
    getProductById(app);
    updateProduct(app);
    deleteProduct(app);
};

module.exports = routes;