const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
const routes = require("./routes")

const dbUrl = require("./config")


mongoose.connect(dbUrl,{ useNewUrlParser: true });

mongoose.connection.once('open', (error) => {
    console.log('Connect to MongoDB success');

    const app = express();
    app.use(cors());
    app.use(bodyParser.json());
    routes(app);

    const PORT = process.env.PORT || 5000;
    app.listen(PORT, () => {
        console.log(`Оно живо! PORT=${PORT}`)
    });

});
