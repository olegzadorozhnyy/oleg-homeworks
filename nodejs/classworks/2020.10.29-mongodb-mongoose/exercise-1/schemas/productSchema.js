const { Schema } = require('mongoose');

module.exports = new Schema({
    name: {
        type: String,
        required: true,
        minlength: 3,
        unique: true
    },
    status: {
        type: String,
        enum: ['ACTIVE', "IN_STOCK", "OUT_OF_STOCK"],
        default: 'ACTIVE'
    },
    category: {
        type: String,
        required: [true, 'Category is required!'],
    },
    price: {
        type: Number,
        min: 0,
        required: [true, 'Price is required!']

    },
    brand: {
        type: String,
        required: [true, 'Brand is required!']
    },
    createdAt: {
        required: false,
        default: Date.now,
        type: Date
    }
});