const authorsRoutes = require("./authors");

module.exports = (app) => {
    authorsRoutes(app);
}