const getAllAuthors = require("./getAllAuthors");
const addAuthor = require("./addAuthor");

module.exports = (app)=> {
    getAllAuthors(app);
    addAuthor(app);
};