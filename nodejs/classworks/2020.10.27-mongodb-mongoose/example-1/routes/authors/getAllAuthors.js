const Author = require("../../models/Author");

const getAllAuthors = (app) => {
    app.get("/authors", (req, res)=> {
        Author.find({}, (err, result) => {
            console.log(err)
            console.log(result)
            res.send({
                status: "Success",
                result
            });
        })

    });
};

module.exports = getAllAuthors;
