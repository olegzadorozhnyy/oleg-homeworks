const {Schema} = require('mongoose');

module.exports = new Schema({
    name: {
        type: String,
        required : true,
        minlength: 3,
        unique: true
    },
    category: String,
    price: Number,
    brand: String,

})