const getAllProducts = require('./products/getAllProducts');
const addNewProduct = require("./products/addNewProduct")

module.exports = (app) => {
    getAllProducts(app);
    addNewProduct(app);
}