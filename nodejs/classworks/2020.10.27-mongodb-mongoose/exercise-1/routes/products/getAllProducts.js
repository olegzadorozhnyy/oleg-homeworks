const Product = require("../../models/Product");

const getAllProducts = (app) => {
    app.get("/products", (req, res)=> {
        Product.find({}, (err, result) => {
            if(!err) {
                res.send({
                    status: "Success",
                    result
                });
            }
            else {
                res.send({
                    status: "Error",
                    message: err
                });
            }
        })
    });
};

module.exports = getAllProducts;