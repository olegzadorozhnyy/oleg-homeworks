const Product = require("../../models/Product");

const addNewProduct = (app) => {
    app.post("/products", (req, res)=> {
        const newProduct = new Product(req.body);
        const answer = newProduct.save();
        answer.then((result) => {
            res.send({
                status: "Success",
                result
            })
        }).catch((err) => {
            console.log(err);
            res.send({
                status : "Error",
                message : err
            })
        })
    });
};

module.exports = addNewProduct;