const express = require("express");
const bodyParser = require("body-parser")

const app = express();
app.use(bodyParser.json());

// const routes = require("./routes");
/*
routes = (app)=> {
    getOneUser(app);
}
*/
// routes(app);
app.get("/", (req, res)=> {
    res.send("<h1>Home Page</h1>");
});

app.get("/contacts", (req, res)=> {
    res.send({
        title: "Contacts page",
        description: "Our contacts page"
    });
});

app.post("/products", (req, res)=> {
    console.log(req.query)
})

app.post("/products/:id", (req, res)=> {
    console.log(req.params.id)
})

app.listen(3000, ()=> console.log("Оно работает!"));