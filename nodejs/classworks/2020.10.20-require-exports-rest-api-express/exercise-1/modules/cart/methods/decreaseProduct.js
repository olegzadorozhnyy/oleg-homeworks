function decreaseProduct(product) {
    const presentProduct = this.products.find(({id}) => product.id === id);
    presentProduct ? this.product.count++ : this.products.push({...product, count: 1})
}

module.export = decreaseProduct;