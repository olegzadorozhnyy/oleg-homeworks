const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')

const products = require('./products')

const app = express()
app.use(cors())
app.use(bodyParser.json())

const routers = require("./routers");
routers(app);


app.listen(3000, ()=>{console.log('alive')})