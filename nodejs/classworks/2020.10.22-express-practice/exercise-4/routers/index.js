const createProduct = require("./createProduct");
const getProducts = require("./getProducts");
const getProductsById = require("./getProductsById");

module.exports = (app) => {
    createProduct(app);
    getProducts(app);
    getProductsById(app);
};