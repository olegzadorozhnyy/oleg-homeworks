const express = require('express')
const cors = require('cors')
const bodyParser = require("body-parser")

const server = express()
const products = require('./products')

server.use(cors())
server.use(bodyParser.json())

server.get('/products', (req, res) => {
    res.send(products)
});

server.listen(3000, () => console.log('server is working'))
