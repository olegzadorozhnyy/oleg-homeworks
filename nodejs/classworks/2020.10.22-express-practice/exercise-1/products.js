const products = [
    {
        id: "34f",
        name: "Lenovo X120S",
        brand: "Lenovo",
        price: 21000
    },
    {
        id: "g5f",
        name: "Acer Swift 5",
        brand: "Acer",
        price: 33000
    },
    {
        id: "22f",
        name: "Acer Swift 3",
        brand: "Acer",
        price: 37000
    },
];

module.exports = products