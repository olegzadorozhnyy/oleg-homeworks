const {model, Schema} = require("mongoose");

const BookSchema = new Schema({
    title: String,
    price: Number
});

const Book = model("book", BookSchema);

Book.find()