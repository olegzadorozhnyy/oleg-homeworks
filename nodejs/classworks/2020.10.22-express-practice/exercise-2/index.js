const express = require('express');
const cors = require('cors');
const bodyParser = require("body-parser");

const server = express();
const products = require('./products');

server.use(cors());
server.use(bodyParser.json());

server.get('/products', (req, res) => {
    res.send(products)
});

server.get('/users', (req, res) => {
    res.sendStatus(301)
})

server.listen(3000, () => console.log('server is working'));
