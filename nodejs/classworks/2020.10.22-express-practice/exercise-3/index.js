const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')

const products = require('./products')

const app = express()
app.use(cors())
app.use(bodyParser.json())

app.get('/products', (req, res)=>{
    const {length} = Object.keys(req.query)
    if (!length) {
        
    }
    res.send(
        {
            status: "Success",
            result: products
        }
    )
})

// {
//     ID: "gfyy1"
// }
// fetch("/products/gfyy1")

app.get('/products/:id', (req, res)=>{
    const product = products.find(({id}) => id === req.params.id)
    console.log(req.params.id)
    if (product) {
        res.send(
            {
                status: "Success",
                result: product
            }  
        )
    } else {
        res.send(
            {
                status: "Error",
                message: "no product found!"
            }  
        )
    }

})


app.post('/products', (req, res)=>{
    const length = products.push(req.body)
    res.send(
        {
            status: "Success",
            result: {...req.body, id: length-1}
        }
    )
})

app.listen(3000, ()=>{console.log('alive')})