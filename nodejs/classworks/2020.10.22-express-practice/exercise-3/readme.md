### Задание

Напишите такие роуты:

1. "/products" - GET-запрос возвращает ответ вида:
{
    status: "Success",
    result: products
}
массив products есть в файле index.js.

2. "/products" - POST-запрос принимает объект вида:
{
    name: product_name,
    brand: product_brand,
    price: product_price
}

И возвращает объект вида:
 {
     status: "Success",
     result: {
                id: product_id,
                 name: product_name,
                 brand: product_brand,
                 price: product_price
             }
 }
3. "/products/:id" - GET-запрос возвращает ответ вида:
{
    status: "Success",
    result: product_with_id
}
4. "/products?brand=brand_name" - GET-запрос возвращает ответ вида:
{
    status: "Success",
    result: product_with_brand=brand_name
}

5. "/products/:id" - PUT-запрос принимает объект вида:
{
    name: product_name,
    brand: product_brand,
    price: product_price
}

И возвращает объект вида:
 {
     status: "Success",
     result: {
                id: product_id,
                 name: product_name,
                 brand: product_brand,
                 price: product_price
             }
 }