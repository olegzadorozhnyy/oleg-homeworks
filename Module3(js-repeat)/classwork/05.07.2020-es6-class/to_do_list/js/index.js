class ToDoList {
    constructor(listTitle, caseList) {
        this.listTitle = listTitle;
        this.caseList = caseList.map(item => ({...item}));
    }

    searchPanel = `<div class="search-panel d-flex"></div>`;
    toDoList = `<ul class="todo-list list-group"></ul>`;

    addItemFormProps = {
        className: "bottom-panel d-flex"
    };

    render() {
        const toDo = document.createElement("div");
        toDo.className = "todo-app";
        toDo.insertAdjacentHTML("beforeend", `${this.getHeader()}${this.searchPanel}${this.toDoList}`);

        const itemsList = this.caseList.map(item => new ToDoItem(item).render());
        this.toDoList = toDo.querySelector('.todo-list');
        this.toDoList.append(...itemsList);

        this.addItemFormProps.action = this.addItem.bind(this);
        this.addItemForm = new AddItemForm(this.addItemFormProps);
        toDo.append(this.addItemForm.render());

        return toDo;
    }

    addItem(e) {
        e.preventDefault();
        const {text} = JSON.parse(this.addItemForm.serializeJSON());
        const itemProps = {text};
        const item = new ToDoItem(itemProps);
        this.toDoList.append(item.render())
    }

    getHeader() {
        const description = new ToDoDescription(this.caseList);
        return `<div class="app-header d-flex">
                    <h1>${this.listTitle}</h1>
                    ${description.render()}
                </div>`;
    }
}

class Form {
    constructor (props) {
        this.props = {...props}
        this.elem = null
    }
    render() {
        const {action, ...attr} = this.props;
        const form = createElement("form", attr);
        form.addEventListener("submit", action);
        this.elem = form;
        return this.elem;
    }

    serializeJSON() {
        const namedFields = [...this.elem.querySelectorAll("[name]")];
        // 2-й вариант:
        // const namedField = elem.querySelectorAll("[name]");
        // const existingNameAttributeValue = [].filter.call(namedField, (item) => item);
        const existingNameAttrValue = namedFields.filter(({value}) => value);
        const result = {};
        existingNameAttrValue.forEach(({name, value}) => result[name] = value);
        return JSON.stringify(result);
    }
}

class AddItemForm extends Form {

    addInputProps = {
        type:"text",
        className:"form-control new-todo-label",
        placeholder:"Введите дело",
        value:"",
        name: "text"
    };

    addButtonProps = {
        type:"submit",
        className: "btn btn-outline-secondary",
        text: "Добавить"
    };

    render() {
        const form = super.render();
        const searchInput = new Input(this.addInputProps);
        const submitButton = new Button(this.addButtonProps);
        form.append(searchInput.render(), submitButton.render());
        return form;
    }
}

function createElement(tag, attr, content = "") {
    const element = document.createElement(tag);
    for(const [key, value] of Object.entries(attr)){
        if(value){
            element[key] = value;
        }
    }
    element.innerHTML = content;
    return element;
}

class Input {
    constructor(props){
        this.props = {...props};
        this.elem = null;
    }
    render() {
        this.elem = createElement("input", this.props);
        return this.elem;
    }
}

class Button {
    constructor(props){
        this.props = {...props};
        this.elem = null;
    }
    render() {
        const {text, ...attr} = this.props;
        this.elem = createElement("button", attr, text);
        return this.elem;
    }
}

class ToDoItem {
    constructor(item){
        this.item = {...item};
        this.elem = null;
    }

    isDone = ({target})=> {
        target.closest('.todo-list-item').classList.toggle('done');
    };

    delete = ()=> {
        this.elem.remove();
    };

    success = ({target})=> {
        target.closest('.todo-list-item').classList.toggle('important');
    };

    render(){
        this.elem = document.createElement('li');
        this.elem.className = 'list-group-item';
        this.elem.innerHTML = `
            <span class="todo-list-item ${this.item.done ? 'done' : ''} ${this.item.important ? 'important' : ''}">
                <span class="todo-list-item-label">${this.item.text}</span>
                <button type="button" class="btn btn-outline-success btn-sm float-right">
                    <i class="fa fa-exclamation"></i>
                </button>
                <button type="button" class="btn btn-outline-danger btn-sm float-right">
                    <i class="fa fa-trash-o"></i>
                </button>
            </span>`;

        this.elem.querySelector('.todo-list-item-label').addEventListener('click', this.isDone);
        this.elem.querySelector('.btn-outline-success').addEventListener('click', this.success);
        this.elem.querySelector('.btn-outline-danger').addEventListener('click', this.delete);

        return this.elem;
    }
}

class ToDoDescription {
    constructor (props) {
        this.props = props;
        this.elem = null;
    }

    contentObject = {
        toDo: 0,
        important: 0,
        done: 0
    };

    getContent(){
        this.contentObject.toDo = this.props.length;
        this.contentObject.important = this.props.filter(({important}) => important ).length;
        this.contentObject.done = this.props.filter(({done}) => done ).length;
        const {toDo, important, done} = this.contentObject;

        return `${toDo} дела, ${important} важное, ${done} сделано`;
    }

    render () {
        this.elem = `<h2>${this.getContent()}</h2>`;
        return this.elem;
    }
}

const simpleCase = {
    text: "Изучить JS",
    done: false,
    important: false
};

const importantCase = {
    text: 'Сходить на охоту',
    important: true,
    done: false
};

const doneCase = {
    text: "Создать HTML-разметку To Do List",
    important: false,
    done: true
};

const currentCaseList = [simpleCase, importantCase, doneCase];
const list = new ToDoList('Список дел', currentCaseList);
document.getElementById('root').append(list.render());



