/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
class Hamburger {
    constructor(size, stuffing) {
        if(size === undefined) {
            throw this.hamburgerException("No size given");
        }
        if(stuffing === undefined) {
            throw this.hamburgerException("No stuffing given");
        }
        if(!(size.name === "LARGE" || size.name === "SMALL")) {
            throw this.hamburgerException(`You handed ${size.name} instead size`);
        }
        this.size = size;
        this.stuffing = stuffing;
        this.toppings = [];
    }
    addTopping (topping) {
        if (!this.toppings.includes(topping)) {
            this.toppings.push(topping);
        }
        else {
            throw this.hamburgerException(`Duplicate topping ${topping.name}`);
        }
    }
    removeTopping (topping) {
        this.toppings = this.toppings.filter(function (item) {
            if (item.name !== topping.name) {
                return item;
            }
        });
    }
    getToppings () {
        return this.toppings;
    }
    getSize () {
        return this.size.name;
    }
    getStuffing () {
        return this.stuffing.name;
    }
    calculatePrice () {
        let cost = this.size.cost;

        cost += this.stuffing.cost;
        this.toppings.forEach(item => cost += item.cost);

        return cost;
    }
    calculateCalories () {
        let calories = this.size.calories;
        calories += this.stuffing.calories;
        this.toppings.forEach(item => calories += item.calories);

        return calories;
    }
    hamburgerException (message) {
        this.name = "HamburgerException";
        this.message = message;
        return this;
    }

}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {cost:50, calories:20, name:"SMALL"};
Hamburger.SIZE_LARGE = {cost:100, calories:40, name:"LARGE"};
Hamburger.STUFFING_CHEESE = {cost:10, calories:20, name:"CHEESE"};
Hamburger.STUFFING_SALAD = {cost:20, calories:5, name:"SALAD"};
Hamburger.STUFFING_POTATO = {cost:15, calories:10, name:"POTATO"};
Hamburger.TOPPING_MAYO = {cost:20, calories:5, name:"MAYO"};
Hamburger.TOPPING_SPICE = {cost:15, calories:0, name:"SPICE"};


/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */





// маленький гамбургер с начинкой из сыра

const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);

// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1



// не передали обязательные параметры
try{
    const h2 = new Hamburger();
}
catch (error) {
    console.log(`${error.name}: ${error.message}`);
}
// => HamburgerException: no size given

// передаем некорректные значения, добавку вместо размера
try {
    const h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
}
catch (error) {
    console.log(`${error.name}: ${error.message}`);
}
// => HamburgerException: invalid size 'TOPPING_SAUCE'

// добавляем много добавок
try {
    const h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
    h4.addTopping(Hamburger.TOPPING_MAYO);
    h4.addTopping(Hamburger.TOPPING_MAYO);
}
catch (error) {
    console.log(`${error.name}: ${error.message}`);
}

// HamburgerException: duplicate topping 'TOPPING_MAYO'

