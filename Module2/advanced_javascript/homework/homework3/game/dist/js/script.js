class Game {
    constructor() {
        this.complexity = [{size: 5, interval: 600},
            {size: 7, interval: 1000},
            {size: 10, interval: 500}];
        this.table = null;
        this.timer = null;
        this.playerScore = 0;
        this.AIScore = 0;
        this.minkCount = 0;
    }
    set level(value) {
        this._level = this.complexity[value];
    }
    get level() {
        return this._level;
    }
    initGame(wrap) {
        const div = document.createElement("div");
        div.className = "module";
        wrap.insertAdjacentElement("beforeend", div);

        div.innerHTML = `<button data-target="0" class="btn-level">Level 1</button><button data-target="1" class="btn-level">Level 2</button><button  data-target="2" class="btn-level">Level 3</button>`;
        div.addEventListener("click", (e) => {
            if(e.target.className === 'btn-level') {
                this.prepareGame(e.target.dataset.target);
                div.style.display = "none";
            }
        })
    }
    prepareGame(complexity) {
        const result = document.getElementById("result");
        if(result) {
            result.remove();
        }
        this.level = complexity;
        const wrap = document.getElementById("table-wrap");
        wrap.insertAdjacentElement("afterbegin", this.render());
        setTimeout(() => {
            this.startGame();
        }, 1000);
    }

    render () {
        this.table = document.createElement("table");
        this.table.className = "game-table";
        this.table.innerHTML = this.renderTableInner();
        return this.table;
    }

    renderTableInner() {
        const tr = [];
        const td = [];
        for (let i=0; i< this.level.size; i++){
            td.push('<td class="clean"></td>');
        }
        const row = td.join('');
        for(let i=0; i < this.level.size; i++){
            tr.push(`<tr>${row}</tr>`);
        }
        return tr.join('');
    }


    startGame() {
        this.table.style.opacity = 1;
        const timeInterval = this.level.interval;
        this.timer = setInterval(()=> {
            this.moleAttack();
        }, this.level.interval);
    }
    moleAttack() {
        let currentMole = document.querySelector(".mole");

        if (currentMole) {
            currentMole.parentElement.className = "lose";
            currentMole.remove();
            this.AIScore++;
        }
        if (this.minkCount > this.level.size**2/2) {
            clearInterval(this.timer);
            setTimeout(() => {
                const wrap = document.getElementById("table-wrap");
                const result = document.createElement("div");
                result.id = "result"
                result.innerHTML = `<p>Player score: ${this.playerScore}</p>
                                    <p>Player score: ${this.AIScore}</p>`;
                wrap.innerHTML = result.outerHTML;
                this.minkCount = 0;
                this.playerScore = 0;
                this.AIScore = 0;
                this.initGame(wrap);
            }, this.level.interval);
        }
        else {
            const emptyMinks = this.table.querySelectorAll(".clean");
            const randomMink = this.getRandomInt(emptyMinks.length);
            const currentMink = emptyMinks[randomMink];

            const mole = document.createElement("div");
            mole.className = "mole";
            currentMink.insertAdjacentElement("afterbegin", mole);
            mole.addEventListener("click", () => {
                currentMink.className = "win";
                mole.remove();
                this.playerScore++;
            });
            this.minkCount++;
        }
    }

    getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }
}


const game = new Game();
const wrap = document.getElementById("table-wrap");
game.initGame(wrap);
