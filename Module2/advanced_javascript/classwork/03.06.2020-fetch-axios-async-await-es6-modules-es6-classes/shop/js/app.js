
const loginForm = document.getElementById('login-form');
loginForm.addEventListener('submit', loginUser);

async function loginUser(event){
    event.preventDefault();
    const body = {
        email: this.querySelector('[name="user-email"]').value,
        password: this.querySelector('[name="user-password"]').value
    }

    const {data} = await axios.post('http://shop.danit.com.ua/login', body);

    if (data.status === 'Success') {
        localStorage.setItem('token', data.token);
    } else {
        this.insertAdjacentHTML('beforeEnd', `<p>${data.message}</p>`)  
    }

    // const request = axios.post('http://shop.danit.com.ua/login', body);

    // request.then(({data}) => {
    //     if (data.status === 'Success') {
    //         localStorage.setItem('token', data.token);
    //     } else {
    //         this.insertAdjacentHTML('beforeEnd', `<p>${data.message}</p>`)
    
    //     }
    // });

}


/*
const productSelection = document.getElementById('productCatecory');
productSelection.addEventListener('change', showAdditionalFields);

async function showAdditionalFields(event){
    event.preventDefault();
    
}*/
