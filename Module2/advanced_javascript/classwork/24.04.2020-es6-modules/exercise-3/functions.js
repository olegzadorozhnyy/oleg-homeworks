export function getByClass(className) {
    const element = [...document.getElementsByClassName(className)];
    return element;
}

export function getById(id) {
    return document.getElementById(id);
}

export function queryAll(selector) {
    return [...document.querySelectorAll(selector)];
}