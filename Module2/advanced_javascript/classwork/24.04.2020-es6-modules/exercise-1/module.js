export class Input {
    constructor({type, classList,id}) {
        this.type = type;
        this.classList = [...classList];
        this.id = id;
        this.elem;
    }
    render() {
        this.elem = document.createElement(this.type);
        this.elem.id = this.id;
        this.elem.className = this.classList.join(" ");
        this.elem.innerText = "Hello there";
        return this.elem;
    }
}