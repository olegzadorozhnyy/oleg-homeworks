const gulp = require('gulp');
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css')
const cleanAll = require('gulp-clean');

const css = () => gulp.src('./src/css/**/*.css').
    pipe(concat('style.min.css')).
    pipe(cleanCSS()).
    pipe(gulp.dest('./dist/css'));

const watch  = () => {
    gulp.watch('./src/css/**/*.css', css);
}

const clean = () => gulp.src('./dist', {read: false})
    .pipe(cleanAll());

gulp.task('css', css);
gulp.task("dev", gulp.series('css', watch));
gulp.task('clean', clean);