const gulp = require('gulp');
const moveCss = function(){
    return gulp.src(`./src/css/**/*.css`).
        pipe(gulp.dest(`./dist/css`))
};

const moveJs = function(){
    return gulp.src(`./src/scripts/**/*.js`).
        pipe(gulp.dest(`./dist/js`))
};

const moveImages = function(){

    return gulp.src(`./src/image/**/*.{jpg,png}`).
        pipe(gulp.dest(`./dist/image`))
};

gulp.task(`moveCss`, moveCss);
gulp.task(`moveJs`, moveJs);
gulp.task(`moveImages`, moveImages);
gulp.task(`build`, gulp.parallel(moveCss, moveJs, moveImages));

const watch = function () {
   gulp.watch(`./src/css/**/*.css` , moveCss);
    gulp.watch(`./src/image/**/*.{jpg,png}`, moveImages);
    gulp.watch(`./src/js/*.js`, moveJs)

};
gulp.task(`dev`, gulp.series(`build` , watch));