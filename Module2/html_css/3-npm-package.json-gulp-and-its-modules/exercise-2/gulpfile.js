const gulp = require('gulp');

const moveScripts = () =>
    gulp.src('./src/scripts/**.js').
        pipe(gulp.dest('./dist/js/'));

gulp.task('moveScripts', moveScripts)


const moveImg = () => gulp.src('./src/img/**/*.{png,jpg}').
    pipe(gulp.dest('./dist/img'));

gulp.task('moveImg', moveImg);

gulp.task("movefiles", gulp.series("moveScripts", "moveImg"));
