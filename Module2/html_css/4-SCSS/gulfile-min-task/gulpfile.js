const gulp = require("gulp");
const scss = require("gulp-sass");
const cleanCSS = require("gulp-clean-css");
const clean = require('gulp-clean');

const patch = {
        css: "./src/scss/**/*.scss",
        js: "./src/js/**/*.js"
}

const style = () =>
    gulp.src(patch.css).
        pipe(scss()).
        pipe(cleanCSS()).
        pipe(gulp.dest("./dist/css"));

const watch = () => {
        gulp.watch(patch.css, style);
}

function cleandev() {										//модуль отчистки папки перед каждой расспаковкой
    return gulp.src('./dist', {read: false})
        .pipe(clean())
}

gulp.task("style", style);

gulp.task("build", gulp.series("cleandev", gulp.parallel(style)));

gulp.task("dev", gulp.series("build", watch));


