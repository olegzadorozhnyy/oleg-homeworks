import React, {useContext} from "react";
import {context} from "../../context/context";


const CartItem = (props) => {
    const {title, price, count} = props;
    const {state, actions} = useContext(context);
    const {createIncrementProductAction, createDecrementProductAction, createClearProductAction} = actions;
    const {currency} = state;
    const {name, rate} = currency;


    const increment = () => createIncrementProductAction(props);
    const decrement = () => createDecrementProductAction(props);
    const clearProduct = () => createClearProductAction(props);

    return (
            <table className="show-cart table">
                <tr>
                    <td>{title}</td>
                    <td>{price*rate} {name}</td>
                    <td>
                        <div className="input-group">
                            <button className="minus-item input-group-addon btn btn-primary" onClick={decrement}>-</button>
                            <input type="number" className="item-count form-control"  value={count} />
                            <button className="plus-item btn btn-primary input-group-addon" onClick={increment}>+</button>
                        </div>
                    </td>
                    <td>
                        <button className="delete-item btn btn-danger" onClick={clearProduct} data-name="Banana">X</button>
                    </td>
                </tr>



            </table>
    )
}
export default CartItem