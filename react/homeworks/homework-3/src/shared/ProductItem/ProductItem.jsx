import React, {useContext} from "react";
import {context} from "../../context/context";

const ProductItem = (props) => {
    const {title, price, id, imageSrc} = props;
    const {state, actions} = useContext(context);
    const {addToCartAction} = actions;
    const {currency} = state;
    const {name, rate} = currency;


    const addProduct = (event) => {
        event.preventDefault();
        addToCartAction(props);
    }
    return (

        <div className="col">
            <div className="card" >
                <img className="card-img-top" src={imageSrc}
                     alt="Card image cap" />
                    <div className="card-block">
                        <h4 className="card-title">{title}</h4>
                        <p className="card-text">{price*rate} {name}</p>
                        <p className="card-text">id: {id}</p>
                        <button onClick={addProduct} data-name="Orange" data-price="0.5" className="add-to-cart btn btn-primary">Add to
                            cart</button>
                    </div>
            </div>
        </div>
    )
}
export default ProductItem