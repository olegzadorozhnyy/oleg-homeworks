import React, {useContext} from 'react'
import ProductItem from "../../../shared/ProductItem";
import {context} from "../../../context/context";

const ProductList = ()=> {
    const {state} = useContext(context);
    const {products} = state;


    const productsElems = products.map(item => <ProductItem {...item} />)

    return (
        <>
            <h2>ProductList</h2>
            <div className="row">
                {productsElems}
            </div>
        </>
    )
}

export default ProductList;