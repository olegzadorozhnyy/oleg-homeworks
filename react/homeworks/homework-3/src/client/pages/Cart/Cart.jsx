import React, {useContext} from 'react';
import CartItem from "../../../shared/CartItem";
import {context} from "../../../context/context";

const Cart = ()=> {
    const {state} = useContext(context);
    const {cart, currency} = state;
    const {name, rate} = currency;


    const {cartProducts = [], totalPrice} = cart;
    const cartsElems = cartProducts.map(item => <CartItem {...item} />);

    return (
            <>
                <h2>Cart</h2>
                <p>total price: {totalPrice*rate} {name}</p>
                <div className="row">
                    {cartsElems}
                </div>
            </>
    )
}

export default Cart;