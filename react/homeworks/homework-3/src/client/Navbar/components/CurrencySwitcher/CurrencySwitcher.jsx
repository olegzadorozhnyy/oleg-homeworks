import React, {useContext} from "react";
import {rate} from "../../../../context/rate";
import {context} from "../../../../context/context";

const CurrencySwitcher = () => {
        const {state, actions} = useContext(context);
        const {currency} = state;
        const {createActionCurrencyChange} = actions;

        const currencyElems = rate.map(({name}) => <option selected={currency === name} value={name}>{name}</option>);
        const changeCurrency = ({target}) => {
            const currency = rate.find(({name}) => name === target.value);
            createActionCurrencyChange(currency);
        }
    return (
        <select onChange={changeCurrency}>{currencyElems}</select>
    )
}

export default CurrencySwitcher;