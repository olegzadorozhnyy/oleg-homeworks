import React, {useContext} from "react";
import {NavLink} from "react-router-dom";
import CurrencySwitcher from "../CurrencySwitcher";
import {context} from "../../../../context/context";

const Navbar = () => {
    const {state} = useContext(context);
    const {cart} = state;
    let cartCount = 0;
    cart.cartProducts.map(({count}) => {
        cartCount += count;
    });
    return(
        <div>
            <div>
                <ul className="navbar">
                    <li>
                        <NavLink to="/">Home</NavLink>
                    </li>
                    <li>
                        <NavLink to="/cart">Cart({cartCount})</NavLink>
                    </li>
                </ul>
            </div>
            <CurrencySwitcher />
        </div>

    )
}

export default Navbar