import React from 'react';
import './App.css';
import {BrowserRouter as Router} from "react-router-dom"
import AppRoutes from "./AppRoutes"
import Navbar from "./client/Navbar/components/Navbar/Navbar";
import {ContextProvider}  from "./context/Provider";

function App() {
  return (
    <div className="App">
          <ContextProvider>
            <Router>
                <div className="container">
                  <Navbar/>
                  <AppRoutes />
                </div>
            </Router>
          </ ContextProvider >
    </div>
  );
}

export default App;
