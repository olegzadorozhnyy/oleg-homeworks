import React from "react";
import {Route, Switch} from "react-router-dom"
import Error from "../client/pages/Error";
import Cart from "../client/pages/Cart";
import ProductList from "../client/pages/ProductList";

const AppRoutes = () => {
    return(
        <Switch>
            <Route exact path="/">
                <ProductList/>
            </Route>
            <Route exact path="/cart">
                <Cart/>
            </Route>
            <Route>
                <Error/>
            </Route>
        </Switch>
    )
}

export default AppRoutes