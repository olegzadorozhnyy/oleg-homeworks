export const initialContext = {
    cart: {
        totalPrice: 0,
        cartProducts: []
    },
    products: [
        {
            id: 1,
            title: "Orange",
            price: 15,
            imageSrc : "http://www.azspagirls.com/files/2010/09/orange.jpg"
        },
        {
            id: 2,
            title: "Banana",
            price: 23,
            imageSrc : "https://images.all-free-download.com/images/graphicthumb/vector_illustration_of_ripe_bananas_567893.jpg"
        },
        {
            id: 3,
            title: "Lemon",
            price: 31,
            imageSrc : "https://3.imimg.com/data3/IC/JO/MY-9839190/organic-lemon-250x250.jpg"
        }
    ],
    currency: {
        name: "UAH",
        rate: 1
    }
}