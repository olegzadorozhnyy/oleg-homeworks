import React, {useMemo, useReducer} from "react";
import {context} from "./context";
import {initialContext} from "./initialContext";
import {SET_CURRENCY, ADD_TO_CART, DECREMENT_PRODUCT, INCREMENT_PRODUCT, CLEAR_PRODUCT } from "./constants";
import {reducer} from "./reducer";
import PropTypes from "prop-types";

export const ContextProvider = ({children}) => {
    const [state, dispatch] = useReducer(reducer, initialContext);

    const actions = useMemo(() => ({
        createActionCurrencyChange : (payload) => {
            dispatch({type: SET_CURRENCY, payload});
        },
        createClearProductAction : (payload) => {
            dispatch({type: CLEAR_PRODUCT, payload});
        },
        addToCartAction : (payload) => {
            dispatch ({type: ADD_TO_CART, payload});
        },
        createDecrementProductAction : (payload) => {
            dispatch({type: DECREMENT_PRODUCT, payload});
        },
        createIncrementProductAction : (payload) => {
            dispatch({type: INCREMENT_PRODUCT, payload});
        }

    }), [])

    return(
        <context.Provider value={{state, actions }} >
            {children}
        </context.Provider>
    )
}

ContextProvider.propTypes = {
    children : PropTypes.element.isRequired
}


export default ContextProvider