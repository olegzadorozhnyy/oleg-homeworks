export const addProduct  = (payload, state) => {
    const totalPrice = state.cart.totalPrice + payload.price;
    const [...cartProducts] = state.cart.cartProducts;
    const product = cartProducts.find(({id}) => id === payload.id);
    if(product) {
        product.count++;
    }
    else {
        cartProducts.push({...payload, count : 1});
    }
    return {
        totalPrice,
        cartProducts
    };
}

export const increment = (payload, state) => {
    const totalPrice = state.cart.totalPrice + payload.price;
    const [...cartProducts] = state.cart.cartProducts;

    const newCart = cartProducts.map(item => {
        if(item.id === payload.id) {
            item.count++;
        }
        return item;
    });
    return {
        totalPrice,
        cartProducts: newCart
    };
};

export const decrement = (payload, state) => {
    let totalPrice = state.cart.totalPrice;
    const [...cartProducts] = state.cart.cartProducts;
    totalPrice -= payload.price;

    const cart = cartProducts.filter(item => {
        if(item.id === payload.id) {
            item.count -= 1;
        }
        return item.count > 0;
    });
    return {
        totalPrice,
        cartProducts: cart
    };
}

export const clearProduct = (payload, state) => {
    const [...cartProducts] = state.cart.cartProducts;
    const {price, id:prod_id, count} = cartProducts.find(({id}) => id === payload.id);
    const totalPrice = state.cart.totalPrice - (price * count);
    const cart = cartProducts.filter(({id}) => id !== prod_id);

    return {
        totalPrice,
        cartProducts: cart
    }
}