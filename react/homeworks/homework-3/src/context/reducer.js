import {ADD_TO_CART, DECREMENT_PRODUCT, INCREMENT_PRODUCT, SET_CURRENCY, CLEAR_PRODUCT} from "./constants";
import {increment, decrement, clearProduct, addProduct} from "./reducerFunctions/reducerFunctions";


export const reducer = (state, {type, payload}) => {

    switch (type) {
        case ADD_TO_CART:
            return {...state, cart: addProduct(payload, state)}
        case DECREMENT_PRODUCT:
            return {...state, cart: decrement(payload, state)};
        case INCREMENT_PRODUCT:
            return {...state, cart: increment(payload, state)};
        case CLEAR_PRODUCT:
            return {...state, cart: clearProduct(payload, state)};
        case SET_CURRENCY:
            return {...state, currency: payload}

        default:
            return state;
    }
}