import React from "react";
import {NavLink} from "react-router-dom";
import CurrencySwitcher from "../CurrencySwitcher"

const Navbar = () => {
    return(
        <div>
            <div>
                <ul className="navbar">
                    <li>
                        <NavLink to="/">Home</NavLink></li>
                    <li>
                        <NavLink to="/product-list">Product List</NavLink>
                    </li>
                    <li>
                        <NavLink to="/cart">Cart</NavLink>
                    </li>
                </ul>
            </div>
            <CurrencySwitcher />
        </div>

    )
}

export default Navbar