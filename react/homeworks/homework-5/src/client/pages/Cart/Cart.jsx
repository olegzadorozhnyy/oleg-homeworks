import React from 'react';
import {shallowEqual, useSelector} from "react-redux";
import CartItem from "../../../shared/CartItem";

const Cart = ()=> {
    const cart = useSelector(({cart})=> cart, shallowEqual);
    const rate = useSelector(({currency} )=> currency.rate);
    const {cartProducts = [], totalPrice} = cart;
    const cartsElems = cartProducts.map(item => <CartItem {...item} />);

    return (
            <>
                <h2>Cart</h2>
                <p>total price: {totalPrice*rate}</p>
                <ul>
                    {cartsElems}
                </ul>
            </>
    )
}

export default Cart;