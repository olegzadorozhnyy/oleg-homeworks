import React from 'react'
import {shallowEqual, useSelector} from "react-redux";
import ProductItem from "../../../shared/ProductItem";

const Product = ({ match}) => {
    const { params: { productId } } = match;
    const products = useSelector(({products}) => products, shallowEqual);
    const productElem = products.find(({id}) => id === +productId);

    return (
        <>
            <h2>Product</h2>
            <ProductItem {...productElem}/>
        </>
    )
}

export default Product;