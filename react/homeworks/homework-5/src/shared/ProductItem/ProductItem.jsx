import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {addToCartAction} from "../../store/createActions/createAddCart";
import {Link} from "react-router-dom";

const ProductItem = (props) => {
    const {title, price, id} = props;
    const rate = useSelector(({currency} )=> currency.rate);
    const dispatch = useDispatch();

    const onClick = (event) => {
        event.preventDefault();
        dispatch(addToCartAction(props));
    }
    return (
        <li>
            <div className="product-item">
                <Link to={`/product/${id}`} >{title}</Link>
                <p>id: {id}</p>
                <p>price: {price*rate}</p>
                <button onClick={onClick}>Add to cart</button>
            </div>
        </li>
    )
}
export default ProductItem