export const increment = (payload, state) => {

    const totalPrice = state.cart.totalPrice + payload.price;
    const cartProducts = state.cart.cartProducts || [];
    const productId = cartProducts.find(({id}) => id === payload.id);

    if (productId) {
        cartProducts.map(item => {
            if(item.id === payload.id) {
                item.count++;
            }
            return item;
        });
    }
    else {
        const count = 1;
        cartProducts.push({...payload, count});
    }
    return {
        totalPrice,
        cartProducts
    };
}
export const decrement = (payload, state) => {
    let totalPrice = state.cart.totalPrice;
    const cartProducts = state.cart.cartProducts;
    totalPrice -= payload.price;
    const cart = cartProducts.filter(item => {
        if(item.id === payload.id) {
            item.count -= 1;
        }
        return item.count > 0;
    });
    return {
        totalPrice,
        cartProducts: cart
    };
}