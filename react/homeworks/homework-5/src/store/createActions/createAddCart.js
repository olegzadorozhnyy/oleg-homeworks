import {ADD_TO_CART} from "../actions";

export const addToCartAction = (payload) => {
    return {
        type: ADD_TO_CART,
        payload
    }
}