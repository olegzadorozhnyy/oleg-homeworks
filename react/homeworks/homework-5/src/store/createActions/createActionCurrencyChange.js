import {CHANGE_CURRENCY} from "../actions";

export const createActionCurrencyChange = (payload) => {
    return {
        type: CHANGE_CURRENCY,
        payload
    }
}