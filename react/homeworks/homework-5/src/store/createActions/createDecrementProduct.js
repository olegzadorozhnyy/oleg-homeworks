import {DECREMENT_PRODUCT} from "../actions";

export const createDecrementProductAction = (payload) => {
    return {
        type: DECREMENT_PRODUCT,
        payload
    }
}