export const initialState = {
    cart: {
        totalPrice: 0,
        cartProducts: []
    },
    products: [
        {
            id: 1,
            title: "HP-Omen-15",
            price: 43319
        },
        {
            id: 2,
            title: "iPhone-8plus",
            price: 13500
        },
        {
            id: 3,
            title: "MacBook-2019",
            price: 83500
        },
        {
            id: 4,
            title: "Razer-hadder",
            price: 23000
        },
        {
            id: 5,
            title: "Lenovo-17",
            price: 28500
        },
    ],
    currency: {
        name: "UAH",
        rate: 1
    }
}