import React from 'react';
import './App.css';
import {BrowserRouter as Router} from "react-router-dom"
import AppRoutes from "./AppRoutes"
import Navbar from "./client/Navbar/components/Navbar/Navbar";
import {Provider} from "react-redux";
import {store} from "./store/store";

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <Router>
          <Navbar/>
          <AppRoutes />
        </Router>
      </Provider>
    </div>
  );
}

export default App;
