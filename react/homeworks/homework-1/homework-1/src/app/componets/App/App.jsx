import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../shared/styles/scss/style.scss';
import Example from "../../../client/example/components/Example";
import {BigImg} from "../../../shared/components/Images/BigImg"
import {SmallImg} from "../../../shared/components/Images/SmallImg"

export const App = () => {
    const pageProps = {
        movies : [
            {   slider: true,
                id : 342334321,
                bigImg: BigImg[0],
                smallImg :  SmallImg[0],
                descr : {
                    title: "The Jungle Book",
                    tags: [
                        "Fatasy", "Family", "Drama"
                    ],
                    duration : "1h 46m",
                    rating : 4.8,
                    isFavorite : false
                }
            },
            {   slider: true,
                id : 342334322,
                bigImg: BigImg[0],
                smallImg :  SmallImg[1],
                descr : {
                    title: "The Jungle Book",
                    tags: [
                        "Fatasy", "Family", "Drama"
                    ],
                    duration : "1h 46m",
                    rating : 4.8,
                    isFavorite : false
                }
            },
            {   slider: true,
                id : 342334323,
                bigImg: BigImg[0],
                smallImg :  SmallImg[4],
                descr : {
                    title: "The Jungle Book",
                    tags: [
                        "Fatasy", "Family", "Drama"
                    ],
                    duration : "1h 46m",
                    rating : 4.8,
                    isFavorite : false
                }
            },
            {   slider: false,
                id : 342334324,
                bigImg: BigImg[10],
                smallImg :  SmallImg[10],
                descr : {
                    title: "The Jungle Book",
                    tags: [
                        "Fatasy", "Family", "Drama"
                    ],
                    duration : "1h 46m",
                    rating : 4.8,
                    isFavorite : false
                }
            },
            {   slider: false,
                id : 342334325,
                bigImg: BigImg[9],
                smallImg :  SmallImg[9],
                descr : {
                    title: "The Jungle Book",
                    tags: [
                        "Fatasy", "Family", "Drama"
                    ],
                    duration : "1h 46m",
                    rating : 4.8,
                    isFavorite : false
                }
            },
            {   slider: false,
                id : 342334326,
                bigImg: BigImg[0],
                smallImg :  SmallImg[8],
                descr : {
                    title: "The Jungle Book",
                    tags: [
                        "Fatasy", "Family", "Drama"
                    ],
                    duration : "1h 46m",
                    rating : 4.8,
                    isFavorite : false
                }
            },
            {   slider: false,
                id : 342334324,
                bigImg: BigImg[0],
                smallImg :  SmallImg[7],
                descr : {
                    title: "The Jungle Book",
                    tags: [
                        "Fatasy", "Family", "Drama"
                    ],
                    duration : "1h 46m",
                    rating : 4.8,
                    isFavorite : false
                }
            },
            {   slider: false,
                id : 342334325,
                bigImg: BigImg[0],
                smallImg :  SmallImg[6],
                descr : {
                    title: "The Jungle Book",
                    tags: [
                        "Fatasy", "Family", "Drama"
                    ],
                    duration : "1h 46m",
                    rating : 4.8,
                    isFavorite : false
                }
            },
            {   slider: false,
                id : 342334326,
                bigImg: BigImg[0],
                smallImg :  SmallImg[5],
                descr : {
                    title: "The Jungle Book",
                    tags: [
                        "Fatasy", "Family", "Drama"
                    ],
                    duration : "1h 46m",
                    rating : 4.8,
                    isFavorite : false
                }
            },
            {   slider: false,
                id : 342334324,
                bigImg: BigImg[0],
                smallImg :  SmallImg[4],
                descr : {
                    title: "The Jungle Book",
                    tags: [
                        "Fatasy", "Family", "Drama"
                    ],
                    duration : "1h 46m",
                    rating : 4.8,
                    isFavorite : false
                }
            },
            {   slider: false,
                id : 342334325,
                bigImg: BigImg[0],
                smallImg :  SmallImg[3],
                descr : {
                    title: "The Jungle Book",
                    tags: [
                        "Fatasy", "Family", "Drama"
                    ],
                    duration : "1h 46m",
                    rating : 4.8,
                    isFavorite : false
                }
            },
            {   slider: false,
                id : 342334326,
                bigImg: BigImg[0],
                smallImg :  SmallImg[2],
                descr : {
                    title: "The Jungle Book",
                    tags: [
                        "Fatasy", "Family", "Drama"
                    ],
                    duration : "1h 46m",
                    rating : 4.8,
                    isFavorite : false
                }
            },
            {   slider: false,
                id : 342334324,
                bigImg: BigImg[0],
                smallImg :  SmallImg[1],
                descr : {
                    title: "The Jungle Book",
                    tags: [
                        "Fatasy", "Family", "Drama"
                    ],
                    duration : "1h 46m",
                    rating : 4.8,
                    isFavorite : false
                }
            },
            {   slider: false,
                id : 342334326,
                bigImg: BigImg[0],
                smallImg :  SmallImg[8],
                descr : {
                    title: "The Jungle Book",
                    tags: [
                        "Fatasy", "Family", "Drama"
                    ],
                    duration : "1h 46m",
                    rating : 4.8,
                    isFavorite : false
                }
            },
            {   slider: false,
                id : 342334324,
                bigImg: BigImg[0],
                smallImg :  SmallImg[7],
                descr : {
                    title: "The Jungle Book",
                    tags: [
                        "Fatasy", "Family", "Drama"
                    ],
                    duration : "1h 46m",
                    rating : 4.8,
                    isFavorite : false
                }
            },
            {   slider: false,
                id : 342334325,
                bigImg: BigImg[0],
                smallImg :  SmallImg[6],
                descr : {
                    title: "The Jungle Book",
                    tags: [
                        "Fatasy", "Family", "Drama"
                    ],
                    duration : "1h 46m",
                    rating : 4.8,
                    isFavorite : false
                }
            },
            {   slider: false,
                id : 342334326,
                bigImg: BigImg[0],
                smallImg :  SmallImg[5],
                descr : {
                    title: "The Jungle Book",
                    tags: [
                        "Fatasy", "Family", "Drama"
                    ],
                    duration : "1h 46m",
                    rating : 4.8,
                    isFavorite : false
                }
            },
            {   slider: false,
                id : 342334324,
                bigImg: BigImg[0],
                smallImg :  SmallImg[4],
                descr : {
                    title: "The Jungle Book",
                    tags: [
                        "Fatasy", "Family", "Drama"
                    ],
                    duration : "1h 46m",
                    rating : 4.8,
                    isFavorite : false
                }
            },
            {   slider: false,
                id : 342334325,
                bigImg: BigImg[0],
                smallImg :  SmallImg[3],
                descr : {
                    title: "The Jungle Book",
                    tags: [
                        "Fatasy", "Family", "Drama"
                    ],
                    duration : "1h 46m",
                    rating : 4.8,
                    isFavorite : false
                }
            },
            {   slider: false,
                id : 342334326,
                bigImg: BigImg[0],
                smallImg :  SmallImg[2],
                descr : {
                    title: "The Jungle Book",
                    tags: [
                        "Fatasy", "Family", "Drama"
                    ],
                    duration : "1h 46m",
                    rating : 4.8,
                    isFavorite : false
                }
            },
            {   slider: false,
                id : 342334324,
                bigImg: BigImg[0],
                smallImg :  SmallImg[1],
                descr : {
                    title: "The Jungle Book",
                    tags: [
                        "Fatasy", "Family", "Drama"
                    ],
                    duration : "1h 46m",
                    rating : 4.8,
                    isFavorite : false
                }
            },


        ],
        footer: {
            links : [
                {
                    name : "About",
                    link : "#"
                },
                {
                    name : "Terms of Service",
                    link : "#"
                },
                {
                    name : "Contact",
                    link : "#"
                },
            ],
            copy : "Copyright © 2017 MOVIERISE. All Rights Reserved.",
            socialLinks : [
                {
                    name: "instagram",
                    link : "#"
                }

            ]
        },
        category: [
            {
                categoryName : "Trending",
                subCategory : false
            },
            {
                categoryName : "Top Rated",
                subCategory : false
            },
            {
                categoryName : "New Arrivals",
                subCategory : false
            },
            {
                categoryName : "Trailers",
                subCategory : false
            },
            {
                categoryName : "Coming Soon",
                subCategory : false
            },
            {
                categoryName : "Genre",
                subCategory : true
            }
            ]
    }

    return (
        <Example {...pageProps}/>
    );
};
