import React from "react"
import "./Footer.css"
import logoSrc from "./logo2.png"
import socialSrc from "./soc.png"


const Footer = ({footer}) => {
    const {links, copy, socialLinks} = footer;
    const linksList = links.map(({name, link}) => (
        <a key={name} className="footer-links-item" href={link}>{name}</a>
        ));
    const socialLinksList = socialLinks.map(({name, link}) => (
        <a key={name} href={link}>
            <img alt={name} src={socialSrc}/>
            </a>
    ))
    return(
        <footer className="footer p-5">
            <div className="footer-links">{linksList}</div>
            <div className="footer-copy">
                <div className="footer-copy-logo"><img src={logoSrc} alt=""/></div>
                <div className="footer-copy-text pt-4">{copy}</div>
            </div>
            <div className="footer-social" >{socialLinksList}</div>
        </footer>
    )
}

export default Footer;