import React from "react"
import "./Paginator.css"

const Paginator = ({sliderMovies}) => {
const paginationItems = sliderMovies.map((item, index) => (
        <div key={index } className={`paginator-item ${(index === 0) ? "active" : ""}`}/>
    ));
    return (
        <div className="paginator p-5">
            {paginationItems}
        </div>
    );
}
export default Paginator