import React from "react"
import MovieItem from "./components/MovieItem";
import "./MovieList.css"
import PromoBlock from "./components/PromoBlock";

const MovieList = ({movies}) => {

    const movieList = movies.map((movie, index) => {
        return (index !== 7) ? <MovieItem key={index} movie={movie}/> : <><MovieItem key={index} movie={movie}/><PromoBlock/></>
    })
        return (
            <div className="movie-list">
                {movieList}
            </div>
        );
}

export default MovieList;