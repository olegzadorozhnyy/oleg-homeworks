import React from "react"
import Button from "../../../../../../../../../shared/components/Button";
import "./PromoBlock.css"
import bg from "./promobg.jpg"

const PromoBlock = () => {
    return(
        <div className="promo">
            <div className="promo-content" style={{background: `url(${bg})`}}>
                <h3 className="promo-header">Receive information on the latest hit movies straight to your inbox.</h3>
                <Button type="primary" text="Subscribe!" />
            </div>
        </div>
    )
}
export default PromoBlock