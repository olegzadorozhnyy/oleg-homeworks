import React from 'react'
import MovieList from "./MovieList";
import MovieFilter from "./MovieFilter";
import "./Wrapper.css";


const Wrapper = ({movies,category}) => {
    return (
            <div className="movie-wrapper p-5">
                <MovieFilter category={category} />
                <MovieList movies={movies}/>
            </div>
    )
};


export default Wrapper