import React from "react"
import "./MovieItem.css"

const MovieItem = ({movie}) => {
    const {smallImg, descr} = movie;
    const {tags, title, rating} = descr;
    const tagList = tags.map((item, index) => (
        <span key={index} className="tags">{item}</span>
    ));
    return (
        <div className="movie-item mb-5">
            <div className="movie-item-img"><img src={smallImg} /></div>
            <div className="p-5">
                <div className="movie-item-title">
                    <div className="movie-item-title-name">{title}</div>
                    <div className="rating blue">{rating}</div>
                </div>
                <div className="movie-item-tags">{tagList}</div>
            </div>

        </div>
    );
}

export default MovieItem