import React from "react";
import MovieTitle from "./components/MovieTitle"
import "./Slider.css"

const Slider = ({sliderMovies}) => {
    const slidesList = sliderMovies.map(({bigImg,id, descr}, index) => (
            <div key={index} className={`slide d-flex justify-content-between ${(index === 0) ? "active" : ""}`}
                 style={{background: `url(${bigImg})`}}>
                <MovieTitle  descr = {descr}/>
            </div>
        ));
    return (
        slidesList
    );
}
export default Slider


