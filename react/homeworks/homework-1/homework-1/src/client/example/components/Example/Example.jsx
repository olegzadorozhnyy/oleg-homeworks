import React from 'react';
import Slider from "./components/Slider";
import Wrapper from "./components/Wrapper"
import Footer from "./components/Footer";
import Header from "./components/Header";
import Paginator from "./components/Slider/components/Paginator";
import "./Example.css"

 const Example = (props) => {
     const {movies, footer, category} = props;
     const sliderMovies = movies.filter(({slider}) => (slider))
     return (
         <>
             <div className="wrapper">
                 <Header />
                 <Paginator sliderMovies= {sliderMovies} />
                 <div className="slides">
                     <Slider sliderMovies = {sliderMovies} />
                 </div>
             </div>
             <Wrapper category={category} movies = {movies}/>
             <Footer footer = {footer}/>
         </>
    );
};
 export default Example