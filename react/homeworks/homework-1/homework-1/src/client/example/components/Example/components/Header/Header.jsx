import React from "react"
import Logo from "../../../../../../shared/components/Logo/Logo";
import AuthPanel from "../../../../../../shared/components/AuthPanel/AuthPanel";
import "./css/Header.css"

const Header = () => {
    return (
        <header className="d-flex justify-content-between p-5 header">
            <Logo/>
            <AuthPanel/>
        </header>
    )
}

export default Header;