import React from "react"
import "./MovieTitle.css"
import Button from "../../../../../../../../shared/components/Button";

const MovieTitle = ({descr}) => {
    const {tags, title, duration, rating, id} = descr;
    const tagList = tags.map(item => (
        <span key={item} className="tags">{item}</span>
    ));
    const starsCount = (Math.round(rating));
    let stars = [];
    for (let i=0; i < starsCount; i++){
        stars.push(<i key={i} className="stars">★</i>);
    }

    return(
        <div className="movieTitle d-flex p-5">
            <div>
                <h2>{title}</h2>
                <div className="description">
                    <div className="tags">{tagList}</div>
                    <div className="separator">|</div>
                    <div className="duration">{duration}</div>
                </div>
            </div>
            <div className="more-info">
                <div className="rating-block">
                    {stars}
                    <div className="rating">{rating}</div>
                </div>
                <div className="film-options">
                    <Button type='primary' text='Watch Now' />
                    <Button type='semi' text='View Info' />
                    <Button type='outline' text='+ Favorites' />
                </div>
            </div>
        </div>
    );
}

export default MovieTitle