import React from "react"
import Button from "../../../../../../../shared/components/Button";
import "./MovieFilter.css"
import sprite from "./sprite1.png"

const MovieFilter = ({category}) => {
    const categoryList = category.map(({categoryName, subCategory}, index) =>
        {
            const isActive= (index === 0) ? "active" : "";
            const isSub=  (subCategory) ? "sub" : "";
            const btnClass = isActive + isSub;

            return <Button key={index} type='filter' text={categoryName} btnClass={btnClass} />
        }
    )
    const VuewFilter = () => {
        return (
            <div>
                <div className="position-filter">
                    <span className="position-filter-item grid active" style={{background: `url(${sprite}`}}></span>
                </div>
                <div className="position-filter">
                    <span className="position-filter-item table " style={{background: `url(${sprite}`}}></span>
                </div>
            </div>
            )
    }
    return (
        <div className="category-filter">
            <div>{categoryList}</div>
            <VuewFilter />

        </div>
    );

}

export default MovieFilter;