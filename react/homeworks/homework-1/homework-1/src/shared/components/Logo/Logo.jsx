import React from "react"
import './css/logo.css';
import src from "./img/logo.png"


const Logo = ({alt}) => {
    const logoImg = <img className='logo-img' src={src} alt={alt} />
    return(
        <a className='logo' href="/">
            {logoImg}
        </a>
    )
}

export default Logo;