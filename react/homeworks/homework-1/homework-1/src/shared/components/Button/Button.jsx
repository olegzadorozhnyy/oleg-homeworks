import React from 'react';
import "./css/Buttons.css"

const btnTypes = {
    outline: 'btn-outline',
    primary: 'btn-primary',
    light: 'btn-light',
    semi : 'semi',
    filter : 'filter'
}
const Button =({type, text, btnClass})=>{
    return(
        <button type="button" className={`btn ${btnTypes[type]} text-white ${btnClass}`}>{text}</button>
    );
}

export default Button;