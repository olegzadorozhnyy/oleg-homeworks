function importAll(r) {
    return r.keys().map(r);
}
const BigImg = importAll(require.context('../../../../assets/images/SliderImages', false, /\.(png|jpe?g|svg)$/));

export {BigImg};