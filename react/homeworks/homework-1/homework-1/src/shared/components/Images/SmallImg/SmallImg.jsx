function importAll(r) {
    return r.keys().map(r);
}
const SmallImg = importAll(require.context('../../../../assets/images/small', false, /\.(png|jpe?g|svg)$/));

export {SmallImg};