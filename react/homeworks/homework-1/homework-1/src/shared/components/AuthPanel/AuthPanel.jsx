import React from 'react';
import Button from '../Button';

const AuthPanel =()=>{
    return(
        <div className='auth-panel'>
            <Button type='outline' text='Log In' />
            <Button type='primary' text='Sign Up' />
        </div>
    );
}

export default AuthPanel;