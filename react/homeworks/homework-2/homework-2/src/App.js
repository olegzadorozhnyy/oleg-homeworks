import React, {useState} from 'react';
import './App.css';
import './bootstrap.min.css';
import Header from "./components/Header";
import FormAddBook from "./components/FormAddBook";
import BookList from "./components/BookList";



const props = {
    addBtn : {
        value: "Add book",
        type: "submit"
    }
}

function App() {
    const {addBtn} = props;
    const [books, setBook] = useState([]);

    const handleBook = (bookInfo) => {
        setBook([...books, bookInfo]);
    }

    const deleteBook = (isbn) => {
        setBook( () => {
            const index = books.findIndex(item => item.isbn === isbn);
            return books.splice(index, 1);
        });
    };

  return (
      <div className="container mt-4">
      <Header />
      <FormAddBook addBtn = {addBtn} handleBook = {handleBook}   />
      <BookList books = {books} deleteBook = {deleteBook} handleBook = {handleBook}/>
    </div>
  );
}

export default App;
