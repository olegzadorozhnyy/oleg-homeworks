import React from "react";
import BookItem from "./components/BookItem";


const BookList = ({books, deleteBook, handleBook}) => {
    return(
        <table className="table table-striped mt-2">
            <thead>
            <tr>
                <th>Title</th>
                <th>Author</th>
                <th>ISBN#</th>
                <th>Title</th>
            </tr>
            </thead>
            <tbody id="book-list">
               {books.map(item => <BookItem key={item.isbn} {...item} deleteBook = {deleteBook} handleBook={handleBook}/>
            )}
            </tbody>
        </table>
    )
}

export default BookList