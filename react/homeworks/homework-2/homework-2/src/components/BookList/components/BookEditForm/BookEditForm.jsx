import React from "react";
import Button from "../../../../shared/Button";

const BookEditForm = ({props, editBookItem}) => {
    const {name, author, isbn, handleBook, deleteBook} = props;
    const AddBook = (event) => {
        event.preventDefault();
        const elements = Array.from(event.target.elements);
        const inputs = elements.filter(({nodeName}) => nodeName === "INPUT");
        const book = {};
        inputs.map(({name, value}) => {
            book[`${name}`] = value;
        });
        deleteBook(isbn)
        handleBook(book);
        editBookItem();
    }
    const btnProps = {
            value: "Edit book",
            type: "submit"
    }
    return(

        <div>
            <form  onSubmit={AddBook}>
                    <div className="form-group">
                        <input type="text" name="name" className="form-control" defaultValue={name}/>
                    </div>
                    <div className="form-group">
                        <input type="text" name="author" className="form-control" defaultValue={author}/>
                    </div>
                    <div className="form-group">
                        <input type="text" name="isbn" className="form-control" defaultValue={isbn}/>
                    </div>
                <Button {...btnProps} />
            </form>
        </div>
    )
}

export default BookEditForm