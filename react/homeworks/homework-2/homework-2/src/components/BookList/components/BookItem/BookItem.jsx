import React, {useState} from "react";
import BookTr from "../BookTr";
import BookEditForm from "../BookEditForm";


const BookItem = (props) => {
    const [row, setRow] = useState("true");
    const editBookItem = () => {
        setRow(!row);
    }

    const item = (row) ? <BookTr editBookItem={editBookItem} props={props} /> :
        <BookEditForm  editBookItem={editBookItem} props={props} />
    return(
       <>
           {item}
       </>
    );
}

export default BookItem