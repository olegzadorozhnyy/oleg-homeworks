import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faTimes} from "@fortawesome/free-solid-svg-icons";

const BookTr = ({editBookItem, props}) => {
    const {name, author, isbn, deleteBook} = props;
    return (
        <tr>
            <td>{name}</td>
            <td>{author}</td>
            <td>{isbn}</td>
            <td><a onClick={() => editBookItem()}  href="#" className="btn btn-info btn-sm"><FontAwesomeIcon icon={faEdit} /></a></td>
            <td><a onClick={() => deleteBook(isbn)} href="#" className="btn btn-danger btn-sm btn-delete"><FontAwesomeIcon icon={faTimes} /></a></td>
        </tr>
    )
}

export default BookTr;