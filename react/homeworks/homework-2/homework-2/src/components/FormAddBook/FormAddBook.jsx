import React from "react";
import Button from "../../shared/Button";


const FormAddBook = ({addBtn, handleBook}) => {
    const AddBook = (event) => {
        event.preventDefault();
        const elements = Array.from(event.target.elements);
        const inputs = elements.filter(({nodeName}) => nodeName === "INPUT");
        const book = {};
        inputs.map(({name, value}) => {
            book[`${name}`] = value;
        });
        handleBook(book);
    }
    return(
        <div className="row">
            <div className="col-lg-4">
                <form id="add-book-form" onSubmit={AddBook}>
                    <div className="form-group">
                        <label htmlFor="title">Title</label>
                        <input type="text" name="name" className="form-control" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="author">Author</label>
                        <input type="text" name="author" className="form-control" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="title">ISBN#</label>
                        <input type="text" name="isbn" className="form-control" />
                    </div>
                <Button {...addBtn} />
                </form>
            </div>
        </div>
    )
}

export default FormAddBook