import React from "react";

const Button = ({type, value}) => {
    return(
        <button type={type} className="btn btn-primary">{value}</button>
    )
}

export default Button