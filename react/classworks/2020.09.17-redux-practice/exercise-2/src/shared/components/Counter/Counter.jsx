import React from "react";
import {useSelector, useDispatch} from "react-redux";
import {createDecrementAction, createIncrementAction} from "../../../store/createActions";

const Counter = () => {
    const count = useSelector((state) => state.count);
    const dispatch = useDispatch();
    const decrement = () => dispatch(createDecrementAction());
    const increment = () => dispatch(createIncrementAction());

    return(
        <div>
            <button onClick={increment}>+</button>
            <span>{count}</span>
            <button onClick={decrement}>-</button>
        </div>
    )
}


export default Counter
