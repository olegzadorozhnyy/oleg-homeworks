import React from "react";
import {createDecrementAction, createIncrementAction} from "../../../store/createActions";
import {connect} from "react-redux";

const Counter = ({count,decrement,increment}) => {
    return(
        <div>
            <button onClick={increment}>+</button>
            <span>{count}</span>
            <button onClick={decrement}>-</button>
        </div>
    )
}

const mapStateToProps = (state) => {
    return{
        count: state.count
    }
}

const mapDispatchToProps = (dispatch) => {
    return  {
        decrement: () => dispatch(createDecrementAction()),
        increment: () => dispatch(createIncrementAction())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter)