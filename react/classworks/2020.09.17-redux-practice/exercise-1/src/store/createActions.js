import {INCREMENT_COUNTER, DECREMENT_COUNTER} from "./actions";

export const createIncrementAction = (payload) => ({type: INCREMENT_COUNTER, payload});

export const createDecrementAction = (payload) => ({type: DECREMENT_COUNTER, payload});