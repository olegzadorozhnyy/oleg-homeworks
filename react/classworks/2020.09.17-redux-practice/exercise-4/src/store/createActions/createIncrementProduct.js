import {INCREMENT_PRODUCT} from "../actions";

export const createIncrementProductAction = (payload) => {
    return {
        type: INCREMENT_PRODUCT,
        payload
    }
}