import {initialState} from "./initialState";
import {ADD_TO_CART, DECREMENT_PRODUCT, INCREMENT_PRODUCT, CHANGE_CURRENCY} from "./actions";
import {increment, decrement} from "./reducerFunctions/reducerFunctions";


export const reducer = (state = initialState, {type, payload}) => {

    switch (type) {
        case ADD_TO_CART:
            return {...state, cart: increment(payload, state)}
        case DECREMENT_PRODUCT:
            return {...state, cart: decrement(payload, state)};
        case INCREMENT_PRODUCT:
            return {...state, cart: increment(payload, state)};
        case CHANGE_CURRENCY:
            return {...state, currency: payload}

            default:
            return state;
    }
}