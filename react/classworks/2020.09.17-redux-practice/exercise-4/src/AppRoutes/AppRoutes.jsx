import React from "react";
import {Route, Switch} from "react-router-dom"
import HomePage from "../client/pages/HomePage";
import Error from "../client/pages/Error";
import Cart from "../client/pages/Cart";
import ProductList from "../client/pages/ProductList";
import Product from "../client/pages/Product";

const AppRouter = () => {
    return(
        <Switch>
            <Route exact path="/">
                <HomePage/>
            </Route>
            <Route exact path="/cart">
                <Cart/>
            </Route>
            <Route exact path="/product/:productId" component={Product} />
            <Route exact path="/product-list">
                <ProductList/>
            </Route>
            <Route>
                <Error/>
            </Route>
        </Switch>
    )
}

export default AppRouter