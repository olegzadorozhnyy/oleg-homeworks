import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {createIncrementProductAction} from "../../store/createActions/createIncrementProduct";
import {createDecrementProductAction} from "../../store/createActions/createDecrementProduct";


const CartItem = (props) => {
    const {title, price, id, count} = props;
    const dispatch = useDispatch();
    const rate = useSelector(({currency} )=> currency.rate);

    const increment = () => dispatch(createIncrementProductAction(props));
    const decrement = () => dispatch(createDecrementProductAction(props));

    return (
        <li>
            <div className="product-item">
                <h4>{title}</h4>
                <p>id: {id}</p>
                <p>price: {price*rate}</p>
                <p>count: {count}</p>
                <button onClick={increment}>+</button>
                <button onClick={decrement}>-</button>
            </div>
            </li>
    )
}
export default CartItem