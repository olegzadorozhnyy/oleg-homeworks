import React from 'react'
import {useSelector, shallowEqual} from "react-redux";
import ProductItem from "../../../shared/ProductItem";

const ProductList = ()=> {
    const products = useSelector(({products}) => products, shallowEqual);
    const productsElems = products.map(item => <ProductItem {...item} />)

    return (
        <>
            <h2>ProductList</h2>
            <ul>
                {productsElems}
            </ul>
        </>
    )
}

export default ProductList;