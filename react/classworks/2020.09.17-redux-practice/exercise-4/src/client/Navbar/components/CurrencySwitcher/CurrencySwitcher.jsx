import React from "react";
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import {createActionCurrencyChange} from "../../../../store/createActions/createActionCurrencyChange"
import {rate} from "../../../../store/rate";

const CurrencySwitcher = () => {
    const currency = useSelector(({currency}) => currency, shallowEqual);
    const dispatch = useDispatch();
    const currencyElems = rate.map(({name}) => <option selected={currency === name} value={name}>{name}</option>);
    const changeCurrency = ({target}) => {
        const currency = rate.find(({name}) => name === target.value);
        dispatch(createActionCurrencyChange(currency))
    }
    return (
        <select onChange={changeCurrency}>{currencyElems}</select>
    )
}

export default CurrencySwitcher;