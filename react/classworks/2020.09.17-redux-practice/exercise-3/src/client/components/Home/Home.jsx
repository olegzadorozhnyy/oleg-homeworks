import React from 'react';
import Button from '../Button';
import {useSelector, useDispatch, shallowEqual} from "react-redux";
import {setEng} from "../../../store/actionCreators/setEng"
import {setRu} from "../../../store/actionCreators/setRu"
import translate from "../../../shared/translate"

const Home = () => {

    const currentLanguage = useSelector(state => state.currentLanguage, shallowEqual);
    const dispatch = useDispatch();

    const title = translate.title[currentLanguage];
    return (
        <>
        <p>
        <Button text="English" handleClick={() => dispatch(setEng())}/>
        <Button text="Russian" handleClick={() =>  dispatch(setRu()) }/>
        </p>
        
        <h1>{title}</h1>
        <h2>Time: </h2>
        <h2>Consultant is: </h2>
        </>
    )
}

export default Home;