const localLanguage = localStorage.getItem('language');
const browserLanguage = window.navigator.language;

console.log(window.navigator.language);
export const initialState = {
    currentLanguage: localLanguage || browserLanguage
};