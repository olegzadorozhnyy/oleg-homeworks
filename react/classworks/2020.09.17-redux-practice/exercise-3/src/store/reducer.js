import {initialState} from "./initialState";
import {SET_ENG, SET_RU} from "./actions";

export const reducer = (state = initialState, action)=> {
    switch(action.type) {
        case SET_ENG:
            localStorage.setItem('language', "en");
            return ({...state, currentLanguage: "en"});
        case SET_RU:
            localStorage.setItem('language', "ru");
            return ({...state, currentLanguage: "ru"});           
        default: 
            return state;
    }
};