import React from 'react';
import './App.css';
import ProductApp from "./Client/ProductApp/components/ProductApp";

const products = ["Манускрипт войнича", 
"Второй том мертвых душ", 
"Евангелие от Иуды", 
"Настоящее завещание Франциска Оллара"];

function App() {
  return (
    <div className="App">
      <ProductApp productsList={products}/>
    </div>
  );
}

export default App;
