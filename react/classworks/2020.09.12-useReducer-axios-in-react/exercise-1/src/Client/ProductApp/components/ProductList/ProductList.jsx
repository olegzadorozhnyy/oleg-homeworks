import React from "react"

const ProductList = ({productsList, addToCart}) => {
    const productsListElements = productsList.map(elem => (
        <li><button onClick={() => addToCart(elem)}>Buy</button>  {elem}</li>
    ))
    return (
        <ul>
            {productsListElements}
        </ul>
    )
}

export default ProductList