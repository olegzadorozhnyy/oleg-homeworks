import React, {useReducer} from "react"
import ProductList from "../ProductList";
import ProductCart from "../ProductCart";

const initialState = [];
const changeCartList = (state, action) => {
    switch (action.type) {
        case "Add" :
            return [...state, action.elem]
        case "Delete" :
            const newState = [...state];
            newState.splice(action.idx, 1)
            return newState;
        case "Minus" :
            return ;
        case "Plus" :
            return ;
        default :
            break;
    }
}

const ProductApp = ({productsList}) => {
    const [cart, dispatch] = useReducer(changeCartList, initialState);

    return (
        <>
            <ProductCart removeFromCart={(index) => dispatch({type : "Delete", idx: index }, {type: "Minus", idx: index}, {type: "Plus", idx: index})} cart={cart} />
            <ProductList addToCart={(product) => dispatch({type : "Add", elem: product})} productsList={productsList} />
        </>
    )
}

export default ProductApp