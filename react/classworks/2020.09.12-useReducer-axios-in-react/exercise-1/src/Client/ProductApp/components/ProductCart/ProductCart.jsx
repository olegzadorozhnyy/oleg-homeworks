import React from "react"

const ProductCart = ({cart, removeFromCart}) => {

    const cartElements = cart.map((elem, index) => (
        <li><button>+</button>
            <button>—</button>
            <button onClick={() => removeFromCart(index)}>Delete</button>
            {elem}</li>
    ))
    return (
        <ul>
            {cartElements}
        </ul>
    )
}

export default ProductCart