import React from 'react'

const FontDisplaySetting = ({actions})=> {
    const {makeBold, makeNormal, makeItalic} = actions;
    return (
        <>
        <button onClick={(makeNormal)}>Normal</button>
        <button onClick={makeBold}>Bold</button>
        <button onClick={makeItalic}>Italic</button>
        </>
    )
}

export default FontDisplaySetting;