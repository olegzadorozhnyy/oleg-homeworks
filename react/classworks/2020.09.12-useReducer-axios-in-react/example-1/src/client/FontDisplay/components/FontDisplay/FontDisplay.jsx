import React, {useState} from 'react';

import FontPreview from "../FontPreview";
import FontDisplaySetting from "../FontDisplaySetting";

const [font, changeStyle] = useFontDisplay();

const FontDisplay = ()=> {
    const [font, setFont] = useState({
        bold: false,
        italic: false
    });

    const changeStyle = (action) => {
        switch(action) {
            case "make-normal":
                setFont({bold: false, italic: false});
                break;
            case "make-bold":
                setFont({bold: true, italic: font.italic});
                break;
            case "make-italic":
                setFont({bold: font.bold, italic: true});
                break;
            default:
                return new Error("unknown action");
        }
    };

    // const changeWeight = (weight)=> {
    //     console.log(font)
    //     const bold = (weight === "bold");
    //     setFont({bold, italic: font.italic});
    // };

    // const changeStyle = (styles)=> {
    //     const italic = styles === "italic";
    //     setFont({bold: font.bold, italic});
    // };  

    const fontSettingActions = {
        makeBold: ()=> changeStyle("make-bold"),
        makeNormal: ()=> changeStyle("make-normal"),
        makeItalic: ()=> changeStyle("make-italic")
    };

    return(
        <>
        <FontPreview font={font} />
        <FontDisplaySetting actions={fontSettingActions} />
        </>
    )
}

export default FontDisplay;