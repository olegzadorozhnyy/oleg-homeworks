import React, { Component } from "react";
import { OmdbService } from "../../services/omdb-service";
// import { debounce } from "../../utils/debounce.utils";
// import { Search } from "../Search";

export class Navbar extends Component {
  omdbService = new OmdbService();
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
    this.state = {
      search: "",
    };
  }

  changeState = () => {
    this.setState((state) => ({
      ...state,
      search: this.inputRef.current.value,
    }));
  };

  onSubmit = (event) => {
    event.preventDefault();
    this.changeState();
    this.timeout = setTimeout(
      () => alert(JSON.stringify(this.state.search, null, 2)),
      300
    );
  };

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light navbar-custom fixed-top">
        <div className="container-fluid d-flex justify-content-center">
          <form className="d-flex" onSubmit={this.onSubmit}>
            <input
              type="text"
              ref={this.inputRef}
              className="form-control mr-2 search"
              placeholder="Search"
              aria-label="Search"
            />
            <button className="btn btn-outline-primary ml-2" type="submit">
              Search
            </button>
          </form>
          {/* <Search name="search" handleChange={this.handleChange} /> */}
        </div>
      </nav>
    );
  }
}
