import React, { Component } from "react";
import PropTypes from "prop-types";
import { OmdbService } from "../../services/omdb-service";

const Imdb = ["tt5180504", "tt0385002", "tt0460681", "tt0944947", "tt0120689"];

export const FilmView = ({ film: { Title, Poster } }) => {
  return (
    <div className="row d-flex justify-content-center">
      <div className="col-3 mb-5">
        <div className="card">
          <img src={Poster} className="card-img-top image" alt={Title} />
          <div className="car-body text-center py-3">
            <h5 className="card-title title">{Title}</h5>
            <button className="btn btn-primary">More...</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export class RandomFilm extends Component {
  static defaultProps = {
    updateInterval: 10000,
  };

  static propTypes = {
    updateInterval: PropTypes.number,
  };

  omdbService = new OmdbService();

  state = {
    film: {},
    loading: true,
    error: false,
  };

  componentDidMount() {
    const { updateInterval } = this.props;
    this.updateFilm();
    this.interval = setInterval(this.updateFilm, updateInterval);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  onFilmLoaded = (film) => {
    this.setState({
      film,
      loading: false,
      error: false,
    });
  };

  onError = (err) => {
    this.setState({
      error: true,
      loading: false,
    });
  };


  updateFilm = () => {
    const id = Math.floor(Math.random() * Imdb.length);
    this.omdbService
      .randomFilm(Imdb[id])
      .then(this.onFilmLoaded)
      .catch(this.onError);
  };

  render() {
    const { film, error, loading } = this.state;
    const hasData = !(loading || error);
    const errorMessage = error ? "Господа что пошло не так!" : null;
    const spinner = loading ? <p>Loading...</p> : null;
    const content = hasData ? <FilmView film={film} /> : null;

    return (
      <header className="header pt-3">
        {spinner}
        {errorMessage}
        {content}
      </header>
    );
  }
}
