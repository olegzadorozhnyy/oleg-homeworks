export class OmdbService {
  _apiBase = `http://www.omdbapi.com/`;

  getResource = async (url) => {
    const res = await fetch(`${this._apiBase}${url}&apikey=ee2a4e12`);

    if (!res.ok) {
      throw new Error(`Could not fetch ${url} received ${res.status}`);
    }
    return await res.json();
  };

  searchFilms = async (words) => {
    const response = await this.getResource(`?s=${words}`);
    return response.Search;
  };

  randomFilm = async (id) => {
    const response = await this.getResource(`?i=${id}`);
    return response;
  };
}
