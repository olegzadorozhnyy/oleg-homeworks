import React from 'react';
import {BoysList} from "./components/BoysList"
import logo from './logo.svg';
import './App.css';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <BoysList />
      </header>
    </div>
  );
}

export default App;
