import React from 'react';

export const BoysListElem = ({id, name, handleDelete}) => {
    return (
        <li key={id}>{name}<span onClick={() => handleDelete(id)} className="delete-btn"> X</span></li>
    )
}