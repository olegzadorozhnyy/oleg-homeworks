import React, {Component} from "react"
import "./style.css"
import { BoysListElem } from "../BoysListElem/"

export class BoysList extends Component{
    

    state = {
        list: [
            {name:"Хоумлендер", id: 1},
            {name:"Королева Мэйв", id: 2},
            {name:"Черный Нуар", id: 3},
            {name:"Звездочка", id: 4},
            {name:"Бездна", id: 5},
            {name:"Прозрачный", id: 6}
            ]
    };

    deleteListItem = (id) =>{
        this.setState(({list}) => {
            const newList = [...list];
            const index = newList.findIndex(item =>item.id === id);
            newList.splice(index, 1);
            return {list:newList}
        })
    };

    render() {
        const {list} = this.state;
        const boysItems = list.map((item, index) => {
        return <BoysListElem {...item} handleDelete={this.deleteListItem}/>});
        return(
            <ul className="boys-list">
                {boysItems}
            </ul>
        );
    };
}

