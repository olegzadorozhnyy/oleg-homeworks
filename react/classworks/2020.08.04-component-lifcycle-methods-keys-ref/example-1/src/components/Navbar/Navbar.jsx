import React, { Component } from "react";
import { OmdbService } from "../../services/omdb-service";
import { debounce } from "../../utils/debounce.utils";
import { Search } from "../Search";

export class Navbar extends Component {
  omdbService = new OmdbService();
  state = {
    search: '',
    searchList: []
}

  handleChange = (e) => {
    this.setState({
      search: e.target.value
    })
  }

  componentDidUpdate(prevState) {
    if (prevState.search !== this.state.search) {
      fetch(url, {
        body: JSON.stringify({searchPhrase: this.state.search})
      }).then(responce => responce.json())
      .then((result) => {
        this.setState({
          searchList: result
        })
      })
    }
  }

  onSubmit = (event) => {
    event.preventDefault();
   
  };


  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light navbar-custom fixed-top">
        <div className="container-fluid d-flex justify-content-center">
          <Search name="search" handleChange={this.handleChange} />
        </div>
      </nav>
    );
  }
}
