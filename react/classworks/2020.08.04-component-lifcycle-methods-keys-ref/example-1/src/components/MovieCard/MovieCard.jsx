import React from "react";

export const MovieCard = ({ Title, Poster }) => {
  return (
    <div className="col-3 mb-5">
      <div className="card">
        <img src={Poster} className="card-img-top image" alt={Title} />
        <div className="car-body text-center py-3">
          <h5 className="card-title title">{Title}</h5>
          <button className="btn btn-primary">More...</button>
        </div>
      </div>
    </div>
  );
};
