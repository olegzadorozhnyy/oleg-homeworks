import React from "react";

export const Search = ({ type, name, handleChange, className }) => {
  return (
    <form className="d-flex">
      <input
        type={type}
        name={name}
        onChange={handleChange}
        className={`form-control mr-2 search ${className}`}
        placeholder="Search"
        aria-label="Search"
      />
      <button className="btn btn-outline-primary ml-2" type="submit">
        Search
      </button>
    </form>
  );
};

Search.defaultProps = {
  className: " ",
  type: "text",
  name: "search",
};
