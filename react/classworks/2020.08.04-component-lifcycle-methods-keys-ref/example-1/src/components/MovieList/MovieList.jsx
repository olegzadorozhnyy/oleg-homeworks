import React from "react";
import { MovieCard } from "../MovieCard";

export const MovieList = ({ movies }) => {
  return (
    <div className="row">
      {movies.map((props) => {
        return <MovieCard key={props.imdbID} {...props} />;
      })}
    </div>
  );
};
