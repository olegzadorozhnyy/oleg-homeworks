import React, { Component } from "react";
import "./styles.css";

import { OmdbService } from "./services/omdb-service";
import { MovieList } from "./components/MovieList";
import { Navbar } from "./components/Navbar";
import { RandomFilm } from "./components/RandomFilm";


export default class App extends Component {

  omdbService = new OmdbService();

  state = {
    movies: [],
  };

  

  componentDidMount() {
    this.omdbService.searchFilms("game+of").then((data) =>
      this.setState({
        movies: data,
      })
    );
  }

  componentDidUpdate() {
    
  }

  render() {
    const { movies } = this.state;
    if (!movies) {
      return (
        <div className="spinner-grow text-primary" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      );
    }

    return (
      <div className="App">
        <Navbar />
        <RandomFilm />
        <div className="container">
          <MovieList movies={movies} />
        </div>
      </div>
    );
  }
}
