import React from 'react'
import "./NavbarMenuItem.css";
import {Link} from "react-router-dom"

const NavbarMenuItem = ({href, text, handleClick}) => {
    return (
        <li className="navbar-menu-item">
            <Link to={href} className="navbar-menu-link" >{text}</Link>
        </li>
    )
}

export default NavbarMenuItem;