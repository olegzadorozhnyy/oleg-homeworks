import React, {useState} from "react";
import "./Navbar.css";
import {BrowserRouter as Router, Route} from 'react-router-dom'

import {menuItems} from "./menuItems";
import NavbarMenuItem from "../NavbarMenuItem";

import HomePage from "../../../Home/pages/HomePage";
import AboutPage from "../../../About/pages/aboutPage";
import ContactsPage from "../../../Contacts/pages/ContactsPage";



const Navbar = () => {
    const navbarMenuElements = menuItems.map(item => <NavbarMenuItem {...item} />);

    return (
        <>
            <Router>
                <nav className="navbar">
                    <div className="container">
                        <div className="navbar-row">
                            <ul className="navbar-menu">
                                {navbarMenuElements}
                            </ul>
                        </div>
                    </div>
                </nav>
                <Route exact  path="/home" component={HomePage} />
                <Route exact  path="/about" component={AboutPage} />
                <Route exact  path="/contacts" component={ContactsPage} />
                <div>Hello there</div>
            </Router>
        </>


    )
}

export default Navbar;

