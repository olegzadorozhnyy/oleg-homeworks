import React from 'react';
import logo from './logo.svg';
import './App.css';

import Navbar from "../../../client/Navbar/components/Navbar";

function App() {
  return (
    <div className="App">
      <Navbar />
    </div>
  );
}

export default App;
