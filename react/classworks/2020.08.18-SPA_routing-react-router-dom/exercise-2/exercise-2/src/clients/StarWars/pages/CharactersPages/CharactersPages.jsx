import React, {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import axios from 'axios';

const CharacterPages = () => {
    const [characters, setCharacter] = useState([]);
    useEffect(() => {
        axios.get("https://swapi.dev/api/people/")
            .then(({data}) =>
        { const {results} = data;
            const characterList = results.map(({name, url}) => ({name, url}))
            setCharacter(characterList)
        })
    }, []);
    const characterElements = characters.map(({name, url}) => {
        return <li>{name}<Link href={`character/${url}`}>Подробнее</Link></li>
        }
    );

    return (
        <>
            {characterElements}
       </>
    )
}

export default CharacterPages