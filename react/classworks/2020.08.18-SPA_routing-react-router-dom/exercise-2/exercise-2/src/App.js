import React from 'react';
import './App.css';
import {BrowserRouter as Router} from "react-router-dom";
import CharacterPages from "./clients/StarWars/pages/CharactersPages";

function App() {
  return (
    <div className="App">
            <CharacterPages />
    </div>
  );
}

export default App;
