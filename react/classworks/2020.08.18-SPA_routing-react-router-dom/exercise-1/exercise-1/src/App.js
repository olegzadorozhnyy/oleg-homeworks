import React from 'react';
import './App.css';
import HomePage from "./clients/Home/pages/HomePage";
import ContactsPage from "./clients/Contacts/pages/ContactsPage";
import ProductsPage from "./clients/Products/pages/ProductsPage";
import ProductsFromPartnersPage from "./clients/ProductsFromPartners/pages/ProductsFromPartnersPage";
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Navbar from "./clients/Navbar/components/NavbarMenu";

function App() {
  return (

      <>

        <Router>
        <Navbar />
        <div className="App">
            <Route exact path="/home" component={HomePage}/>
            <Route exact path="/contacts" component={ContactsPage}/>
            <Route exact path="/products" component={ProductsPage}/>
            <Route exact path="/products-from-partners" component={ProductsFromPartnersPage}/>
        </div>
      </Router>
      </>

  );
}

export default App;
