import React from "react";
import {Link} from 'react-router-dom';
import "./NavbarMenu.css"

const NavbarMenu = () => {
    return (
        <ul className="navbar">
            <li><Link to="/home">Home</Link></li>
            <li><Link to="/products">Products</Link></li>
            <li><Link to="products-from-partners">Products from partners</Link></li>
            <li><Link to="/contacts">Contacts</Link></li>
        </ul>
    )
}

export default NavbarMenu