import React, { useState, useCallback } from "react";
import { FormControl } from "../FormControl";

import { fields } from "./fields";

export const Form = () => {
  const [values, setValues] = useState({
    firstName: "",
    lastName: ""
  });

  const handleChange = useCallback(
    (event) => {
      setValues({
        ...values,
        [event.target.name]: event.target.value
      });
    },
    [values]
  );

  const handleSubmit = useCallback(
    (event) => {
      event.preventDefault();
      alert(JSON.stringify(values, null, 2));
    },
    [values]
  );

  const formItems = fields.map((props) => (
    <FormControl key={props.name} handleChange={handleChange} {...props} />
  ));
  return (
    <form onSubmit={handleSubmit}>
      {formItems}
      <button type="submit" className="btn btn-primary btn-block">
        Submit Me
      </button>
    </form>
  );
};
