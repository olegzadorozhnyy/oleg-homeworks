export const fields = [
  {
    type: "text",
    name: "firstName",
    placholder: "First Name",
    label: "First Name"
  },
  {
    type: "text",
    name: "lastName",
    placholder: "Last Name",
    label: "Last Name"
  }
];
