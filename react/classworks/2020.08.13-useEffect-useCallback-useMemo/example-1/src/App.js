import React from "react";
import "./styles.css";
import { Form } from "./Form";
import { ListsExample } from "./ListsExample/ListsExample";

export default function App() {
  return (
    <div className="App">
      <div className="container">
        <div className="row">
          <div className="col-6 offset-3">
            <Form />
          </div>
        </div>

        <ListsExample />
      </div>
    </div>
  );
}
