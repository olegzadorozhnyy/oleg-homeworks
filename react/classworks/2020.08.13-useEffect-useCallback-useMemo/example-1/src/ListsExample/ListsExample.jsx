import React, { useMemo, Fragment } from "react";

const data = [
  {
    title: "Title 1",
    description: "Description"
  },
  {
    title: "Title 22",
    description: "Description"
  },
  {
    title: "Title 33",
    description: "Description"
  }
];

export const ListsExample = () => {
  const memoData = useMemo(() => data, []);
  const listItems = memoData.map(({ title, description }) => (
    <Fragment key={title}>
      <h6>{title}</h6>
      <p>{description}</p>
    </Fragment>
  ));

  return listItems;
};
