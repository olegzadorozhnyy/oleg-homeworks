import React, { useEffect, useState } from 'react';
import {OmdbService} from "./services/omdb.service";
import {MovieList} from "./components/MovieList";
import {Pagination} from "./components/Pagination";
import {Navbar} from "./components/Navbar";

const pagination = [
    {
        page: 1,
    },
    {
        page: 2,
    },
    {
        page: 3,
    },
    {
        page: 4,
    },
    {
        page: 5,
    }
];
const { getAllFilms, searchFilms } = new OmdbService();

function App() {
    const [movies, setMovies] = useState();
    const [page, setPage] = useState(1);
    const [searchPhrase, setSearchPhrase] = useState('');

    useEffect(() => {
        getAllFilms(page).then(movies => {
            setMovies(movies.Search);
        });
        if(searchPhrase) {
            searchFilms(searchPhrase).then(movies => {
                setMovies(movies);
            });
        }
    }, [page, searchPhrase]);

    const handleClick = (currentPage) => {
        setPage(currentPage)
    };
    
    const handleSubmit = (searchPhrase) => {
        setSearchPhrase(searchPhrase);
    }

    return (
        <div className="App">
            <Navbar handleSubmit={handleSubmit}/>
            <MovieList data={movies} />
            <div className="container py-5 d-flex justify-content-center">
                <Pagination paginationArr={pagination} handleClick={handleClick} />
            </div>
        </div>
    );
}

export default App;
