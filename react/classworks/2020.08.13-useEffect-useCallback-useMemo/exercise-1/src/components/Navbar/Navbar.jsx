import React, {useState} from 'react';

export const Navbar = ({handleSubmit}) => {
    const [value, setValue] = useState('');
    const handleChange = (e) => {
        setValue(e.target.value);
    }
    const onSubmit =(e)=> {
        e.preventDefault();
        handleSubmit(value);
    }
    return (
        <nav className="navbar navbar-light bg-light mb-5">
            <div className="container-fluid">
                <a href="/" className="navbar-brand">OMDB</a>
                <form className="d-flex" onSubmit={onSubmit}>
                    <input value={value} onChange={handleChange} className="form-control mr-2" type="search" placeholder="Search" aria-label="Search"/>
                    <button className="btn btn-outline-success" type="submit">Search</button>
                </form>
            </div>
        </nav>
    );
};