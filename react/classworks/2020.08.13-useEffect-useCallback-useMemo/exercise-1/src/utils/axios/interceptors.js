export const modifyRequest = (request) => {
    if (request) {
        const apiKey = "?i=tt3896198&apikey=c6474f09"
        request.url = `${apiKey}${request.url}`;
    }
    //console.log(request);
    return request;
}

export default (axios) => {
    axios.interceptors.request.use(modifyRequest);
}