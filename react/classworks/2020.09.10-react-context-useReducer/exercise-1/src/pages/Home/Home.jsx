import React, {useContext} from "react";

import {UserName} from "../../UserNameContext";

const Home = () => {
    const name = useContext(UserName)
    return (
        <>
            <h1>Добро пожаловать, {name}</h1>
        </>
    )
}

export default Home