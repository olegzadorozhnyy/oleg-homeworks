import React, {useContext} from "react";
import {UserName} from "../../UserNameContext";

const MyAcc = () => {
    const name = useContext(UserName)
    return (
        <>
            <h1>Ваш аккаунт, {name}</h1>
        </>
    )
}

export default MyAcc