import React from "react";
import {Route, Switch} from "react-router-dom";
import Home from "./pages/Home/Home";
import MyAcc from "./pages/MyAcc/MyAcc";


export const AppRoutes = () => {
    return (

    <Switch>
        <Route exact path="/home">
            <Home/>
        </Route>
        <Route exact path="/my-accout">
            <MyAcc/>
        </Route>
    </Switch>
    )
}
