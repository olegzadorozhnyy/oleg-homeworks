import React, {useContext} from 'react';
import {BrowserRouter as Router, Link, Route} from 'react-router-dom';
import './App.css';
import {UserName} from "./UserNameContext";
import {AppRoutes} from "./AppRoutes";

const MenuProps = [
    {
        href: "/home",
        text: "Home"
    },
    {
        href: "/my-accout",
        text: "Account"
    },

]

const NavLink = (props) => {
    const {href, text} = props;
    return (
        <>
            <Link to={href}>{text}</Link>
        </>
    )
}

const NavMenu = () => {
const menu = MenuProps.map((link) => <NavLink {...link} />)
    return (
        <nav>
            {menu}
        </nav>
    )
}



function App() {
  return (
      <UserName.Provider value="господин Аинз">
          <Router>
            <NavMenu />
            <AppRoutes/>
          </Router>
      </UserName.Provider>
  );
}

export default App;
