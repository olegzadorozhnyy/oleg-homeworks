import React,{useState} from 'react';
import LangsSwitcher from './components/LangsSwitcher/LangsSwitcher';
import Hi from './components/Hi/HI';
import Time from './components/Time/Time';
import ConsultStatus from './components/ConsultStatus';
import {PageContextProvider, PageContext} from './PageContext';


function App() {


  return (
    <PageContextProvider>
      <div className="App">
        <LangsSwitcher/>
        <Hi />
        <Time time='Some time'/>
        <ConsultStatus status='status'/>
      </div>
    </PageContextProvider>
  );
}

export default App;
