import React,{useContext} from 'react';
import {PageContext} from '../../PageContext';


const Hi = () => {

    const {currentLang} = useContext(PageContext)
    const text = (currentLang === 'ru') ? 'Добро пожаловать!' : 'Welcome!';  

    return(
    <h2>{text}</h2>
    )
}


export default Hi;