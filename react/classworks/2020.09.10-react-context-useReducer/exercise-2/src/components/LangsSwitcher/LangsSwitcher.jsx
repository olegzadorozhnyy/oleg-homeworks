import React, {useContext} from 'react';
import {PageContext} from "../../PageContext"

const LangsSwitcher = () => {

const {changeLang} = useContext(PageContext)

    return(
        <button onClick={changeLang}>
            Change language
        </button>
    )
};


export default LangsSwitcher;