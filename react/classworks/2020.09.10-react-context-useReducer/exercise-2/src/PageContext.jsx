import React, {useState} from 'react';

const lang = window.navigator.language;

const PageContext = React.createContext(lang);
const PageContextProvider = ({children}) =>{
    const [currentLang, setCurrentLang] = useState(lang);

    const changeLang = () => {
    const newLang = currentLang === 'ru' ? 'en' : 'ru';
    setCurrentLang(newLang);

}
    return(
        <PageContext.Provider value={{currentLang,changeLang}}>
            {children}
        </PageContext.Provider>
    )
}

export {PageContext, PageContextProvider};