import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import ThemeContext from './theme.context';
import { useTheme } from '../../../client/theme/hooks/useTheme';
import { updateCssVariablesHelper } from '../../../client/theme/helpers/update-css-variables.helper';

export const ThemeProvider = ({ children }) => {
  const [state, selectDarkTheme, selectLightTheme] = useTheme();

  useEffect(() => {
    updateCssVariablesHelper(state);
  }, [state]);

  return (
    <>
      <ThemeContext.Provider
        value={{ state, selectDarkTheme, selectLightTheme }}>
        {children}
      </ThemeContext.Provider>
    </>
  );
};

ThemeProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
