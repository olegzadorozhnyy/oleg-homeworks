import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../assets/fonts/popins/popins.css';
import '../../../assets/fonts/font-awesome/font-awesome.min.css';

import { Header } from '../../../client/header/components/Header';
import '../../../shared/styles/scss/style.scss';
import { ThemeProvider } from '../../context/theme/theme.provider';

export const App = () => {
  return (
    <ThemeProvider>
      <Header />
      <div className="container" />
    </ThemeProvider>
  );
};
