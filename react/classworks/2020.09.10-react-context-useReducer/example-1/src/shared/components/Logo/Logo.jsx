import React from 'react';
import classes from './Logo.module.scss';

export const Logo = () => {
  const { logo, logoPart } = classes;
  return (
    <a href="/" className={logo}>
      Renoshop
      <span className={logoPart}>bee</span>
    </a>
  );
};
