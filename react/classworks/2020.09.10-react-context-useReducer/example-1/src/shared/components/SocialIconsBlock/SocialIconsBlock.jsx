import React from 'react';
import classes from './SocialIconsBlock.module.scss';
import { IconButton } from '../IconButton';

const socialIcons = [
  {
    href: 'https://www.facebook.com/',
    iconClass: 'fa-facebook',
  },
  {
    href: 'https://www.twitter.com/',
    iconClass: 'fa-twitter',
  },
  {
    href: 'https://www.instagram.com/',
    iconClass: 'fa-instagram',
  },
  {
    href: 'https://www.linkedin.com/',
    iconClass: 'fa-linkedin',
  },
  {
    href: 'https://www.behance.com/',
    iconClass: 'fa-behance',
  },
];

export const SocialIconsBlock = () => {
  const { wrapper } = classes;
  return (
    <div className={wrapper}>
      {socialIcons.map(({ href, iconClass }) => (
        <IconButton href={href} iconClass={iconClass} key={href} />
      ))}
    </div>
  );
};
