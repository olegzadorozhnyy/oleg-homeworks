import React from 'react';
import PropTypes from 'prop-types';

import classes from './Drawer.module.scss';
import { changeAnchor } from './helpers/change-anchor';

export const Drawer = (props) => {
  const { open, anchor, onClose, children, logo } = props;
  const {
    drawer,
    animate,
    hidden,
    overlay,
    overlayOpen,
    overlayHidden,
    header,
    closeDrawer,
    logoWrapper,
  } = classes;

  const logotype = logo && <div className={logoWrapper}>{logo}</div>;

  return (
    <>
      <div
        className={`${overlay} ${!open && overlayHidden} ${
          open && overlayOpen
        }`}
        onClick={onClose}
        aria-hidden="true"
      />
      <div
        tabIndex="-1"
        className={`${drawer} ${open && animate} ${
          !open && hidden
        } ${changeAnchor(anchor, classes)}`}>
        <div className={header}>
          <button
            type="button"
            className={`close ${closeDrawer}`}
            aria-label="Close"
            onClick={onClose}>
            <span aria-hidden="true">&times;</span>
          </button>
          {logotype}
        </div>
        {children}
      </div>
    </>
  );
};

Drawer.defaultProps = {
  logo: null,
};

Drawer.propTypes = {
  open: PropTypes.bool.isRequired,
  anchor: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
  logo: PropTypes.element,
};
