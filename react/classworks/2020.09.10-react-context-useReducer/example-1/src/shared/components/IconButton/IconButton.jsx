import React from 'react';
import PropTypes from 'prop-types';
import classes from './IconButton.module.scss';

export const IconButton = (props) => {
  const { href, iconClass, className } = props;
  const { link } = classes;
  return (
    <a href={href} rel="noopener noreferrer" className={`${link} ${className}`}>
      <i className={`fa ${iconClass}`} aria-hidden="true" />
    </a>
  );
};

IconButton.defaultProps = {
  iconClass: 'fa-battery-quarter',
  className: '',
};

IconButton.propTypes = {
  href: PropTypes.string.isRequired,
  iconClass: PropTypes.string,
  className: PropTypes.string,
};
