// This would be set equal to the root styles in our CSS
const lightTheme = {
  '--color-primary': '#31e981',
  '--text-primary-theme': '#000000',
  '--text-for-dark-theme': '#ffffff',
  '--color-secondary': '#61dafb',
  '--color-secondary-hover': '#309ebd',
  '--color-three': '#2a454d',
  '--color-three-hover': '#477583',
  '--color-four': '#d35269',
  '--bg-color': '#efefef',
  '--card-color': '#ffffff',
};

// This is our dark mode
const darkTheme = {
  '--color-primary': '#286843',
  '--text-primary-theme': '#ffffff',
  '--text-for-dark-theme': '#000000',
  '--color-secondary': '#2a454d',
  '--color-secondary-hover': '#309ebd',
  '--color-three': '#61dafb',
  '--color-three-hover': '#477583',
  '--color-four': '#6d343e',
  '--bg-color': '#000000',
  '--card-color': '#172B4D',
};

export { lightTheme, darkTheme };
