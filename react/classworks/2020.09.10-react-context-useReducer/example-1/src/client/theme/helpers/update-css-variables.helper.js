export const updateCssVariablesHelper = (theme) => {
  const arrayOfVariableKeys = Object.keys(theme);
  const arrayOfVariableValues = Object.values(theme);

  // Loop through each array key and set the CSS Variables
  arrayOfVariableKeys.forEach((cssVariableKey, index) => {
    // Based on our snippet from MDN
    document.documentElement.style.setProperty(
      cssVariableKey,
      arrayOfVariableValues[index]
    );
  });
};
