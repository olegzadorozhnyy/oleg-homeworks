import { useReducer, useCallback } from 'react';
import { darkTheme, lightTheme } from '../helpers/theme.helper';

const SELECT_DARK_THEME = 'SELECT_DARK_THEME';
const SELECT_LIGHT_THEME = 'SELECT_LIGHT_THEME';

const initialState = {
  lightTheme,
};

const themeReducer = (state, action) => {
  switch (action.type) {
    case SELECT_DARK_THEME:
      return darkTheme;
    case SELECT_LIGHT_THEME:
      return lightTheme;
    default:
      return state;
  }
};

export const useTheme = () => {
  const [state, dispatch] = useReducer(themeReducer, initialState);

  const selectDarkTheme = useCallback(
    () =>
      dispatch({
        type: SELECT_DARK_THEME,
      }),
    []
  );

  const selectLightTheme = useCallback(
    () =>
      dispatch({
        type: SELECT_LIGHT_THEME,
      }),
    []
  );

  return [state, selectDarkTheme, selectLightTheme];
};
