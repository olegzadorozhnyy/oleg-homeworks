import React, { PureComponent } from 'react';
import './Search.scss';

/**
 * Component for Search website products .
 *
 * @component
 * @example
 * return (
 *   <Search />
 * )
 */

export class Search extends PureComponent {
  state = {
    search: '',
    isVisible: false,
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleOpen = () => {
    this.setState(() => ({
      isVisible: true,
    }));
  };

  handleClose = () => {
    this.setState(() => ({
      isVisible: false,
    }));
  };

  render() {
    const { search, isVisible } = this.state;
    const classNamesOverlay = isVisible ? 'overlayOpen' : 'overlayClose';
    const classNamesContent = isVisible ? 'contentOpen' : '';

    return (
      <>
        <div
          className={`overlay ${classNamesOverlay}`}
          onClick={this.handleClose}
        />
        <div className={`content ${classNamesContent}`}>
          <button
            type="button"
            className={`close close-button ${isVisible && 'close-button-open'}`}
            aria-label="Close"
            onClick={this.handleClose}>
            <span aria-hidden="true">&times;</span>
          </button>
          <div className="overlay-content">
            <form className="position-relative">
              <input
                type="text"
                placeholder="Search.."
                name="search"
                className="search"
                onChange={this.handleChange}
                value={search}
              />
              <button type="submit" className="search-button">
                <i className="fa fa-search" />
              </button>
            </form>
          </div>
        </div>
        <button
          type="button"
          className="btn position-relative"
          onClick={this.handleOpen}>
          <i className="fa fa-search" aria-hidden="true" />
        </button>
      </>
    );
  }
}
