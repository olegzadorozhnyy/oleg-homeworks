import React from 'react';
import classes from './ShoppingCartWidget.module.scss';

export const ShoppingCartWidget = () => {
  const { badges } = classes;

  return (
    <button type="button" className="btn position-relative">
      <span className={badges}>3</span>
      <i className="fa fa-shopping-cart" aria-hidden="true" />
    </button>
  );
};
