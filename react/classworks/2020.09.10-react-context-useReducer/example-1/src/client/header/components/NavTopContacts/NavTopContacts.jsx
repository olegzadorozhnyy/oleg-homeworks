import React from 'react';
import classes from './NavTopContacts.module.scss';

export const NavTopContacts = () => {
  const { contacts, item, iconWrapper, dash } = classes;
  return (
    <div className={contacts}>
      <a href="tel:+1 123 456 789" className={item}>
        <span className={iconWrapper}>
          <i className="fa fa-phone" aria-hidden="true" />
        </span>
        <span>+1 123 456 789</span>
      </a>
      <span className={dash} />
      <a href="mailto:@info@company.com" className={item}>
        <span className={iconWrapper}>
          <i className="fa fa-envelope" aria-hidden="true" />
        </span>
        <span>info@company.com</span>
      </a>
    </div>
  );
};
