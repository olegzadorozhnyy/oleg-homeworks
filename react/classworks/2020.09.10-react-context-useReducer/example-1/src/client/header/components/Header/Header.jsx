import React from 'react';
import classes from './Header.module.scss';
import { NavTop } from '../NavTop';
import { Navbar } from '../Navbar';

export const Header = () => {
  const { header } = classes;
  return (
    <header className={header}>
      <NavTop />
      <Navbar />
    </header>
  );
};
