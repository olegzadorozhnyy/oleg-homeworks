import React from 'react';
import classes from './NavTop.module.scss';
import { NavTopContacts } from '../NavTopContacts';
import { SocialIconsBlock } from '../../../../shared/components/SocialIconsBlock';

export const NavTop = () => {
  const { nav } = classes;
  return (
    <nav className={nav}>
      <div className="container">
        <div className="d-flex justify-content-between flex-wrap">
          <NavTopContacts />
          <SocialIconsBlock />
        </div>
      </div>
    </nav>
  );
};
