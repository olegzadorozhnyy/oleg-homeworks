import React from 'react';
import classes from './NavbarMenu.module.scss';

const links = [
  {
    to: '/',
    label: 'Home',
  },
  {
    to: '/',
    label: 'Women',
  },
  {
    to: '/',
    label: 'Men',
  },
  {
    to: '/',
    label: 'Kids',
  },
  {
    to: '/',
    label: 'Jewellery',
  },
  {
    to: '/',
    label: 'Accessories',
  },
];

export const NavbarMenu = () => {
  const { navbarMenu, item, link } = classes;

  const listItems = links.map(({ to, label }) => (
    <li className={item} key={label}>
      <a href={to} className={link}>
        {label}
      </a>
    </li>
  ));

  return <ul className={navbarMenu}>{listItems}</ul>;
};
