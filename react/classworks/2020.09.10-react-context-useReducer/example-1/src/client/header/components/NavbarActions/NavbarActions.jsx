import React, { useState, useCallback } from 'react';

import { Search } from '../../../search/components/Search';
import { ShoppingCartWidget } from '../../../shopping-cart/components/ShoppingCartWidget';
import { Drawer } from '../../../../shared/components/Drawer';
import classes from './NavbarActions.module.scss';
import { Logo } from '../../../../shared/components/Logo';

export const NavbarActions = () => {
  const [open, setOpen] = useState(false);
  const handleOpen = useCallback(() => setOpen(true), []);
  const handleClose = useCallback(() => setOpen(false), []);

  const { wrapper } = classes;

  return (
    <div className={wrapper}>
      <ShoppingCartWidget />
      <Search />
      <button type="button" className="btn" onClick={handleOpen}>
        <i className="fa fa-bars" aria-hidden="true" />
      </button>
      <Drawer anchor="right" open={open} onClose={handleClose} logo={<Logo />}>
        <ul>
          <li>
            <a href="/">Home</a>
          </li>
        </ul>
      </Drawer>
    </div>
  );
};
