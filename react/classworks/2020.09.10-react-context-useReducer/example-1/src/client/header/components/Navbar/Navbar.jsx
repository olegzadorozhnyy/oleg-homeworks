import React from 'react';

import classes from './Navbar.module.scss';
import { Logo } from '../../../../shared/components/Logo';
import { NavbarMenu } from '../NavbarMenu';
import { NavbarActions } from '../NavbarActions';

export const Navbar = () => {
  const { navbar } = classes;
  return (
    <nav className={navbar}>
      <div className="container d-flex justify-content-between align-items-center flex-wrap">
        <Logo />
        <NavbarMenu />
        <NavbarActions />
      </div>
    </nav>
  );
};
