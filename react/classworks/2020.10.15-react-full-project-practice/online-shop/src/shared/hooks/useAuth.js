import React from 'react';
import { useSelector } from "react-redux";

const useAuth = () => {
    const isAuthenticated = useSelector( state => state.auth.is_authenticated )
    return {isAuthenticated}
}

export default useAuth