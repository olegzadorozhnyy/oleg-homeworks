import axios from "axios";
import {FETCH_PRODUCTS_REQUEST, FETCH_PRODUCTS_SUCCESS, FETCH_PRODUCTS_FAILED} from "./actionTypes";

export const productsRequestCreator = (page = 1, limit = 3) => {
    const url = "https://students-store-app.herokuapp.com/api/v1/products";
    //const fullUrl = `${url}?page=${page}&limit=${limit}`;
    const fullUrl = `${url}?page=${page}&limit=${limit}`;


    return async (dispatch) => {
        dispatch({type: FETCH_PRODUCTS_REQUEST})
        try {
            const {data} = await axios.get(fullUrl);
            dispatch({type: FETCH_PRODUCTS_SUCCESS, payload: data});
        }
        catch (error) {
            dispatch({type: FETCH_PRODUCTS_FAILED, payload : error})
        }
    }
}

/*
const productsService = new ProductsService();

const fetchProducts = () => async (dispatch) => {
    try {
        dispatch(fetchProductsRequested());
        const products = await productsService.fetchProducts();
        dispatch(fetchProductsSuccess({ products }));
    } catch (err) {
        dispatch(fetchProductsFailure({ payload: err }));
    }
};*/
