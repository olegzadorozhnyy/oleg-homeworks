import {FETCH_PRODUCTS_REQUEST, FETCH_PRODUCTS_FAILED, FETCH_PRODUCTS_SUCCESS} from "./actionTypes";
const initialState = {
    pager : {},
    products : [],
    loading : true,
    errors : null
}

export const productsReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case FETCH_PRODUCTS_REQUEST:
            return {...state, loading: true};
        case FETCH_PRODUCTS_SUCCESS:
            return {...state, loading: false, products: payload.result, pager : payload.pager};
        case FETCH_PRODUCTS_FAILED:
            return {...state, errors: payload, loading: false};
        default:
            return state;
    }
}