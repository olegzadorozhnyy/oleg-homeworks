import React from "react";

const ProductItem = ({thumbnail, title, price}) => {
    return(
        <div className="product-item">
            <h4>{title}</h4>
            <img src={thumbnail} width="100px"/>
            <p>price: {price}</p>
        </div>
    )
}

export default ProductItem