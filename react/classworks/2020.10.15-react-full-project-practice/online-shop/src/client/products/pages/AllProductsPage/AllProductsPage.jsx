import React, {useEffect} from 'react';
import {productsRequestCreator} from "../../reducers/actionCreators";
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import ProductItem from "../../components/ProductItem";

const AllProductsPage = () => {
    const dispatch = useDispatch();
    const action = productsRequestCreator();

    useEffect(()=>{
        dispatch(action);
    }, [])

    const {products, error, loading} = useSelector(state => state.productStore, shallowEqual);
    const productsElems = products.map(item => <ProductItem {...item} />);
    const content = loading ? <span>Loading...</span> : productsElems;
    const errorsMessage = error ? <span>error</span> : null;


    return (
        <section>
            <h2>All Products Page</h2>
            <div className="products">
                {content}
                {errorsMessage}
            </div>
        </section>
    )
}

export default AllProductsPage;