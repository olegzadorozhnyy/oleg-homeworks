import React from 'react'
import {linkProps} from "./linkProps";
import {Link, NavLink} from "react-router-dom"
import { v4 as uuidv4 } from 'uuid';
import useAuth from "../../../../shared/hooks/useAuth";
import AuthProfile from "../../../auth/components/AuthProfile/AuthProfile";

const Navbar = () => {
    const {isAuthenticated} = useAuth();
    const linksElements = linkProps.map(({to, text}) =>

        <li><NavLink
            to={to}
            key={uuidv4()}
            className='link'
            activeClassName="link--active">
            {text}
        </NavLink></li>);
    const authContent = isAuthenticated ? <Link to="/profile">Profile</Link> : <AuthProfile/>
    return (
        <nav>
            <ul>
                {linksElements}
            </ul>
            {authContent}
        </nav>
    )
}

export default Navbar;