export const linkProps = [
    {
        to: '/',
        text: 'Главная страница'
    },
    {
        to: '/products',
        text: 'Список товаров'
    },
    {
        to: '/cart',
        text: 'Корзина'
    },
    {
        to: '/contacts',
        text: 'Контакты'
    },
]