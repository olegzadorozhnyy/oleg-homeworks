import React from 'react'

const NoMatchPage = () => {

    return (
        <section>
            <h2>404</h2>
        </section>
    )
}

export default NoMatchPage;