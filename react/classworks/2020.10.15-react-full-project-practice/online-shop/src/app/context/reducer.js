import {SET_CURRENCY, SET_LANG} from "./constants";

export const reducer = (state, {type, payload}) => {
    switch (type) {
        case SET_CURRENCY :
            return {...state, currency: payload}
        case SET_LANG :
            return {...state, lang: payload}
        default : return state;
    }
}