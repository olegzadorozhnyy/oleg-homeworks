import React from 'react';
import {Provider} from "react-redux";
import {BrowserRouter as Router} from "react-router-dom";

import ContextProvider from "../../context/Provider";
import Navbar from "../../../client/navbar/components/Navbar";
import {AppRoutes} from "../../routers/AppRoutes"
import {store} from "../../store/store"



const App = () => {
  return (
        <Provider store={store}>
          <Router>
              <ContextProvider>
                  <Navbar />
                  <AppRoutes />
              </ContextProvider>
          </Router>
        </Provider>
  );
}

export default App;
