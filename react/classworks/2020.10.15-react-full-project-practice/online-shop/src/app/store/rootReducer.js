import {combineReducers} from "redux";
import {productsReducer} from "../../client/products/reducers/productsReducer";
import {loginFormReducer as auth} from "../../client/auth/reducers/loginFormReducer"

export default combineReducers({
    productStore: productsReducer,
    auth
})