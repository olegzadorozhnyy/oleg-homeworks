import React from 'react'
import {Switch, Route} from "react-router-dom";

import HomePage from "../../client/home/pages/HomePage";
import AllProductsPage from "../../client/products/pages/AllProductsPage";
import CategoryProductsPage from "../../client/products/pages/CategoryProductsPage";
import CartPage from "../../client/cart/pages/CartPage";
import ProductsPage from "../../client/products/pages/ProductsPage";
import ContactsPage from "../../client/singleInfo/pages/ContactsPage";
import NoMatchPage from "../../client/noMatch/pages/NoMatchPage";
import LoginPage from "../../client/auth/components/LoginPage";
import ProfilePage from "../../client/profile/pages/ProfilePage";
import PrivateRoute from "../../shared/components/PrivateRoute/PrivateRoute";
import useAuth from "../../shared/hooks/useAuth"

export const AppRoutes = () => {
    const {isAuthenticated} = useAuth();
    return (
        <Switch>
            <Route exact path="/" >
                <HomePage/>
            </Route>
            <Route exact path="/products" >
                <AllProductsPage/>
            </Route>
            <Route exact path="/products/:id" >
                <ProductsPage/>
            </Route>
            <Route exact path="/cart" >
                <CartPage />
            </Route>
            <Route exact path="/register">
                {/* <ContactsPage /> */}
            </Route>
            <PrivateRoute component={ProfilePage} isAuthenticated = {isAuthenticated} path="/profile">

            </PrivateRoute>
            <Route exact path="/login">
                <LoginPage />
            </Route>
            <Route exact path="/contacts" >
                <ContactsPage/>
            </Route>
            <Route exact path="*">
                <NoMatchPage/>
            </Route>
        </Switch>
    )
}