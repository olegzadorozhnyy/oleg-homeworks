import React from 'react';
import {Formik, Field, Form} from "formik";
import {fields} from "./fields";

const LoginForm = ()=> {
    const initialValues = {
        email: "",
        password: "",
        submit: "Вход"
    };
    const requireValidate = (values, errorRequireMessage) => {
        const errors = {}
        for(const [key, value] of Object.entries(values)) {
            if (!value) {
                errors[key] =errorRequireMessage;
            }
        }
        return errors;
    }

    const validate = (values) => {
        const errors = requireValidate(values, "Поле обязательно к заполнению");
        if (values.login.length < 2 && value.login) {
            errors.login = "Логин должен быть не меньше 2х букв"
        }
        if (values.login.length < 2 && value.login) {
            errors.login = "Логин должен быть не меньше 2х букв"
        }
    };

    const onSubmit = (values)=> {
        console.log(values);
    };
  
    return (
      <Formik {...{ initialValues, onSubmit }}>
        <Form>
          <Field {...fields.email} />
          <Field {...fields.password} />
          <button type="submit">Submit</button>   
        </Form>
      </Formik>
    );
  };

  export default LoginForm;