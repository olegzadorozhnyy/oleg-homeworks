import React, {useState} from 'react';
import {useFormik} from "formik";

import Input from "../../../../shared/components/Input";
import {fields} from "./fields";

const LoginForm = ()=> {
  const {getFieldProps, handleSubmit} = useFormik({
    initialValues: {
      email: "",
      password: ""
    },
    onSubmit(values) {

    }
  });
  
    return (
      <form onSubmit={handleSubmit}>
        <Input {...fields.email} 
          {...getFieldProps("email")} />
        <Input {...fields.password} 
          {...getFieldProps("password")} />            
        <button type="submit">Submit</button>             
      </form>
    );
  };

  export default LoginForm;