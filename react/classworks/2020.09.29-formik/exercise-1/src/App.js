import React from 'react';
import './App.css';
import ProductForm from "./shared/components/ProductFom"

function App() {
  return (
    <div className="App">
      <ProductForm/>
    </div>
  );
}

export default App;
