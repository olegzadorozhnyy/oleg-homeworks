import React from "react";
import {useFormik} from "formik";
import {fields} from "./fields";


const ProductForm = () => {
    const formik = useFormik({
        initialValues: {
            name: "",
            articul: "",
            price: "",
            category: "laptop",
            stock : "yes",
            delivery: false,
            description: ""
        },
        onSubmit(values) {
            console.log(values)
        }
    });
    const categoryOptions = fields.category.options.map(({value,text}) => <option value={value}>{text}</option>)
    return(
        <form onSubmit={formik.handleSubmit}>
            <input {...fields.name} onChange={formik.handleChange}/>
            <input {...fields.articul} onChange={formik.handleChange}/>
            <input {...fields.price} onChange={formik.handleChange}/>
            <select {...fields.category} onChange={formik.handleChange}>
                {categoryOptions}
            </select>
            <input checked {...fields.stock} value="yes" onChange={formik.handleChange}/>
            <input {...fields.stock} value="no" onChange={formik.handleChange}/>
            <input {...fields.delivery} onChange={formik.handleChange}  />
            <textarea {...fields.description} onChange={formik.handleChange}></textarea>
            <input value="Добавить продукт" type="submit"/>
        </form>
    )
}

export default ProductForm;