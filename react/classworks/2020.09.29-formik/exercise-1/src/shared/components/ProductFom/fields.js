export const fields = {
    name: {
        type: "text",
        name: "name",
        placeholder: "name"
    },
    articul: {
        type: "text",
        name: "articul",
        placeholder: "articul"
    },
    price: {
        type: "text",
        name: "price",
        placeholder: "price"
    },
    category:  {
        name: "category",
        options: [{
            value: "laptop",
            text: "laptop"
        },{
            value: "tablet",
            text: "tablet"
        },{
            value: "phone",
            text: "phone"
        }]
    },
    stock:{
        type: "radio",
        name: "stock",
        label: "Stock"
    },
    delivery: {
        name: "delivery",
        type: "checkbox",
        label: "delivery"
    },
    description : {
        name : "description",
        placeholder : "description"
    }




}