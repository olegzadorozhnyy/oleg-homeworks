import {initialState} from "./initialState";

export const reducer = (state = initialState, action)=> {
    switch(action.type) {
        case "LOGOUT":
            return {name: "", middleName: "", lastName: ""};
        case "CHANGE_NAME":
            return {...state, name: action.payload};
        default:
            return state;
    }  
};