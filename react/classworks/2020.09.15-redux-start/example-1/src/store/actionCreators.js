import {LOGOUT, CHANGE_NAME} from "./actions";

export const createLogOut = (payload)=> ({type: LOGOUT, payload});


export const createChangeName = (payload)=> {
    return {type: CHANGE_NAME, payload};
};