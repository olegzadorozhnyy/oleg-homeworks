import React from 'react';
import {Link} from "react-router-dom";

import store from "../../../../store/store";
import {createLogOut, createChangeName} from "../../../../store/actionCreators";

const UserProfile = ()=> {
    const {name, middleName, lastName} = store.getState();

    const logOut = () => store.dispatch(createLogOut());

    const newName =  "Момон";

    const changeName = () => store.dispatch(createChangeName(newName));

    store.subscribe(()=> console.log(store.getState()));

    return (
        <div>
            <Link to="/my-account">{name} {middleName} {lastName}</Link>
            <button onClick={logOut}>Выйти</button>
            <button onClick={changeName}>Сменить Имя</button>
        </div>
    
    )
}

export default UserProfile;