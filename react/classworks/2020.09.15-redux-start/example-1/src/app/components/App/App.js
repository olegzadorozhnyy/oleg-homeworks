import React from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import './App.scss';
import Navbar from '../../../client/Navbar/components/Navbar';
import {AppRoutes} from "../../routes/AppRoutes";



function App() {

  return (
    <Router>
      <div className="App">
        <Navbar />
        <AppRoutes  />
      </div>
    </Router>
  );
}

export default App;
