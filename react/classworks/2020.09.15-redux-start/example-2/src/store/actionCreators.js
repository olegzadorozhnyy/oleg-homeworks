import {LOGOUT, CHANGE_NAME, CHANGE_LAST_NAME} from "./actions";

export const createLogOut = (payload)=> ({type: LOGOUT, payload});

export const createChangeName = (payload)=> {type: CHANGE_NAME, payload};

export const createChangeLastName = (payload)=> ({type: CHANGE_LAST_NAME, payload});