import {createLogOut, createChangeName, createChangeLastName} from "./actionCreators";
import store from "./store";

export const logOut = () => store.dispatch(createLogOut());
export const changeName = (value)=> store.dispatch(createChangeName(value));
export const changeLastName = (value)=> store.dispatch(createChangeLastName(value));