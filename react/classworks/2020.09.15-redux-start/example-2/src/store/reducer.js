import {initialState} from "./initialState";

export const reducer = (state = initialState, action)=> {
    switch(action.type) {
        case "LOGOUT":
            return {name: "", middleName: "", lastName: ""};
        case "CHANGE_NAME":
            return {...state, name: action.payload};
            case "CHANGE_LAST_NAME":
                return {...state, lastName: action.payload};            
        default:
            return state;
    }  
};