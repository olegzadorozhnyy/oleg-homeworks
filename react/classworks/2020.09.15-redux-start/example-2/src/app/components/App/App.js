import React from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import {Provider} from "react-redux";
import './App.scss';
import Navbar from '../../../client/Navbar/components/Navbar';
import {AppRoutes} from "../../routes/AppRoutes";

import store from "../../../store/store";

function App() {

  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <Navbar />
          <AppRoutes  />
        </div>
      </Router>
    </Provider>

  );
}

export default App;
