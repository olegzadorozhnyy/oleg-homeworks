import React from 'react';
import {useDispatch} from "react-redux";

import {createLogOut, createChangeName, createChangeLastName} from "../../../../store/actionCreators";

const HomePage = () => {
    const dispatch = useDispatch();

    const newName =  "Момон";
    const newLastName =  "Аврора";

    const logOut = () => dispatch(createLogOut());
    const changeN = ()=> dispatch(createChangeName(newName));
    const changeL = ()=> dispatch(createChangeLastName(newLastName));

    return (
        <div className="container">
            <h1 className="text-center">Добро пожаловать, господин!</h1>
            <button onClick={logOut}>Выйти</button>
            <button onClick={changeN}>Сменить Имя</button>
            <button onClick={changeL}>Сменить Фамилию</button>
        </div>
    )
};

export default HomePage;