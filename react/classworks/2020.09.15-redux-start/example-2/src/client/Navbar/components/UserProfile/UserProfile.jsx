import React from 'react';
import {Link} from "react-router-dom";
import {useSelector, shallowEqual} from "react-redux";

const UserProfile = ()=> {
    const {name, middleName, lastName} = useSelector(state => (state), shallowEqual);

    return  <Link to="/my-account">{name} {middleName} {lastName}</Link>;
}

export default UserProfile;