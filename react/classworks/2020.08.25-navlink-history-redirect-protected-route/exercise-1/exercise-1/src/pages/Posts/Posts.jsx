import React , {useState, useEffect} from 'react';
import PostItem from "../../components/PostItem";
import {useHistory, useRouteMatch} from "react-router-dom";
const Posts = () => {

const history = useHistory();
const [posts, setPosts] = useState([]);
const {path} = useRouteMatch();

useEffect(() => {
    fetch("http://danit.com.ua/posts")
    .then(response => response.json())
    .then(setPosts)
},[])

const showSinglePost = id => history.push(`${path}${id}`);


const postElements = posts.map((post, id) => <PostItem {...post} 
handleClick={() => showSinglePost(id)} />);

const postReady = (posts.length) ? postElements : <h2>Loading...</h2>;
    return (
        <section className="breweries" id="breweries">
          <ul>
        {postReady}
        </ul>
        </section>
    )
}

export default Posts;