import React, {useState, useEffect} from 'react';
import {useParams} from "react-router-dom";

const SinglePost = () => {
     
const [post, setPost] = useState([]);
const {id} = useParams();

useEffect(() => {
    fetch(`http://danit.com.ua/posts/${id}`)
    .then(response => response.json())
    .then(setPost)
},[])
    
    const {src, title, "full-description": fullDescription} = post;
    return (
        <li>          
        <figure>
          <img src={src} alt="Several hands holding beer glasses" />
          <figcaption><h3>{title}</h3></figcaption>
        </figure>
        <p>
          {fullDescription}
        </p>
        <button>More</button>
      </li>
    )
}

export default SinglePost;