import React from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import './App.css';

import NavBar from "./components/NavBar";
import AppRoutes from './AppRoutes';

function App() {
  return (
    <main className="wrapper">
    <Router>
      <NavBar />
      <AppRoutes />
    </Router>
    </main>
  );
}

export default App;
