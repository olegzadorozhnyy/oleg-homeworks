import React from 'react'
import {NavLink} from "react-router-dom";

const NavBar = () => {
    
    return (
        <nav className="navbar">
            <ul className="navbar-nav">
            <li><NavLink exact to="/" className="navbar-nav-link" activeClassName="active">
                    Home
                </NavLink></li>
                <li><NavLink exact to="/posts" className="navbar-nav-link" activeClassName="active">
                    Posts
                </NavLink></li>
                <li><NavLink exact to="/contacts" className="navbar-nav-link" activeClassName="active">
                    Contacts
                </NavLink></li>
            </ul>   
        </nav>

    )
}

export default NavBar;