import React from 'react'

const PostItem = (props) => {
    
    const {handleClick, src, title, "short-description": shortDescription} = props;

   
    return (
        <li>          
        <figure>
          <img src={src} alt="Several hands holding beer glasses" />
          <figcaption><h3>{title}</h3></figcaption>
        </figure>
        <p>
          {shortDescription}
        </p>
        <button onClick={handleClick} >More</button>
      </li>
    )
}

export default PostItem;