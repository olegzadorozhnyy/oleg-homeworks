import React from 'react';

import {Route, Switch} from "react-router-dom";

import Home from "../pages/Home";
import Posts from "../pages/Posts"
import Contacts from "../pages/Contacts";
import SinglePost from "../pages/SinglePost";


const AppRoutes = ()=> {

    return(
        <div className="container">
        <Switch>
            <Route exact path="/">
                <Home />
            </Route>
            <Route exact path="/posts/">
                <Posts  />
            </Route>      
            <Route exact path="/contacts">                      
                <Contacts />
            </Route>
            <Route exact path="/posts/:id">                      
                <SinglePost />
            </Route>
            </Switch>   
        </div>
        
    )
}


export default AppRoutes;