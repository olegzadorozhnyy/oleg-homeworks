import React from 'react'

const AboutPage = ({history})=> {
    const onToHomePage = () => history.push("/");
    return (
        <>
        <h2>About us</h2>
        <button onClick={onToHomePage}>To Home Page</button>
        </>
    )
}

export default AboutPage;