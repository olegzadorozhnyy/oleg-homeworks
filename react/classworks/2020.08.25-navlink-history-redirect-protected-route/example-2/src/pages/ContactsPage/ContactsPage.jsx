import React from 'react';
import {useHistory} from "react-router-dom"

const ContactsPage = ()=> {
    let history = useHistory()
    const onToHomePage = () => history.goBack();
    return (
        <>
        <h2>Our contacts</h2>
        <button onClick={onToHomePage}>To Home Page</button>
        </>
    )
}

export default ContactsPage;