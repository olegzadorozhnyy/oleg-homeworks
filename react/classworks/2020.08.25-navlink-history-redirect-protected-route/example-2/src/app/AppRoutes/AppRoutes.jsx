import React from 'react';

import {Route, Switch} from "react-router-dom";
import HomePage from "../../pages/HomePage";
import AboutPage from "../../pages/AboutPage";
import ContactsPage from "../../pages/ContactsPage";

const AppRoutes = ()=> {
    return(
        <div className="container">
        <Switch>
            <Route exact path="/">
                <HomePage />
            </Route>
            <Route exact path="/about" component={AboutPage} />
            {/* if(this.props.component) {
                return component match={match}
            } else {
                return children
            } */}
            <Route exact path="/contacts">
                <ContactsPage />
            </Route>                        
        </Switch>
        </div>

    )
}

export default AppRoutes;