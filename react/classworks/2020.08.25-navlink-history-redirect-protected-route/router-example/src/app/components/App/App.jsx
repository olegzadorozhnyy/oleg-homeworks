import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { HelmetProvider } from 'react-helmet-async';

import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../shared/styles/scss/style.scss';
import { CommonLayout } from '../../../shared/layouts/CommonLayout';
import { AppRoutes } from '../../routes';

export const App = () => {
  return (
    <BrowserRouter>
      <HelmetProvider>
        <CommonLayout>
          <AppRoutes />
        </CommonLayout>
      </HelmetProvider>
    </BrowserRouter>
  );
};
