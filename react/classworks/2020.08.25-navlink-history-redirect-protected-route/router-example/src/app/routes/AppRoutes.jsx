import React, { useState, useCallback } from 'react';
import { Switch, Route, Redirect, useLocation } from 'react-router-dom';

import { HomePage } from '../../client/home/pages/HomePage';
import { NoMatch } from '../../shared/components/NoMatch';
import { ProtectedRoute } from '../../shared/components/PrivateRoute/PrivateRoute';
import { GamesDetailsPage } from '../../client/games/pages/GamesDetailsPage';
import { GamesPage } from '../../client/games/pages/GamesPage';
import { LoginPage } from '../../client/personal-area/pages/LoginPage';
import { AccountsPage } from '../../client/personal-area/pages/AccountsPage';

export const AppRoutes = () => {
  const location = useLocation();
  const [login, setLogin] = useState(
    Boolean(localStorage.getItem('refreshToken')) || false,
  );

  const loginIn = useCallback(() => setLogin(true), []);

  return (
    <Switch>
      <Route exact path="/">
        <HomePage />
      </Route>

      <Route exact path="/sign-in/">
        <LoginPage setLogin={loginIn} />
      </Route>

      <Route exact path="/games/">
        <GamesPage />
      </Route>

      <Route exact path="/games/:slug/">
        <GamesDetailsPage />
      </Route>

      <ProtectedRoute
        path="/accounts/"
        isAuthenticated={login}
        component={AccountsPage}
      />    

      <Route path="/404" component={NoMatch} />
      <Route path="*">
        <Redirect from={location.pathname} to="/404" />
      </Route>
    </Switch>
  );
};
