import React from 'react';

import { useFetchData } from '../../../../shared/hooks/use-fetch-data';
import { ListItems } from '../../../../shared/components/ListItems';
import { Spinner } from '../../../../shared/components/Spinner';
import Seo from '../../../../shared/components/Seo/Seo';

export const HomePage = () => {
  const { data, loading, error } = useFetchData(
    `https://api.rawg.io/api/publishers`,
  );
  const { results } = data;
  const games = data && <ListItems data={results} />;

  if (loading) {
    return <Spinner />;
  }

  return (
    <>
      <div className="container mt-5 py-5">
        <Seo description="Home Page" title="Home Page" />
        <h1 className="mb-4">Publishers</h1>
      </div>
      {games}
      {error}
    </>
  );
};
