import React, { useState } from 'react';
import { Link, NavLink } from 'react-router-dom';

const Navbar = () => {
  const [isLogin] = useState(localStorage.getItem('refreshToken'));
  const [links] = useState([
    {
      to: '/',
      label: 'Home',
    },
    {
      to: '/games',
      label: 'Games',
    },
    {
      to: '/sign-in/',
      label: 'SignIn',
    },
    {
      to: '/accounts/',
      label: 'Accounts',
    },
  ]);

  const linkItems = links.map(item => {
    if (isLogin && item.to === '/sign-in/') return null;
    if (!isLogin && item.to === '/accounts/') return null;
    return (
      <li className="nav-item" key={item.label}>
        <NavLink
          exact
          className="nav-link"
          activeClassName="nav-link-active"
          aria-current="page"
          to={item.to}>
          {item.label}
        </NavLink>
      </li>
    );
  });

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light fixed-top">
      <div className="container">
        <Link className="navbar-brand" to="/">
          GamesDB
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation">
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav ml-auto">{linkItems}</ul>
        </div>
      </div>
    </nav>
  );
};

export { Navbar };
