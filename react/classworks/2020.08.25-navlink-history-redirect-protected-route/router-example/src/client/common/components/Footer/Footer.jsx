import React from 'react';

export const Footer = () => {
  return (
    <footer className="bg-light mt-5">
      <div className="container">
        <div className="row">
          <div className="col-6 offset-3 py-3 text-center">
            <strong>GamesDB &copy; {new Date().getFullYear()}</strong>
          </div>
        </div>
      </div>
    </footer>
  );
};
