import React from 'react';
import Seo from '../../../../shared/components/Seo/Seo';

export const AccountsPage = () => {
  return (
    <>
      <Seo description="Accounts" title="Accounts" />
      <div className="container py-5 mt-5">
        <div className="row">
          <div className="col-6 offset-3 text-center">I am Protected!</div>
        </div>
      </div>
    </>
  );
};
