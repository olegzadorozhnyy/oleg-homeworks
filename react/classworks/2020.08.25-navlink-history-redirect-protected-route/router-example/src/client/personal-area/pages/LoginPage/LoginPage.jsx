import React from 'react';

import { Form } from '../../components/Form';
import Seo from '../../../../shared/components/Seo/Seo';

export const LoginPage = ({ setLogin }) => {
  return (
    <>
      <Seo description="Sign In" title="Sign In" />
      <div className="container mt-5">
        <div className="row">
          <div className="col-6 offset-3 py-5">
            <Form setLogin={() => setLogin(true)} />
          </div>
        </div>
      </div>
    </>
  );
};
