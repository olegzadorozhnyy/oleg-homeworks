import React, { useState, useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { CSSTransition } from 'react-transition-group';

import { useSubmit } from '../../../../shared/hooks/use-submit/useSubmit';
import { FormControl } from '../FormControl';

import { fields } from './fields';
import { Spinner } from '../../../../shared/components/Spinner';

export const Form = ({ setLogin }) => {
  const history = useHistory();
  const [values, setValues] = useState({
    email: '',
    password: '',
  });

  const { state, sendData, clearMessage } = useSubmit();
  const { loading, errors } = state;

  const handleChange = useCallback(
    event => {
      setValues({
        ...values,
        [event.target.name]: event.target.value,
      });
    },
    [values],
  );

  const handleSubmit = event => {
    event.preventDefault();
    sendData(
      `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${process.env.REACT_APP_FIRE_BASE_URL}`,
      values,
    )
      .then(() => {
        setLogin();
        history.push('/accounts/');
      })
      .catch(() => {
        clearMessage();
      });
  };

  const formItems = fields.map(props => (
    <FormControl key={props.name} handleChange={handleChange} {...props} />
  ));

  const spinner = loading && <Spinner />;

  return (
    <>
      {spinner}
      <h1 className="text-center mb-5">Sign in</h1>
      <form onSubmit={handleSubmit} className="mb-5">
        {formItems}
        <CSSTransition
          in={Boolean(errors)}
          timeout={300}
          classNames="alert"
          unmountOnExit>
          <div className="alert alert-danger d-block" role="alert">
            {errors}
          </div>
        </CSSTransition>
        <button
          type="submit"
          className="btn btn-primary btn-block"
          disabled={loading}>
          Sign In
        </button>
      </form>
    </>
  );
};
