import React from 'react';
import { Pagination } from '../../../../shared/components/Pagination';
import { useQuery } from '../../../../shared/hooks/use-query/use-query';
import { useFetchData } from '../../../../shared/hooks/use-fetch-data';
import { ListItems } from '../../../../shared/components/ListItems';
import { Spinner } from '../../../../shared/components/Spinner';
import Seo from '../../../../shared/components/Seo/Seo';

export const GamesPage = () => {
  const query = useQuery();
  const { data, loading, error } = useFetchData(
    `https://api.rawg.io/api/games?page_size=20&page=${query.get('page') || 1}`,
  );
  const { results, seo_title, seo_h1 } = data;
  const games = data && <ListItems data={results} />;

  if (loading) {
    return <Spinner />;
  }

  return (
    <>
      <div className="container mt-5 py-5">
        <Seo description={seo_title} title={seo_title} />
        <h1 className="mb-4">{seo_h1}</h1>
      </div>
      {games}
      {error}
      <Pagination />
    </>
  );
};
