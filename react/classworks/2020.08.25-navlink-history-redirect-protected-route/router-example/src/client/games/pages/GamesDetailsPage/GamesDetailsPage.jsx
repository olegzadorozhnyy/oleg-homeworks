import React, { Fragment } from 'react';
import { Link, useParams } from 'react-router-dom';

import { useFetchData } from '../../../../shared/hooks/use-fetch-data';
import classes from './GamesDetailsPage.module.scss';
import Seo from '../../../../shared/components/Seo/Seo';
import { Spinner } from '../../../../shared/components/Spinner';

export const GamesDetailsPage = () => {
  const { slug } = useParams();
  const { data, loading, error } = useFetchData(
    `https://api.rawg.io/api/games/${slug}`,
  );
  const {
    background_image,
    background_image_additional,
    released,
    name,
    developers,
    description,
    description_raw,
    genres,
  } = data;

  const developersArr =
    developers &&
    developers.map(({ slug, name }) => (
      <Link to={`/developers/${slug}`} className={classes.developer} key={slug}>
        {name}
      </Link>
    ));

  const genresArr =
    genres &&
    genres.map(({ name, slug }) => (
      <Fragment key={slug}>
        <Link to={`/genres/${slug}`}>
          {name}
        </Link>
        {' / '}
      </Fragment>
    ));

  if (loading) {
    return <Spinner />;
  }

  if (error) {
    return <p>Ops...</p>;
  }

  return (
    <>
      <Seo
        description={description_raw && description_raw.slice(0, 245)}
        title={name}
      />
      <div
        style={{
          position: 'absolute',
          top: 0,
          zIndex: -1,
          minHeight: '56vh',
          width: '100%',
          background: `linear-gradient(rgba(0, 0, 0, .6), rgba(0, 0, 0, .6)), url(${background_image_additional}) no-repeat fixed center/cover`,
        }}
      />
      <div className={`container py-4 ${classes.container}`}>
        <div className="row">
          <div className="col-4">
            <img src={background_image} className="img-fluid" alt={name} />
          </div>
          <div className="col-8">
            <h1 className={classes.title}>{name}</h1>
            <p className={classes.date}>{released}</p>
            {developersArr}
          </div>
        </div>
        <div className="row">
          <div className="col-4 mt-2">
            <span>Genres: </span>
            {genresArr}
          </div>
          <div className="col-8">
            <h4 className="mb-4">Description:</h4>
            <div
              dangerouslySetInnerHTML={{
                __html: description,
              }}
            />
          </div>
        </div>
      </div>
    </>
  );
};
