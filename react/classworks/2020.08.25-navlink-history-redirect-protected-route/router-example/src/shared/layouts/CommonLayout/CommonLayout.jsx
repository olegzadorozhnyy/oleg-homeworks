import React from 'react';
import PropTypes from 'prop-types';

import { Navbar } from '../../../client/common/components/Navbar';
import { Footer } from '../../../client/common/components/Footer';

export const CommonLayout = ({ children }) => {
  return (
    <>
      <Navbar />
      <main>{children}</main>
      <Footer />
    </>
  );
};

CommonLayout.propTypes = {
  children: PropTypes.node.isRequired,
};
