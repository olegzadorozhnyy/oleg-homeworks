import {
  SUBMIT_FORM_DATA_FAILURE,
  SUBMIT_FORM_DATA_REQUEST,
  SUBMIT_FORM_DATA_SUCCESS,
  CLEAR_ERRORS_MESSAGE,
} from '../submit.constants';
import { AuthService } from '../../../../client/personal-area/services/auth.service';

const { handleError } = new AuthService();

export const submitFormRequested = () => ({
  type: SUBMIT_FORM_DATA_REQUEST,
});

export const submitFormSuccess = newData => ({
  type: SUBMIT_FORM_DATA_SUCCESS,
  payload: newData,
});

export const submitFormError = errors => ({
  type: SUBMIT_FORM_DATA_FAILURE,
  payload: handleError(errors),
});

export const clearErrorsMessage = () => ({
  type: CLEAR_ERRORS_MESSAGE,
});
