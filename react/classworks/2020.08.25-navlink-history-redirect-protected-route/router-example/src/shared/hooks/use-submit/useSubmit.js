import { useCallback, useReducer } from 'react';
import axios from 'axios';

import { submitReducer } from './reducer/submit.reducer';
import {
  clearErrorsMessage,
  submitFormError,
  submitFormRequested,
  submitFormSuccess,
} from './actions/submit.action';

const InitialState = {
  data: {},
  loading: false,
  errors: false,
};

export const useSubmit = () => {
  const [state, dispatch] = useReducer(submitReducer, InitialState);

  const clearMessage = useCallback(
    () => setTimeout(() => dispatch(clearErrorsMessage()), 5000),
    [dispatch],
  );

  const sendData = async (url = '', data = {}) => {
    dispatch(submitFormRequested());
    data.returnSecureToken = true;
    await axios
      .post(`${url}`, data)
      .then(response => {
        localStorage.setItem('refreshToken', response.data.refreshToken);
        localStorage.setItem('expiresIn', response.data.expiresIn);
        dispatch(submitFormSuccess(response.data));
      })
      .catch(errors => {
        dispatch(submitFormError(errors.response.data));
      });
  };

  return {
    sendData,
    state,
    clearMessage,
  };
};
