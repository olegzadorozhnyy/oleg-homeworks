import React from 'react';

const NoMatch = () => {
  return (
    <div className="mt-5 py-5 text-center">
      <h1>404</h1>
      <p>We did not find the page you were looking for.</p>
    </div>
  );
};

export { NoMatch };
