import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';

export const ProtectedRoute = ({
  component: Component,
  isAuthenticated,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={props =>
        isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect
            from={props.location.pathname}
            to={{
              pathname: '/sign-in/',
            }}
          />
        )
      }
    />
  );
};

ProtectedRoute.defaultProps = {
  location: {},
};

ProtectedRoute.propTypes = {
  component: PropTypes.node.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  location: PropTypes.object,
};
