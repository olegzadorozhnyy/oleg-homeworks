import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classes from './CardItems.module.scss';

const CardItems = ({ name, slug, background_image, image_background }) => {
  return (
    <div className="col-md-4 col-lg-3 mb-5">
      <Link className={`card ${classes.card}`} to={`/games/${slug}`}>
        <img
          src={background_image || image_background}
          className={`card-img-top ${classes.image}`}
          alt={name}
        />
        <div className="card-body">
          <h5 className={`card-title text-center ${classes.title}`}>{name}</h5>
        </div>
      </Link>
    </div>
  );
};

CardItems.defaultProps = {
  image_background: null,
  background_image: null,
};

CardItems.propTypes = {
  slug: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  name: PropTypes.string.isRequired,
  background_image: PropTypes.string,
  image_background: PropTypes.string,
};

export { CardItems };
