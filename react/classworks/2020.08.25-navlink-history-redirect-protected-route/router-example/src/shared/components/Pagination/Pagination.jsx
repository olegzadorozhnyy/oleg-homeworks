import React from 'react';
import { Link } from 'react-router-dom';
import { useQuery } from '../../hooks/use-query/use-query';

export const Pagination = () => {
  const query = useQuery('page');
  const page = Number(query.get('page'));
  return (
    <div className="container">
      <nav
        aria-label="bottom-pagination"
        className="d-flex justify-content-center">
        <ul className="pagination pagination-lg">
          <li className={`page-item ${!page || (page === 1 && 'disabled')}`}>
            <Link
              to={{
                search: `?page=${page - 1}`,
              }}
              className="page-link"
              tabIndex="-1"
              aria-disabled="true">
              Previous
            </Link>
          </li>
          <li className="page-item">
            <Link
              className="page-link"
              to={{
                search: `?page=${!page ? 2 : page + 1}`,
              }}>
              Next
            </Link>
          </li>
        </ul>
      </nav>
    </div>
  );
};
