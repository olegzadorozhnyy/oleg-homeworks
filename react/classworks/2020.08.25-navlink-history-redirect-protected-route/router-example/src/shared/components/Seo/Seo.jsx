import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet-async';

const Seo = ({ title, description }) => {
  return (
    <Helmet>
      <title>{title || 'default title'}</title>
      <meta name="description" content={description || 'default description'} />
    </Helmet>
  );
};

Seo.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

export default Seo;
