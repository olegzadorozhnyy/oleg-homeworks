import React from 'react';
import PropTypes from 'prop-types';
import { CardItems } from '../CardItems';

const ListItems = ({ data }) => {
  const listItems =
    data && data.map(props => <CardItems key={props.id} {...props} />);

  return (
    <div className="container">
      <div className="row">{listItems}</div>
    </div>
  );
};

ListItems.defaultProps = {
  data: [],
};

ListItems.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
};

export { ListItems };
