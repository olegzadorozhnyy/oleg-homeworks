import React from "react";

const ProductItem = ({id, name, category}) => {
    return(
        <div className="product-item">
            <h4>{name}</h4>
            <p>id: {id}</p>
            <p>category: {category}</p>
        </div>
    )
}

export default ProductItem