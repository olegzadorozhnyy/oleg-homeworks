import React, {useEffect} from "react";
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import ProductItem from "../ProductItem";
import {getData} from "../../store/createActions/getData"

const ProductList = () => {
    const dispatch = useDispatch();
    const url = "http://danit.com.ua/products";
    const action = getData(url);
    useEffect(() => {
        dispatch(action);
    }, []);

    const {products, loading, error} = useSelector(state => state.products, shallowEqual);
    const productsElems = products.map(item => <ProductItem {...item} />);
    const content = loading ? <span>Loading...</span> : productsElems;
    const errorsMessage = error ? <span>Не будет Вам Саши Грей</span> : null;

    return(
        <div className="products">
            {content}
            {errorsMessage}
        </div>
    )
}

export default ProductList