import axios from "axios"
import {FETCH_PRODUCTS_REQUEST,FETCH_PRODUCTS_LOADED,FETCH_PRODUCTS_FAIL} from "../constants"

export const getData = (url) => {
    return async (dispatch) => {
        dispatch({type: FETCH_PRODUCTS_REQUEST});
        try {
            const {data} = await axios.get(url);
            dispatch({type: FETCH_PRODUCTS_LOADED, payload: data});
        }
        catch (error) {
            dispatch({type: FETCH_PRODUCTS_FAIL, payload : error})
        }
    }
}
