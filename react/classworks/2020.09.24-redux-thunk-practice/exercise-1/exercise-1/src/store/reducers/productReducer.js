import {initialState} from "../initialState";
import {FETCH_PRODUCTS_REQUEST,FETCH_PRODUCTS_LOADED,FETCH_PRODUCTS_FAIL} from "../constants"

export const productReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case "FETCH_PRODUCTS_REQUEST":
            return {...state, loading: true}
        case "FETCH_PRODUCTS_LOADED":
            return {...state, loading: false, products: payload}
        case "FETCH_PRODUCTS_FAIL":
            return {...state, loading: false, error: payload}
        default:
            return state;
    }
}