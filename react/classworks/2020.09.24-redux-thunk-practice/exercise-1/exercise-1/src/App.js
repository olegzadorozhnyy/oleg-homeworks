import React from 'react';
import './App.css';
import {store} from "./store/store";
import {Provider} from "react-redux";
import ProductList from "./clients/ProductList";

function App() {
  return (
      <Provider store={store}>
        <div className="App">
            <ProductList />
        </div>
      </Provider>
  );
}

export default App;
