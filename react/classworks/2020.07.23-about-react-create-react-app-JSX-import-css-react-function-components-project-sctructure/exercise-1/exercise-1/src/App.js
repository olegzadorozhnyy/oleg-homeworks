import React from 'react';
import Footer from "./components/footer/Footer";

import logo from './logo.svg';
import './App.css';
const flag = false;
const text = (flag) ? "Котик на диете - несчастный котик" : "Котик без диеты - счастливый котик";
const varTrue = true;
const varFalse = false;
const varUndefned = undefined;
const varNumber = 123;
const varNull = null;
const obj = {
  name: "Александр",
  lastName: "Леща"
};
const sevens = ["Патриот", "Королева Мэйв", "Звездочка", "Черный Нуар"];
const peopleList = sevens.map(item => <li>{item}</li>);

function App() {

  return (
    <div className="App">
      <header className="App-header">
        <h1 className="header-title">{text}</h1>
        <ul>
          {peopleList}
        </ul>
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <Footer />
    </div>
  );
}

export default App;
