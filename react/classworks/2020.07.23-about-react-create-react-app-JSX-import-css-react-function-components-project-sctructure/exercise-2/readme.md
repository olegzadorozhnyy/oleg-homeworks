## Задание

Возьмите [макет](MovieRise.psd), и сверстайте его, используя JSX, и разбив страницу на отдельные функциональные компоненты. Стилизируйте каждый компонент отдельно с помощью css.

#### Литература:
- [JSX](https://yaroslav-kulpan-dev.gitbook.io/mern/gamer/jsx)
- [Компонентный подход](https://yaroslav-kulpan-dev.gitbook.io/mern/gamer/komponentnyi-podkhod)
- [Компоненты и props](https://yaroslav-kulpan-dev.gitbook.io/mern/gamer/untitled)