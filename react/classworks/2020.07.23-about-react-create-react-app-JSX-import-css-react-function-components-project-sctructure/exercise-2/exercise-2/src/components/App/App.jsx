import React from 'react';

import {Navbar} from "../Navbar";
import {TourList} from "../TourList";
import {Footer} from "../Footer";

// import logo from './logo.svg';
import './App.css';

const pageProps = {
  navbar: {
    logo: {
      firstLetter: "Milan",
      secondLetter: "Holidays"
    },
    contacts: [
      {
        type: "email",
        href: "tours@milanoholidays.com",
        text: "tours@milanoholidays.com"
      },
      {
        type: "tel",
        href: "+39 338 709 0880",
        text: "+39 338 709 0880"
      }
    ]

  },
  tourList: {
    list: ["Shopping Tours", "Sightseeing Tours", "Food Tours", "Bars & Nightlife", "Art & Architecture", "Walking Tours"],
  }
};

function App() {
  const {navbar, tourList} = pageProps;
  return (
    <div className="tour-page tour-page-milan">
      <Navbar logo={navbar.logo} contacts={navbar.contacts} />
      <TourList list={tourList.list} />
      <Footer />
    </div>
  );
}

export default App;
