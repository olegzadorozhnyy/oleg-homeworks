import React from "react";

import {Logo} from "../Logo";
import {Contacts} from "./Contacts";

import "./navbar.css";

const Navbar = (props) => {
    const {logo, contacts} = props;
    return (
        <div className="navbar">
            <div className="container">
                <div className="navbar-row">
                    <Logo firstLetter={logo.firstLetter} secondLetter={logo.secondLetter} />
                    <Contacts contactsList={contacts} />
                </div>
            </div>
        </div>
    )
};

export default Navbar;