import React from "react";

import "./contacts.css";

const contactHrefPrefix = {
    email: "mailto:",
    tel: "tel:"
};

const getLinkHref = (type, href)=> {
    return (`${contactHrefPrefix[type]}${href}`);
}

const Contacts = ({contactsList})=> {
    const contactItems = contactsList.map(({type,href, text}) => <a href={getLinkHref(type, href)}>{text}</a>);
    return (
        <div className="contacts">
            {contactItems}
        </div>
    )
};

export default Contacts;