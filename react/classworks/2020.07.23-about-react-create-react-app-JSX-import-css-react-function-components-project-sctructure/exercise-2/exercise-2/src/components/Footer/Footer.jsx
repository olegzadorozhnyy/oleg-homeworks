import React from 'react';
import "./footer.css";

const Footer = () => {
    return (
        <footer className="footer">
            <div className="container">
                <p className="copyright">© 2018 Copyright Milan Holidays, Inc. All rights reserved.</p>
            </div>
        </footer>
    )
};

export default Footer;