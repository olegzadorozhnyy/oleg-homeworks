import React from "react";

import {TourItem} from "./TourItem";

import "./tourList.css";

const TourList = ({list}) => {
    const tourItems = list.map(tour => <TourItem tourName={tour} />);
    return (
        <main className="tours">
            <div className="container">
                <div className="tour-list">{tourItems}</div>
            </div>
        </main>
    )
};

export default TourList;