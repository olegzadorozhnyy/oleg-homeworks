import React from "react";

import "./tourItem.css";

const TourItem = ({tourName}) => {
    return (    
        <div className="tour-item">
            <a href="#" className="tour-name">{tourName}</a>
        </div>        
    )
};

export default TourItem;