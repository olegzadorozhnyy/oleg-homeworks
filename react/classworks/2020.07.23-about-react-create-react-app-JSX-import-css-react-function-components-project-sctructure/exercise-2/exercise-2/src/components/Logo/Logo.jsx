import React from "react";

import "./logo.css";

const Logo = (props)=> {
    const {firstLetter, secondLetter} = props;
    return (
        <a href="/" className="logo">
            <span className="logo-img"></span>
            <p className="logo-text">
                <span className="logo-big-text">{firstLetter}</span>
                <span className="logo-small-text">{secondLetter}</span>
            </p>
        </a>
    )
};

export default Logo;