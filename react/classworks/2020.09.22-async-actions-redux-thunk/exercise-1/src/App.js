import React from 'react';
import './App.css';
import {Provider} from "react-redux";
import store from "./store/store";
import PostList from "./client/PostList";

function App() {
  return (
      <Provider store={store}>
        <div className="App">
            <PostList/>
        </div>
      </Provider>

  );
}

export default App;
