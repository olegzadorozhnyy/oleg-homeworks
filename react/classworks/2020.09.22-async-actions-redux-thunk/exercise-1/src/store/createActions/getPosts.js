import axios from "axios"

export const getPosts = (url) => {
    return async (dispatch) => {
        const {data} = await axios.get(url);
        dispatch({type: "GET_POSTS", payload: data});
    }
}

