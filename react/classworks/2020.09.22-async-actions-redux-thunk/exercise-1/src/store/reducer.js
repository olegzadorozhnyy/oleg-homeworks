import {initialState} from "./initialState";

export const reducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case "GET_POSTS" :
            const posts = payload;
            return {...state, posts}
        default:
            return state;
    }
}