import React from "react";

const Post = (item) => {
    const {title, shortDescription} = item;
    return (
     <div>
         <h2>Название: {title}</h2>
         <p>Описание: {shortDescription}</p>
     </div>

 )
}

export default Post