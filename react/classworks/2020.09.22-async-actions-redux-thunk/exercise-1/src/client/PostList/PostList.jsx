import React, {useEffect} from "react";
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import {getPosts} from "../../store/createActions/getPosts";
import Post from "../PostElem";

const PostList = () => {
    const posts = useSelector(({posts}) => posts, shallowEqual);
    const dispatch = useDispatch();
    const url = "http://danit.com.ua/posts";
    const action = getPosts(url);
    useEffect(() => {
        dispatch(action);
    }, [])

    const postsList = posts.map(item=> <Post {...item} />);
    return(
        <>
            {postsList}
        </>
    )
}

export default PostList