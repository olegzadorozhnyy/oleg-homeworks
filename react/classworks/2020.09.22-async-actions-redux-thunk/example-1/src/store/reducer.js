const inintialState = {
    users: []
}

export const reducer = (state = inintialState, {type, payload})=> {
    switch(type){
        case "TEST_MIDDLEWARE":
            console.log(payload)
            return state;
        case "GET_USERS":
            console.log(payload)
            return {...state, users: payload}
        default: 
        return state;
    }
};