import React, { useEffect, useState, useCallback } from 'react';
import {OmdbService} from "./services/omdb.service";
import {MovieList} from "./components/MovieList";
import {Pagination} from "./components/Pagination";
import {Navbar} from "./components/Navbar";
import {usePagination} from "./hooks/usePagination"

const pagination = [
    {
        page: 1,
    },
    {
        page: 2,
    },
    {
        page: 3,
    },
    {
        page: 4,
    },
    {
        page: 5,
    }
];
const { searchFilms } = new OmdbService();

function App() {
    const [searchValue, setSearchValue] = useState("Batman");
    const getAllFilms = useCallback(searchFilms(searchValue), [searchValue]);
    const [movies, handleClick] = usePagination(getAllFilms);

    return (
        <div className="App">
            <Navbar handleSearch={setSearchValue} initValue={searchValue}/>
            <MovieList data={movies} />
            <div className="container py-5 d-flex justify-content-center">
                <Pagination paginationArr={pagination} handleClick={handleClick} />
            </div>
        </div>
    );
}

export default App;
