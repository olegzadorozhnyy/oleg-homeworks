import axios from 'axios';
import interceptors from './interceptors';

const instance = axios.create({
    baseURL: process.env.NODE_ENV !== 'production' ? 'http://www.omdbapi.com/' : '',
});

interceptors(instance);

export default instance;
