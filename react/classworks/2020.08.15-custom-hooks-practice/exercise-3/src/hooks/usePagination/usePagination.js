import {useEffect, useState} from 'react'

export const usePagination = (getAllItems)=>{
    const [items, setItems] = useState();
    const [page, setPage] = useState(1)

    useEffect(() => {
        getAllItems(page).then(items => {
                setItems(items.Search);
            });
    
    }, [page, getAllItems]);

    const handleClick = (currentPage) => {
        setPage(currentPage)
    }

    return [items, handleClick]
}