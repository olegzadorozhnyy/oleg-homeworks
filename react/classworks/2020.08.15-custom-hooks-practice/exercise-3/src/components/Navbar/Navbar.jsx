import React, {useState} from 'react';

export const Navbar = ({handleSearch, initValue=""}) => {
    const [searchValue, setSearchValue] = useState(initValue);
    const handleChange = (event) => setSearchValue(event.target.value)
    const handleSubmit = (event) => {
        event.preventDefault();
        handleSearch(searchValue);
    }

    return (
        <nav className="navbar navbar-light bg-light mb-5">
            <div className="container-fluid">
                <a href="/" className="navbar-brand">OMDB</a>
                <form className="d-flex" onSubmit={handleSubmit}>
                    <input  value={searchValue} onChange={handleChange} className="form-control mr-2" type="search" placeholder="Search" aria-label="Search"/>
                    <button className="btn btn-outline-success" type="submit">Search</button>
                </form>
            </div>
        </nav>
    );
};