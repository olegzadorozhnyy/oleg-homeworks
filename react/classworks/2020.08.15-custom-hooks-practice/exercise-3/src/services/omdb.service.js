import axios from '../utils/axios';

export class OmdbService {

    getResource = async (url) => {
        return await axios.get(url).then(response => response.data);
    };

    page = (page = 1) => {
        return `&page=${page}`
    }

    searchFilms =  (words) => async (page) => {
        return await this.getResource(`?s=${words}${this.page(page)}`);
      };

}