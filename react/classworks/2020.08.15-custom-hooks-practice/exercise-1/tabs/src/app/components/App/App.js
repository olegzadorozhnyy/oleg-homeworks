import React from 'react';
import Tabs from "../../../client/Tabs/components/Tabs";

// const items = [
//   {title: "Title", content: "Content 1", id: 1},
//   {title: "Title", content: "Content 2", id: 2},
//   {title: "Title", content: "Content 3", id: 3},
//   {title: "Title", content: "Content 4", id: 4}
// ];

// const allItems = items.map(item => ({...item, isActive: false}));

function App() {
  return (
    <div className="App">
      <Tabs />
    </div>
  );
}

export default App;
