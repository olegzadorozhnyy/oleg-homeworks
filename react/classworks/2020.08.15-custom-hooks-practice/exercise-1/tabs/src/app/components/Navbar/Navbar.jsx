import React from 'react'

const Navbar = (props) => {
    return (
        <nav className="navbar">
            <div className="container">
                <div className="navbar-row">
                    <Logo />
                    <Menu />
                    <AuthPanel />
                </div>
            </div>
        </nav>
        
    )
}