import React, {useState} from "react";

const useTabs = (itemsProps) => {
    const [items, setItems] = useState(itemsProps)

    const handleClick = (id) => {
        const newItems =  items.map( item => ({...item, isActive: item.id === id}))
        setItems(newItems);
    };

    return[items, handleClick]
}
export default useTabs