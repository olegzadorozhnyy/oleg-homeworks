import React, {useState} from "react";
import TabsList from "../TabsList";
import useTabs from "../../../../hooks/useTabs"

const Tabs = () => {


    const itemsProps = [
            {title: "Title", content: "Content 1", id: 1, isActive: true},
            {title: "Title", content: "Content 2", id: 2, isActive: false},
            {title: "Title", content: "Content 3", id: 3, isActive: false},
            {title: "Title", content: "Content 4", id: 4, isActive: false}
        ];

    const [items, handleClick] = useTabs(itemsProps)



    return (
        <div className="container-tab">
            <TabsList
                items={items}
                handleClick={handleClick}
            />

        </div>
    )

}

export default Tabs;