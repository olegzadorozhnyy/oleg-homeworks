import React from "react";
import TabsItem from "../TabsItem";
import PropTypes from "prop-types";

const TabsList = ({items, handleClick}) => {

    const itemsComponets = items.map( (itemProps) => {
     return <TabsItem key={itemProps.id} {...itemProps} handleClick={handleClick} />
    })

    return (
        <ul className="accordion-tabs">
            {itemsComponets}
        </ul>
    )
}

TabsList.propTypes = {
    items: PropTypes.arrayOf(
        PropTypes.shape({
            title: PropTypes.string.isRequired,
            content: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.element,
            ]).isRequired,
          })
    ),
    handleClick: PropTypes.func.isRequired,
}


export default TabsList;