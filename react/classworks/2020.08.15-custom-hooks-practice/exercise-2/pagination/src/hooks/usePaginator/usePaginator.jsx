import {useState, useEffect} from "react";

const usePaginator = (getAllItems) => {
    const [items, setItems] = useState();
    const [page, setPage] = useState(1)

    useEffect(() => {

        getAllItems(page).then(items => {
            setItems(items.Search);
        });

    }, [page, getAllItems]);

    const handleClick = (currentPage) => {
        setPage(currentPage)
    }
    return[items, handleClick]
}

export default usePaginator
