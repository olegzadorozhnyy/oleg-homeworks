import React from 'react';

export const Pagination = ({ paginationArr, handleClick }) => {
    const pagination = paginationArr.map(({ page }) => (
        <li onClick={() => handleClick(page)} className="page-item" key={page + 1}>
            <button className="page-link">{page}</button>
        </li>
    ))
    return (
        <nav aria-label="Page navigation example">
            <ul className="pagination pagination-lg">
                <li className="page-item">
                    <button className="page-link" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </button>
                </li>
                {pagination}
                <li className="page-item">
                    <button className="page-link" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </button>
                </li>
            </ul>
        </nav>
    );
};

