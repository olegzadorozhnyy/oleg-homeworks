import React from 'react';

export const Navbar = () => {
    return (
        <nav className="navbar navbar-light bg-light mb-5">
            <div className="container-fluid">
                <a href="/" className="navbar-brand">OMDB</a>
                <form className="d-flex">
                    <input className="form-control mr-2" type="search" placeholder="Search" aria-label="Search"/>
                    <button className="btn btn-outline-success" type="submit">Search</button>
                </form>
            </div>
        </nav>
    );
};