import React from "react";

export const MovieCard = ({Title, Poster}) => {
    return (
        <div className="col-lg-3 mb-3">
            <div className="card">
                    <img src={Poster} className="products-card-image" alt={Poster}/>
                <div className="card-body">
                    <div className="card-divider py-2 text-center">
                        <h5 className="card-title">{Title}</h5>
                    </div>
                </div>
            </div>
        </div>
    );
};
