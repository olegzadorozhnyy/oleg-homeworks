import React from 'react';
import {MovieCard} from "../MovieCard";

export const MovieList = ({data}) => {
    if (!data || !data.length) {
        return (
            <div className="container py-3 d-flex justify-content-center flex-column align-items-center">
                <div className="spinner-grow text-primary" role="status" style={{
                    height: '10rem',
                    width: '10rem',
                }}>
                    <span className="sr-only">Loading...</span>
                </div>
                <h2>Загружаю...</h2>
            </div>
        )
    }

    const moviesList = data.map(props => (
        <MovieCard key={props.imdbID} {...props} />
    ))

    return (
        <div className="container">
            <div className="row">
                {moviesList}
            </div>
        </div>
    );
};

