export const modifyRequest = (request) => {
    if (request) {
        request.url = `${request.url}${process.env.REACT_APP_SERVER_KEY}`;
    }

    return request;
}

export default (axios) => {
    axios.interceptors.request.use(modifyRequest);
}