import React, { useEffect, useState } from 'react';
import {OmdbService} from "./services/omdb.service";
import {MovieList} from "./components/MovieList";
import {Pagination} from "./components/Pagination";
import {Navbar} from "./components/Navbar";
import usePaginator from "./hooks/usePaginator";

const pagination = [
    {
        page: 1,
    },
    {
        page: 2,
    },
    {
        page: 3,
    },
    {
        page: 4,
    },
    {
        page: 5,
    }
];
const { getAllFilms } = new OmdbService();

function App() {
    const [movies, handleClick] = usePaginator(getAllFilms);

    return (
        <div className="App">
            <Navbar/>
            <MovieList data={movies} />
            <div className="container py-5 d-flex justify-content-center">
                <Pagination paginationArr={pagination} handleClick={handleClick} />
            </div>
        </div>
    );
}

export default App;
