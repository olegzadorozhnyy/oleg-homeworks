import axios from '../utils/axios';

export class OmdbService {

    getResource = async (url) => {
        return await axios.get(url).then(response => response.data);
    };

    page = (page = 1) => {
        return `&page=${page}`
    }

    searchFilms = async (words) => {
        const response = await this.getResource(`?s=${words}`);
        return response.Search;
      };
      
    getAllFilms = async (page) => {
        return await this.getResource(`/?s=Batman${this.page(page)}`);
    };
}