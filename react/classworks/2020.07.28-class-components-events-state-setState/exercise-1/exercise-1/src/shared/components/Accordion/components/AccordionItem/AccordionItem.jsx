
import React, {Component} from "react";
import "./AccordionItem.css"

export class AccordionItem extends Component {

    state = {
        expanded: false
    }

    handleClick = () => {
        /*
        this.setState(({expanded}) => {
            expanded = !expanded;
            return {
                expanded
            }
        });
        */
       this.setState(({expanded}) => ({expanded: !expanded}));       
    }

    render() {
    const {title, content} = this.props;
    const {expanded} = this.state;
    const className = (expanded) ? 'is-expanded' : '';

        return (
        
            <div className="accordion-item">
                 <p className={`title ${className}`} onClick={this.handleClick}>
                  {title}
                </p>
                <div className={`content ${className}`}>
                  <p>
                      {content}
                  </p>
                </div>
            </div>
        )
    }
} 