import React, {Component} from "react";

import "./select-text.css";

class SelectText extends Component {

    state = {
        selected: "no",
        bold: false
    };

    handleClick = () =>  {
        this.setState(state => {
            let {selected, bold} = state;
            // let selected = state.selected;
            selected = (selected === "yes") ? "no" : "yes";
            bold = !bold;
            return {
                selected, 
                bold
            };
        });
    };

    render(){
        const {selected, bold} = this.state;
        let className = (selected === "yes") ? "selected" : "";
        className += (bold) ? " bold" : "";
        return <p className={`select-text ${className}`} onClick={this.handleClick}>{this.props.text}</p>;
    }
}

export default SelectText;