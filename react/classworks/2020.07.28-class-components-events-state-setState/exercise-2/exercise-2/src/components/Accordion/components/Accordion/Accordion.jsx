
import React, {Component} from "react";
import "./Accordion.css"
import {AccordionItem} from "../AccordionItem"

export class Accordion extends Component  {
    state = {
        listExpanded : false
    }

    expendAll = () => {
        this.setState(({listExpanded})=>({listExpanded:true}))
    }

    collapseAll = () => {
        this.setState(({listExpanded})=>({listExpanded:false}))
    }

    render(){
        const {list} = this.props;
        const {listExpanded} = this.state;
    const accordionItems = list.map(elem=> {
        const props = {...elem, listExpanded}
       return <AccordionItem {...props}/>
    })
        return (
            <div className="container">
                <div className="accordion">
                    <h1>FAQ</h1>
                    <button type="button" className="btn" onClick = {this.expendAll}>Expand All</button>
                    <button type="button" className="btn" onClick = {this.collapseAll}>Collapse All</button>
                    {accordionItems}
                </div>
            </div>
        )
        }
    
};



// export const Accordion = (props) => {

//     return (
//         <div className="">
            
//         </div>
//     )
// };
