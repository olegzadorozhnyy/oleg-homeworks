import React from 'react';
import Tabs from "../../../client/Tabs/components/Tabs";

function App() {
  return (
    <div className="App">
      <Tabs />
    </div>
  );
}

export default App;
