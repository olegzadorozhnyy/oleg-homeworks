import React from "react";
import PropTypes from 'prop-types';

const TabsItem = ({title, content, isActive, handleClick}) => {
    const className = isActive ? "is-active" : "";
    const contentStyles = isActive ? {display: "block"} : {display: "none"};
    
    return (
        <li class="tab-head-cont">
			<a href="#" onClick={handleClick} className={className}>{title}</a>
			<section style={contentStyles}>
				{content}
			</section>
		</li>
    )
}

TabsItem.propTypes = {
    title: PropTypes.string.isRequired,
    content: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element,
    ]).isRequired,
    isActive: PropTypes.bool.isRequired,
    handleClick: PropTypes.func.isRequired,
}

export default TabsItem;