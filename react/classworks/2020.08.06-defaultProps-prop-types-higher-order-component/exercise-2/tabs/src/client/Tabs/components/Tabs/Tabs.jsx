import React, {Component} from "react";
import TabsList from "../TabsList";

class Tabs extends Component {
    state = {
        items: [
            {title: "Title", content: "Content"},
        ]
    }

    handleClick = () => {
        
    }

    render() {
        return (
            <div class="container-tab">
                <TabsList />
            </div>
        )
    }
}

export default Tabs;