import React from "react";
import TabsItem from "../TabsItem";
import PropTypes from "prop-types";

const TabsList = ({items, handleClick}) => {

    const itemsComponets = items.map( (itemProps) => {
     return <TabsItem key={itemProps} {...itemProps} handleClick={handleClick} />
    })

    return (
        <ul class="accordion-tabs">
            {itemsComponets}
        </ul>
    )
}

TabsList.propTypes = {
    items: PropTypes.arrayOf(PropTypes.objectOf(
        PropTypes.shape({
            title: PropTypes.string.isRequired,
            content: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.element,
            ]).isRequired,
          })
    )),
    handleClick: PropTypes.func.isRequired,
}


export default TabsList;