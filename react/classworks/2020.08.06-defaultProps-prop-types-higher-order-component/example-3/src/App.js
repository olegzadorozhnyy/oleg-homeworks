import React from "react";
import "./styles.css";
import { ToDo } from "./components/ToDo";

export default function App() {
  return (
    <div className="App">
      <ToDo />
    </div>
  );
}
