import React, { Component } from "react";
import { v4 } from "uuid";

export const withToDo = Wrapped => {
  return class extends Component {
    state = {
      title: "",
      todos: [
        {
          id: v4(),
          title: "Learning React.js",
          complete: false
        }
      ]
    };

    handleChangeTitle = event => {
      this.setState({
        [event.target.name]: event.target.value
      });
    };

    ressetForm = () => {
      this.setState({
        title: ""
      });
    };

    handleAdded = todo => {
      this.setState(({ todos }) => ({
        todos: [...todos, todo]
      }));
    };

    toggleComplete = id => {
      this.setState(({ todos }) => {
        const todo = todos.find(item => item.id === id);
        const newToDo = { ...todo, complete: !todo.complete };

        const newTodos = todos.map(item => {
          if (item.id === newToDo.id) {
            return newToDo;
          }

          return item;
        });
        return {
          todos: [...newTodos]
        };
      });
    };

    handleUpdate = (id, todos) => {
      this.setState(({ todo }) => {
        const updatesToDos = todo.filter(item => item.id !== id);
        return {
          to: [...updatesToDos, todos]
        };
      });
    };

    handleDelete = (id) => {
      const index = this.state.todos.findIndex((item) => {
        return item.id === id;
      })

      this.setState(({todos})=>{
        const newTotos = [...todos];
        newTodos.splice(index, 1);
        return {
          todos: newTodos
        }
      })

    };

    render() {
      return <Wrapped {...this} />;
    }
  };
};
