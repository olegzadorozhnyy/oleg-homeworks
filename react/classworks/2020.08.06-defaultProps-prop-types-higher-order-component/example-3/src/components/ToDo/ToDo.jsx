import React from "react";
import { withToDo } from "../HocExamples/withToDo/withToDo";
import { ToDoItem } from "../ToDoItem/ToDoItem";
import { ToDoHeader } from "../ToDoHeader";

const ToDo = props => {
  const {
    state: { todos },
    toggleComplete
  } = props;

  const todosItems = todos.map(props => (
    <ToDoItem
      key={props.id}
      toggleComplete={() => toggleComplete(props.id)}
      {...props}
    />
  ));

  return (
    <>
      <ToDoHeader {...props} />
      <ul id="myUL">{todosItems}</ul>
    </>
  );
};

export default withToDo(ToDo);
