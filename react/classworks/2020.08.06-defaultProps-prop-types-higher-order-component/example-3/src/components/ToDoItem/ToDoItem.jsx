import React from "react";
import propTypes from "prop-types";

export const ToDoItem = ({ title, complete, toggleComplete }) => {
  return (
    <li className={`${complete ? "checked" : ""}`}>
      {title}
      <span>
        <button className="button button-success" onClick={toggleComplete}>
          &#10004;
        </button>
      </span>
    </li>
  );
};

ToDoItem.deafaultProps = {
  title: "This is React tricks",
  complete: false
};

ToDoItem.propTypes = {
  title: propTypes.string.isRequired,
  complete: propTypes.bool.isRequired
};
