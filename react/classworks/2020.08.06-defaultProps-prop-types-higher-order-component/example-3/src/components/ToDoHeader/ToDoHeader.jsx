import React from "react";
import { v4 } from "uuid";

const ToDoHeader = props => {
  const {
    state: { title },
    handleChangeTitle,
    ressetForm,
    handleAdded
  } = props;

  const handleSubmit = event => {
    event.preventDefault();

    handleAdded({
      title,
      id: v4(),
      complete: false
    });
    ressetForm();
  };

  return (
    <form id="myDIV" className="header" onSubmit={handleSubmit}>
      <h2>Какие хуки ? Классы сейчас !</h2>
      <input
        type="text"
        name="title"
        id="myInput"
        placeholder="Title..."
        onChange={handleChangeTitle}
        value={title}
      />
      <button type="submit" className="addBtn">
        Add
      </button>
    </form>
  );
};

export default ToDoHeader;
