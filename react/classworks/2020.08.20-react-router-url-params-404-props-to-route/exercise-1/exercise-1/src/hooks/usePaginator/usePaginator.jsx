import {useState, useEffect} from "react";
import Service from "../../components/Service/Service";

const usePaginator = () => {

    const [workers, setWorkers] = useState([]);
    const [page, setPage] = useState(1);
    const [data] = Service(page);
    useEffect( (data) => {
        const workersList = data.map(({name, phone, job, email}) => ({name, phone, job, email}))
        setWorkers(workersList);

    }, [page])

    const handleClick = (e) => {
        e.preventDefault();
        setPage(e.target.textContent)
    }
    return[workers, handleClick]
}

export default usePaginator
