import axios from "axios";


const Service = async (page) => {
      const workersData = await axios.get(`http://danit.com.ua/workers?page=${page}`)
        .then(({data}) => {
            return data
        });
      return workersData;
}

export default Service;