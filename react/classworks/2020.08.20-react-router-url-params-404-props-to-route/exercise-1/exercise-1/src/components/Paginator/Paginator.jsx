import React from "react";
import {Link} from "react-router-dom"
import "./Pagination.css"

const Paginator = ({pages, handleClick}) => {

    const pagination = pages.map((page) => (
        <li>
            <Link href={page} onClick={handleClick}>{page}</Link>
        </li>
    ));

    return (
        <>
            <ul className="pagination">
                {pagination}
            </ul>
        </>

        )
}

export default Paginator