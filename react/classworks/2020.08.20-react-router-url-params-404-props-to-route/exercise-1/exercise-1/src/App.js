import React from 'react';
import {BrowserRouter as Router} from "react-router-dom"
import SinglePage from "./clients/Workers/pages/SinglePage";
import Paginator from "./components/Paginator";
import usePaginator from "./hooks/usePaginator";


function App() {
    const pages = [1, 2, 3]
    const [workers, handleClick] = usePaginator();

  return (
    <div className="App">

        <Router>

            <SinglePage workers={workers} />
            <Paginator pages={pages} handleClick={handleClick} />

        </Router>
    </div>
  );
}

export default App;
