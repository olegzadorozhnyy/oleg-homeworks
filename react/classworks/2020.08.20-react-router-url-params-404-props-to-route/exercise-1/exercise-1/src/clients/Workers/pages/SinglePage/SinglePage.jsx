import React, {useState, useEffect} from "react";
import axios from "axios"

const SinglePage = (workers) => {


    const workersElements = workers.map(({name, phone, job, email}) => {
            return <li>{name} - {phone} - {job}<a href={email}>{email}</a></li>
        }
    );
    return (
        <>
            {workersElements}
            </>

    )

}

export default SinglePage;