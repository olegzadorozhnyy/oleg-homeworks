import React, {Component} from "react";
import TabsList from "../TabsList";

import { withTabs } from "../../../../shared/withTabs";

const Tabs = (props) => {

    const {state: {items}, handleClick} = props;

    return (
        <div className="container-tab">
            <TabsList 
                items={items}  
                handleClick={handleClick}
            />
        </div>
    )
}

export default withTabs(Tabs);