import React from "react";
import PropTypes from 'prop-types';

const TabsItem = ({title, content, isActive, handleClick, id}) => {
    const className = isActive ? "is-active" : "";
    const contentStyles = isActive ? {display: "block"} : {display: "none"};
    
    return (
        <li className="tab-head-cont">
			<a href="#" onClick={() => handleClick(id)} className={className}>{title}</a>
			<section style={contentStyles}>
				{content}
			</section>
		</li>
    )
}

TabsItem.propTypes = {
    title: PropTypes.string.isRequired,
    content: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element,
    ]).isRequired,
    isActive: PropTypes.bool.isRequired,
    handleClick: PropTypes.func.isRequired,
}

export default TabsItem;