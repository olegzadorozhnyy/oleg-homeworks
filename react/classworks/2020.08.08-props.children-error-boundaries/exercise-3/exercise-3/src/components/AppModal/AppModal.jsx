import React, {Component} from 'react';
import Modal from "../Modal";
import App from '../../App';

class AppModal extends Component {

    render(){
        return (
            <>
                <button type="button" class="btn btn-info btn-round" data-toggle="modal" data-target="#loginModal">
                    Login
                </button>  
                <Modal handleClose={this.handleClose}>
                        {this.props.children}
                </Modal>
            </>
        )

    }
}

export default AppModal;


