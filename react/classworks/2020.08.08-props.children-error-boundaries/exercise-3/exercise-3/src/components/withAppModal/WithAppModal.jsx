import React, { Component } from 'react'

const withAppModal = Wrapper => {
    return class extends Component {
        state = {
            isOpen: false
        }
    
        handleOpen = ()=>{
            this.setState( {
                isOpen: true
            })
        }
        handleClose = ()=>{
            this.setState( {
                isOpen: false
            })
        }
        
        render() {
            return <Wrapper {...this} />
        }
    }
}

export default withAppModal;