import React from 'react';
import logo from './logo.svg';
import './App.css';
import AppModal from "./components/AppModal";
import LoginForm from "./components/LoginForm";
console.log(AppModal)
console.log(LoginForm)

function App() {
  return (
    <div className="App">
        <AppModal>
          <LoginForm/>
        </AppModal>
    </div>
  );
}

export default App;
