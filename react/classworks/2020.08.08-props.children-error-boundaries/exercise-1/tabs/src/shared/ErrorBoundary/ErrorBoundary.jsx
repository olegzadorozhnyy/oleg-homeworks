import React, {Component} from 'react'

class ErrorBoundary extends Component {
    state = {
        isError: false
    }
     
    static getDerivedStateFromError(error) {
        // Обновить состояние с тем, чтобы следующий рендер показал запасной UI.
        return { isError: true };
      }
    
      componentDidCatch(error, errorInfo) {
        // Можно также сохранить информацию 
        //об ошибке в соответствующую службу 
        //журнала ошибок
        logErrorToMyService(error, errorInfo);
      }

    render(){
        const {isError} = this.state;
        if (isError) {
              return(
                this.props.errorText
            )
        }
        else {
            return this.props.children;
        }
        }
    }





