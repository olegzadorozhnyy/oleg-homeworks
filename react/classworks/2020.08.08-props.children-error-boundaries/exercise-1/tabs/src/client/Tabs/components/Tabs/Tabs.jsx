import React, {Component} from "react";
import TabsList from "../TabsList";

class Tabs extends Component {
    state = {
        items: [
            {title: "Title", content: "Content 1", id: 1, isActive: true},
            {title: "Title", content: "Content 2", id: 2, isActive: false},
            {title: "Title", content: "Content 3", id: 3, isActive: false},
            {title: "Title", content: "Content 4", id: 4, isActive: false}
        ]
    };

    handleClick = (id) => {
        this.setState( ({items}) => {
            const newItems =  items.map( item => ({...item, isActive: item.id === id}))

            return {items: newItems};
        } )
    };

    render() {
        const errorText = <p>Попробуйте поже</p>
        return (
            <div className="container-tab">
                <ErrorBoundary errorText={errorText}>
                <TabsList 
                    items={this.state.items}  
                    handleClick={this.handleClick}
                />
                </ErrorBoundary>

            </div>
        )
    }
}

export default Tabs;