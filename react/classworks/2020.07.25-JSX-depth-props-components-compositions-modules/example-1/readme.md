### Пример

Верстка [макета](https://www.figma.com/file/B2YYD62F3K1HqP96OUoC9D/Milan-Holidays), используя принципы, изложенные в [статье](https://yaroslav-kulpan-dev.gitbook.io/mern/gamer/komponentnyi-podkhod), а также модульный подход.