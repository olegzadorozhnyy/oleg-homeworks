import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../shared/styles/scss/style.scss';

// if you using css deleting scss import, and uncomment this import
// import '../../../shared/styles/scss/style.css';

// import {Example} from "../../../client/example/components/Example";
import {Navbar} from "../../../client/Navbar/components/Navbar";
import {TourList} from "../../../client/Tour-list/components/Tour-list";
import {Footer} from "../../../client/Footer/components/Footer";

export const App = () => {

    const props = {
        navbar: {
            logo: {
                firstWord: "Milan",
                secondWord: "Holidays"
            },
            contacts: [
                {
                    type: "email",
                href: "tours@milanoholidays.com",
                text: "tours@milanoholidays.com"
                },
                {
                    href: "+39 338 709 0880",
                    text: "+39 338 709 0880"
                }
            ]
        },
        tourList: 
        {
            heading: "Milan tours list",
            list: [
                {
                    title: "Shopping Tours"
                },
                {
                    title: "Sightseeing Tours"
                },
                {
                    title: "Food Tours"
                },
                {
                    title: "Bars & Nightlife"
                },
                {
                    title: "Art & Architecture"
                },
                {
                    title: "Walking Tours"
                },                                                            
            ]
        },
        footer: {
            copyright: "© 2018 Copyright Milan Holidays, Inc. All rights reserved."
        }
    };
    // const {contacts, ...navbarPage} = props.navbar;
    const {navbar, tourList, footer} = props;
    return (
        <div className="container">
            <Navbar {...navbar} />
           {/* <Navbar logo={props.navbar.logo} contacts={props.navbar.contacts} />*/}
            <TourList {...tourList} />
            <Footer copyright={footer.copyright} />
        </div>
    );
};
