import React from "react";
import {Form} from "../Form/";

/*
{
    type: "email",
    href: "tours@milanoholidays.com",
    text: "tours@milanoholidays.com"
},
*/

const prefix = {
    email: "mailto:",
    tel: "tel:",
    telegram: "teleg?:"
};

export const Contacts = (props)=> {
    const contactList = props.map(item => <a href={`${prefix[item.type]}${item.href}`}>{item.text}</a>)
    return (
        <div className="contacts">
            {contactList}
        </div>
    )
}