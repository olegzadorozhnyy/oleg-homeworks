### Задание

Сверстайте  [psd-макет](page.psd), используя принципы, изложенные в [статье](https://yaroslav-kulpan-dev.gitbook.io/mern/gamer/komponentnyi-podkhod), а также модульный подход.
Для работы с psd вы можете использовать [онлайн-сервис](https://psdetch.com/)