import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../shared/styles/scss/style.scss';

import {Navbar} from '../../../client/Navbar/components/Navbar';
import {AboutUs} from '../../../client/About-us/components/About-us';

export const App = () => {
    const pageProps = {
        navbar: {
            logo: {
                alt: 'Company Logo',
                src: '../../../assets/images/logo.png',
                text: ''
            },
            menu: [
                {text: 'Home', url: '#Home'}, 
                {text: 'Packages', url: '#Packages'}, 
                {text: 'About us', url: '#About-us'}, 
                {text: 'Contact us', url: '#Contact-us'}]
        },
        aboutUs: {
            title: "About Us",
            desription: "Inaql is the best search engine for finding a suitable transportation method",
            features: ["Register Now !it’s for free", "Search using multiple parameters", "Come back again we’r here all the time"]
        },

        search: {
            title: 'Search',
        }
    }
    return (
        <div className="container">
            <Navbar {...pageProps.navbar} />
            <AboutUs {...pageProps.aboutUs} />
        </div>
    );
};
