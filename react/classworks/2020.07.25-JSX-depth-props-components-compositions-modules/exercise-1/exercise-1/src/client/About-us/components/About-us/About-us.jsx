import React from "react";
import {Section} from "../../../../shared/components/Section";

export const AboutUs = (props)=>{
    const {title, desription, features} = props;
    const contentItems = features.map(item => <div className="features-item">{item}</div>)
    const content = <div className="features-list">{contentItems}</div>
    /*
        <div className="features-list">
            <div className="features-item">Register Now !it’s for free</div>
            <div className="features-item">Search using multiple parameters</div>
            <div className="features-item">Come back again we’r here all the time</div>
        </div> 
    */
    return (
        <Section title={title} desription={desription} content={content} />
    )
}