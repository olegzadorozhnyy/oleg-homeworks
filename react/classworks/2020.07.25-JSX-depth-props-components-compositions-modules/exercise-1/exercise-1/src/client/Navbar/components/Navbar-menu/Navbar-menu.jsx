import React from 'react';

export const NavbarMenu = (props) => {
    console.log(props.menu);
    const menuElements = props.menu.map(item => {
    return <li className='navbar-menu-item'><a className='navbar-menu-link' url={item.url}>{item.text}</a></li>
});
    return(
        <ul className='navbar-menu'>
            {menuElements}
        </ul>
    );
}