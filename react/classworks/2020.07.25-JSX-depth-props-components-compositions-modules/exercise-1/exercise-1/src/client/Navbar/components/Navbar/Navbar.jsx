import React from 'react';

import {Logo} from '../../../../shared/components/Logo';
import {NavbarMenu} from '../Navbar-menu';
import {AuthPanel} from '../../../../shared/components/Auth-panel';
import {LangSwitcher} from '../../../../shared/components/Lang-switcher';


export const Navbar =(props)=>{
        const {logo, menu, LangSwitcher} = props;
    console.log(menu)
    return(
        <div className="navbar">
            <Logo {...logo} />
            <NavbarMenu menu={menu} />
{/*            <AuthPanel />
            <LangSwitcher {...LangSwitcher} />*/}
        </div>
    );
}