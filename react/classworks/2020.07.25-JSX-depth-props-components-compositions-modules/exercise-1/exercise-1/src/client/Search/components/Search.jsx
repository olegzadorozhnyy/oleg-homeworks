import React from "react";
import {Section} from "../../../shared/components/Section"
import {FormSearch} from "../../../shared/components/FormSearch"

export const Search = ({title}) => {
const content = <FormSearch />
    return (
        <Section title={title} content={content}/>
    )
}