import React from 'react';

const btnTypes = {
    outline: 'btn-outline',
    primary: 'btn-primary',
    light: 'btn-light'
}
export const Button =({type, text})=>{
    return(
        <a className={`btn ${btnTypes[type]}`}>{text}</a>
    );
}