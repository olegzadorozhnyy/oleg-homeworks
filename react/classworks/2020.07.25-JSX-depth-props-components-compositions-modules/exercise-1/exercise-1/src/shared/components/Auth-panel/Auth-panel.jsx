import React from 'react';

import {Button} from '../Button';

export const AuthPanel =()=>{
    return(
        <div className='auth-panel'>
            <a href="" className='auth-link'>Log In</a>
            <Button type='outline' text='Sign Up' />
        </div>
    );
}