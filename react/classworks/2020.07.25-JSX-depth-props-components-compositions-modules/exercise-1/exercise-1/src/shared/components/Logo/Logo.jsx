import React from 'react';

import './Logo.css';

export const Logo = ({alt, src, text}) => {
    const logoImg = (src) ? <img src={src} alt={alt} className='logo-img'/> : null;
    const logoText = (text) ? <span className='logo-text'>{text}</span> : null;
    return(
    <a className='logo' href='/'>
        {logoImg}
        {logoText}
    </a>
    );
}