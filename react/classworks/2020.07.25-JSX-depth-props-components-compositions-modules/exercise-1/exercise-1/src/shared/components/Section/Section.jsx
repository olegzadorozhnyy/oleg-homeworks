import React from "react"

export const Section = (props)=> {
    const {title, desription, sectionClass, id, content} = props;
    const sectionDescription = (desription) ? <h4 className="section-description">{desription}</h4> : null;
    return (
        <section id={id || ""} className={`section ${sectionClass}`}>
            <h2 className="section-title">{title}</h2>
            {sectionDescription}
            {content}
        </section>
    );
}