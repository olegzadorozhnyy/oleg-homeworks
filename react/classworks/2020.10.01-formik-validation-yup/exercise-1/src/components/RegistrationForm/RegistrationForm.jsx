import React from "react";
import {Formik, Form, Field, ErrorMessage} from "formik"
import {fields} from "./fields"
import * as yup from 'yup';
import {validationsMessage} from "../../shared/validation";
import FormErrorMessage from "../../shared/FormErrorMessage"

const RegistrationForm = () => {
    const initialValues = {
        login: "",
        email: "",
        password: "",
        repeatePassword: ""
    };
    const onSubmit = (value) => {
        console.log(value);
    }
    /*
    const requireValidate = (values, errorRequireMessage) => {
        const errors =  {};
        for(const [key, value] of Object.entries(values)) {
            if (!value) {
                errors[key] =errorRequireMessage;
            }
        }
        return errors
    }

    const validate = (values) => {
        const errors = requireValidate(values, "Поле обязательно к заполнению");

        if (values.login.length < 2 && values.login) {
            errors.login = "Логин должен быть не меньше 2х букв"
        }
        if ((!values.email.includes("@") || values.email.includes(" ")) && values.email ) {
            errors.email = "Не должно быть пробелов, обязательно @"
        }
        if (values.password.length < 6 && values.password) {
            errors.password = "Пароль должен быть не меньше 6 символов";
        }
        if (values.repeatePassword.length < 6 && values.repeatePassword) {
            errors.repeatePassword = "Пароль должен быть не меньше 6 символов";
        } else if (values.password !== values.repeatePassword) {
            errors.repeatePassword = "Пароль должен совпадать с предыдущим полем";
        }
        return errors;
    };*/


    const schema = yup.object().shape({
        login: yup.string().required(validationsMessage.required).min(2, validationsMessage.login),
        email: yup.string().required(validationsMessage.required).email(validationsMessage.email),
        password: yup.string().required(validationsMessage.required).min(6, validationsMessage.password),
        repeatePassword: yup.string().required(validationsMessage.required).min(6, validationsMessage.password),
    });

    const formProps = {
        initialValues,
        onSubmit,
        validationSchema : schema
    }
    return(
        <Formik {...formProps}>
            <Form>
                <Field {...fields.login}/>
                <FormErrorMessage name="login"/>
                <Field {...fields.email}/>
                <FormErrorMessage name="email"/>
                <Field {...fields.password}/>
                <FormErrorMessage name="password"/>
                <Field {...fields.repeatePassword}/>
                <FormErrorMessage name="repeatePassword"/>
                <Field {...fields.submit}/>
            </Form>
        </Formik>
    )
}

export default RegistrationForm;