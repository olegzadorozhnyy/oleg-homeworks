export const fields = {
    login: {
        name: "login",
        placeholder: "login",
    },
    email : {
        name: "email",
        placeholder: "email",
    },
    password : {
        name: "password",
        type: "password",
        placeholder: "password",
    },
    repeatePassword : {
        name: "repeatePassword",
        type: "password",
        placeholder: "repeat",
    },
    submit: {
        name: "submit",
        type: "submit"
    }
}