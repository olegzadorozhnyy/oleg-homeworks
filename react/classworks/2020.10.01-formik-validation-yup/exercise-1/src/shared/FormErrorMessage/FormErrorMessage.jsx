import React from "react";
import {ErrorMessage} from "formik";

const FormErrorMessage = ({name}) => {
    return (
        <ErrorMessage name={name} >
            {(errorMessage) => {
                return <p className="error">{errorMessage}</p>
            }}
        </ErrorMessage>
    )

}
export  default FormErrorMessage;