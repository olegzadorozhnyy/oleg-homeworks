import React, { Component } from "react";
import "./styles.css";
import { Field } from "./components/Field";

export class App extends Component {
  state = {
    name: "",
    description: "",
    bookId: ""
  };

  handleChange = event => {
    // {name: "Песнь Льда"}
    
    const subject = prompt("Введите название предмета"); // biology;
    const score = propmt("Введите оценку");
    const tabel = {[subject]: score}; 
    // const tabel = {};
    // tabel[subject] = score;

    this.setState({ [event.target.name]: event.target.value });
  };

  onSubmit = event => {
    event.preventDefault();

    alert(JSON.stringify(this.state, null, 2));
    this.setState({
      name: "",
    })
  };

  render() {
    const { name, description, bookId } = this.state;
    return (
      <div className="App">
        <form action="#" onSubmit={this.onSubmit} className="wrapper">
          <div className="mb-3">
            <Field
              name="name"
              className="form-control"
              placeholder="Name"
              handleChange={this.handleChange}
              value={name}
            />
          </div>
          <div className="mb-3">
            <Field
              name="description"
              className="form-control"
              placeholder="Description"
              handleChange={this.handleChange}
              value={description}
            />
          </div>
          <div className="mb-3">
            <Field
              name="bookId"
              className="form-control"
              placeholder="bookId"
              handleChange={this.handleChange}
              value={bookId}
            />
          </div>

          <button className="btn btn-block btn-primary">Отправить</button>
        </form>
      </div>
    );
  }
}
