import React from "react";

export const Field = ({ name, handleChange, className, type, placeholder }) => {
  return (
    <input
      name={name}
      type={type}
      placeholder={placeholder}
      className={className}
      onChange={handleChange}
    />
  );
};

Field.defaultProps = {
  name: "name",
  type: "text"
};
