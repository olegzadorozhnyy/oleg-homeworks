import React,{Component} from 'react';
import Button from '../Button';
import Modal from '../Modal';


class AppModal extends Component {

    state = {
        open: false,
    }

    handleOpen = () => {
        this.setState({
            open: true,
        });
    }

    handleClose = () => {
        this.setState({
            open: false,
        });
    }

    render(){
        return(
            <>
                <Button handleClick={this.handleOpen} text='Open modal!' type='primary'/>
                <Modal handleClose={this.handleClose} />
            </>
        )
    }
}


export default AppModal;