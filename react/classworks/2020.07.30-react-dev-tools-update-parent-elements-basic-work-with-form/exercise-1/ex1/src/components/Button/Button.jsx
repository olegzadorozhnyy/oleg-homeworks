import React from 'react';


const buttonTypes = {
    primary: 'btn-primary'
}


const Button = ({text, type, handleClick}) => {
    return (
        <button onClick={handleClick} className={`btn ${buttonTypes[type]}`}>
            {text}
        </button>
    )
}


export default Button;