import React from 'react';
import logo from './logo.svg';
import './App.css';
import AppModal from './components/AppModal/AppModal';

function App() {
  return (
    <div className="App">
      <AppModal />
    </div>
  );
}

export default App;
