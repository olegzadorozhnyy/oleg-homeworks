import React from "react";

export const toCurrency = price => {
  return new Intl.NumberFormat("ukr", {
    currency: "uah",
    style: "currency"
  }).format(price);
};

export const ProductsCard = ({
  title,
  image,
  price,
  description,
  handleAddedToCart
}) => {
  console.log(handleAddedToCart)
  return (
    <div className="col-md-4">
      <div className="card">
        <img src={image} className="products-card-image" alt={title} />
        <div className="card-body">
          <h5 className="card-title">{title}</h5>
          <strong className="text-center">{toCurrency(price)}</strong>
          <p className="card-text">{description}</p>
          <button className="btn btn-primary" onClick={handleAddedToCart}>
            Добавить в корзину
          </button>
        </div>
      </div>
    </div>
  );
};
