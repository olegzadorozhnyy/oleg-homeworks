import React from "react";
import { ProductsCard } from "../ProductsCard/ProductsCard";

export const ProductsGrid = ({ products, handleAddedToCart }) => {
  const productsList = products.map(item => (
    <ProductsCard
      key={item.title}
      handleAddedToCart={() => handleAddedToCart(item.id)}
      {...item}
    />
  ));

  return (
    <div className="container">
      <div className="row">{productsList}</div>
    </div>
  );
};
