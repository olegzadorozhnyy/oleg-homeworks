import React from "react";

export const toCurrency = price => {
  return new Intl.NumberFormat("ukr", {
    currency: "uah",
    style: "currency"
  }).format(price);
};

export const ShoppingCartItems = ({
  id,
  title,
  item,
  total,
  count,
  handleAddedToCart,
  handleRemoveFromCart,
  allItemsRemovedFromCart
}) => {
  return (
    <tr>
      <th scope="row">{item}</th>
      <td>{id}</td>
      <td>{title}</td>
      <td>{toCurrency(total)}</td>
      <td>{count}</td>
      <td>
        <button className="btn btn-primary" onClick={() => handleAddedToCart()}>
          +
        </button>
        <button
          className="btn btn-warning"
          onClick={() => handleRemoveFromCart()}
        >
          -
        </button>
        <button
          className="btn btn-danger"
          onClick={() => allItemsRemovedFromCart()}
        >
          х
        </button>
      </td>
    </tr>
  );
};
