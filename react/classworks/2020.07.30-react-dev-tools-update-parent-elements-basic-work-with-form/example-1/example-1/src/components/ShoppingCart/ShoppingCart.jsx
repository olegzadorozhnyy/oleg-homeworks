import React from "react";
import { ShoppingCartItems } from "../ShoppingCartItems";

export const toCurrency = price => {
  return new Intl.NumberFormat("ukr", {
    currency: "uah",
    style: "currency"
  }).format(price);
};

export const ShoppingCart = ({
  cartItems,
  handleAddedToCart,
  handleRemoveFromCart,
  allItemsRemovedFromCart,
  cartTotal
}) => {
  if (!cartItems.length) {
    return <p>Ваша корзина пуста!</p>;
  }

  const items = cartItems.map((item, index) => (
    <ShoppingCartItems
      key={item.id}
      {...item}
      item={index + 1}
      handleAddedToCart={() => handleAddedToCart(item.id)}
      handleRemoveFromCart={() => handleRemoveFromCart(item.id)}
      allItemsRemovedFromCart={() => allItemsRemovedFromCart(item.id)}
    />
  ));

  return (
    <div className="container">
      <div className="row">
        <table className="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">ID</th>
              <th scope="col">Товар</th>
              <th scope="col">Количество</th>
              <th scope="col">Цена</th>
              <th scope="col">Действия</th>
            </tr>
          </thead>
          <tbody>{items}</tbody>
        </table>
        <p className="text-center">{toCurrency(cartTotal)}</p>
      </div>
    </div>
  );
};
