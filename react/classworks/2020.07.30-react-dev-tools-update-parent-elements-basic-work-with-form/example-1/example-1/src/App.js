import React, { Component } from "react";
import "./styles.css";
import { ProductsGrid } from "./components/ProductsGrid/ProductsGrid";
import { ShoppingCart } from "./components/ShoppingCart";

export default class App extends Component {
  state = {
    products: [
      {
        id: "edfr4",
        title: "The Witcher Enhanced Edition",
        image: "https://gamesisart.ru/images/screens/Witcher_1_Cover.jpg",
        price: 168,
        description: `
        Ведьмак Геральт из Ривии, 
        легендарный охотник на чудовищ, оказался в центре борьбы за мировое господство. 
        Примерьте на себя его роль и погрузитесь в увлекательную историю, в которой 
        вам предстоит не раз принимать сложные решения и сталкиваться с их последствиями.        
        `
      },
      {
        id: "edfr5",
        title: "The Witcher 2: Assassins of Kings Enhanced Edition",
        image:
          "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTExQVFhUXFx4bGRgYGB4fHRogHRodGhkYIRofHSggHhsmHxoaITEhJSkrLi4uIB8zODMtNygtLisBCgoKDg0OGxAQGy0lICUtLSstLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAQoAvQMBIgACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAAEBQIDBgcBAAj/xAA9EAACAQIEAwYEBAYCAgIDAQABAhEDIQAEEjEFQVEGEyJhcYEykaGxFELB8AcjUtHh8WJyM4IVwpKy00P/xAAaAQADAQEBAQAAAAAAAAAAAAABAgMEAAUG/8QAJxEAAgICAgICAQQDAAAAAAAAAAECEQMhEjEEQSJRcRMyYYEjkdH/2gAMAwEAAhEDEQA/AMdUcAEm7HAVVGI1Rbr+nri3MTfrOJ0qZjTeOk2nrGMpvii/gLwzpyIBj0scMauTI8xyOAODsKeZolvhLaT6N4fvje53hJS4Ejn5Y8/ycn6c/wAnp+NkSjxZigkb4sppG+GWbycgx64EFImAcBZFJG5QslRAPl+mG+Xpb4pyiAgdeceWxw+y+WWoxK26jb5YyZsgJNRQv02wQ51GbaQAf8Ri2rlirxgzhvDVqKwZiCp+n+8Z+SJTlFLkxHxQgqNOA8tRYxacaGvwZxYKSvWMCmiRAWdQ6fQYpHJUaHhKLWmVrkiolrEmIxr6eRgQD+UYXZbLF6iKeQk/LD3OkBSoaCRY4S7Vs83ycjlJRMjxlJpEKJKteOXmfLfGfpiSFO84e1KZViAxuLxzwTlOBSAw+IYMciiqNsXHHDZleKN4oGwsBhYyYfZ6h4iYi5wFUywA/TGvHkVIvGFoXd1ihgOhOCswZOnbmcDmoeQnzjGiIXAqNGfy4rOUJMAfLB1NHO9vljySD085wykTlFHnDqMETOGlTKzywupsOk+5GChSqt8DsB0MH7gnEp92JVGcUFnJ6ffE9V5H+MGrw9kBLfmMzy9PbC8EA++PRTT6PG6Ps2srb1/fnjr3Ac/+Iy9OoRJZBq8yPC31BxyRttue+N1/Cziq+PKtzJqU/oHX9fnjD52PlC16Kr9t/QVxbhjU3lRKtcDAncTcL6jG64jl5Enl+/nhNmEIIEW648j9Vo9Lx/IcopPsS0qAQqwvyI29R6YZd6LhfCBtb54sbhxqA6BdeX64Y8O4atJC1YEabkzb6fLCylyGyZYVb7+hUVGoTvbDHh7BHZ56CPU7+2A0o6mJHt6Hlgk5Ug3G+F6YsuMlTDlzJYSSQCDf9fXEcstNiDpIgRqP3/XBGVyhYAcgPthXmuJ922kaYM/S9zPT9caMXj5MitIwynCOkaTh2SCrJ+I7nCPjR/nDTfw4M4TxLv6cLAYD4fvj7OZYkzuYj6YPkQeNKLRPA/8AJykxe2TBIbnGJGqKSyJMfc74cUsoIXrF8K+LaEVgzKBaJIH1xmjGTLRyRk6fQh4o4YggLe88h7dcJM0AZ0388OMxl5nUPbCyrvpiB5Y0Y3R7OKKUdCB6BLmdvzHy6YJK9LYI4gywB1/TC964H+cb4tyVjMub1wJWcTvgfMZwsYXnhrw3gWz1DJ6YelFXIzZMiRDIUTUPhWfM2GNN/wDA1dKxVZeoUADFnD8rcRjXZWj4RgQXNnm+T5Eo9GDq0O8pMsSYOnzIFh64w3cnVEc8dD4VXK0DVrQpUkn/AIjkPP8A1jFVoqVSU2LGBHIm2NmNcW0YIy5Kyru7EYHyOZelVFRDpZTqB+49D09caLLcIashNJlJB0+IEEwL9QN+ZF8ZusvTffDzSZbx8m6Z3PsvxD8VRWpyYEMOhFiP3648zXDWFQKPh3n988cz7D9omyzlb6GIJHQ7SMdRzPafKpZ6yAkTHr1tb3x4mbxuLqmUk5Y53Hplq0lSIJHKDzxns/n+9qlEM06cSR+Zv6fbB/a3jCU8qXWGLjTTIO5PP2EnCLgWX00YaxAuPO+M6xNR5Mv46v5S7HtKh4hHl/bDape1tsLcq5Zulvtg6YnEU6YuW7J5h9GXqECSfDA5zyxgMwwqlgZUSYi8ctz6Y32d1dwzJErqInaQjRjmGUzuZ1aAiFSCQ881BawBMgwBfrj3vEf+NUZGk2/yM+FUHo1u+pl2FJhrXqGOnVbleb46Oy/zDG8/fHO+xmfr1q5pVVRBUpmVAmQBYgzM7bjpjpGWpxcm8CZwnmx5RX5Fbp/0eZkhR/bHN+0WQXNZtKYMCkpNRxa5jQtufO55npjbdpuIrRoNV6CFB5sbKPcxjIdlafg71rtWYuT1F9J9yWb/ANsYISeO8i/CLYIKXx/2EZlQqxPKMJcw9zGw/f8AbDDjtTSTG2ENWuAsHeJwuOLls+hww+CYHxCuGY/lAGFNWqpssepw74TwU5ksSYUfU4HzHCTQJmmKg68xy9Pnj0oOMdEcuR3UQHh+YpIwLCVUySLkzt7C+Nlls7TqCUYHGVy/Z160sid2m8G9xb++G3Z7s/UVr8xhcqjLd7Mbf2avh4540OXqiN8c47QZ+vTqNlqIhlAljaT/AErP3xmszxnM0XZKmpmHPWx+xw+KEkYcsVJmizudevlzRCqsmSwYkEC8bdYwuymSph9Ks6sqKWYEEAkxtp3sT7YzOXzFVQAHMTebztbb1x7W4gxB1G/L06ROPVcIro8uDlVWbPg/GUokWLU5kH8wItPnPNfljO5zKOKrLSDVVBsUBa0WBtYxuMfdn2GsVXEqBcdZMW84mMFVa1Lxlu/LM1lRtIE2CxNzEb29cSk1ZeCcWD0VqUm8SMDEwQQQDzvywQmZLnmT5nb6YPPDmq0FEnXQOiWI8VNyXpyfI60nkQoO8hVSBVipBBmCDyOMs39Hp4GpDJMw+kISdI2UkaRO5gC04cdnqbmppkANvvcc/vijgmT7w3LActvSb8pjGt7N8LaizF4JBiR57D5X9xjz8+TTRscoQi/sc0ael1MYJq0rmML+OcbTLlQQtxJk7CY2G5xjuI9ulBY0qjMSSJJgAcoT3ifLbrmxeLKSZhUZz2bzi3EKVCkO8bSDM+Qg39oxzDN51ctXJQrGo6QwGkAnVC+IHYiT1xm+0faKtVJqVH1GACJsFI5RYb4XZPiVQquhqbhYWHjUg2EyLr6bdMexgwuEKITqLq9+zp3Z3M06S1KxFyISw8OoT4SCfDpO3LT6Y6Dka4ejTddmRSPcdccLPHYp6QylVBkgQCYIMDpv64tyXa/N5WmiM5ekQAFmCvOxHlyOEzYXlVInLRuO21U5nM0skhMC9SDBEgaj/wCq7ebLhjmE0tAAECAByHTGX7D9p6DZuoMwWFeoxCu0aYYhgouYnbfkN8bXMZMljAm5x5/mwljcYekjV4c0m7MtxqTjNskkk8rD1xreKwAwwoocP2J2F/UnC4Z8Ue7jn8Bhwd+7UD9+eLc3qdrQFPPn7DacUFYx6DhkzHNbsbZdkVNOPMsZqDCV8ytJgap0g7TsP84ZcLzKM8rURh5EYrFO0ZZKrG3HuzdLMEOZ849cYbtrwenTqpToJsgLepJi/tjpHeadTT5j3vjm3HeLHvmbSzFibDkB4R8yGONkn9GbE6l8ujLpkByYj1j9cU5rhDGCDPlgmiWj4T7fvbBVKrBG49cetJHiQnJDHg+V/lKW0yN46xpk+cAA8ueAOMr4lOvSefoP374aIGWDurbx9/XA1ehTdGOh2YCRBgiZEFed+keuMb7s0QmE8DoImWWsN5IWZ/q8THkRaQNpwBkuG1a5ZqYgar1D8Ivclj5E4OrUaxyy0lAUInhUEmebD/sSSZnnGGfCeGGsqmkHSkEUVKYafiHxxImSCIjmLYzvts248vBaLOz2XqGaXhZh4VZTIgNOu35bz6R1w77fcTGWodyhJcgMSLQJksfMnYfoMaHhHdpRBKgMQNTkKCYEajYbAY4n2t4u2YruZ/8ANUgQdlUlQPko9zjNDCpzsq8/KSb9CrOcTqVbliSvw35bkYDWrrXUOUTv8zJ3JwPSbTU09DGI5iqaVZ9AEEyLfDPSdsekoJaROWeTew0HSjSQbG3sZEbe5/XAuX4eppq41BiSp0gkXxGlVLK0mTf7YtTvVRCG2JKqDBuOV8FaJS2fZnJFKQ+KFaLiAbSDG8cr8wcMOJV9dHaIg/cYlmOJfiKQDXbu7nSBdAoabST5z164opVl7tU062kA9IFx87DBYFQI7ywcb+EfIC/zxuOyPbDNJnaNB6pejUlCrwdNjpKsRqFwLTGMhn6ahUK2lr+xi3lijONNVYMECQR1mRgTxrJFpgumdg4mnjkm2JrVGkCMLaWb7/KUa/5isN/2WzfUTgLK8WGrQTjwFikm4v0fRRyqWNMeyMWLm6NMjUyj1IwGh1bY8zqqFh6SuPNQcUhGmZ5uxxU4jlG3qIfKcUcI7K5d61SozMRMqqHSI9rkbYyDplWMNTq0fNdUfqMafhuTpUkSqlWrI21QAeWxAtjZHWzLkSjHsM45n1RtCwFH6efL3xi+/KRaS1zhpxCqmos7zewFxPm20emEOczCOE0lpVYb1xox4vls87Jn+Lr7EVFj3kx8C+Y2Hv1wXkGeE8RnWSTM2Ak2xZ+Hj395BE4Y8Pyhbw+RG3WJ+uN8jEpoO4VxBH0yAup2VBcyFuD5GMGZvLqp7xSByjr5YV1OGsp1CxC6VPrY4ilV6Y0x4UVUp2E6iZJBO1uvTGOaKRXtB1CuWJUkSL6RuAdpw87LVXprXDHQWQ935lDqWPOMKGoTFWifGDDr4QXI8IVm6D9MGUJ8alQoJnaYPOCdp254g7TtF001TI9r+MVGyrBlnWwEx8R0mQb/AA2B6288cnoBtaAEDQNV5i19+Vvb0x0Ltqy66QpOClNW1MxO5U2Fr3AE9QRyxz/hEBKrNYhLe1ji3jxajvsrJ/RRn2j+YNndj5iD/nBNOqGqVJG6cvvhfxONKXJsYH+eh++PcpVM23ZfpjRWhL2W8KqqJBA33ImBsTHP0thoKRgFY1QfcEXwiSmdAK9f9YPoZ4/AfC02JmxG/wA/lhZRb6DGVdjAx4YIk6ib/wBQ0mf+UgGPOcUVc0gCqllVoJ/qOk8+mKKVRiGBNibjz5Ni+iAFqrvK6kkAEhSRMXg22nrgJfZzddFdR/DRB5n/AO18QMSKkzLwBPIf5xXWrju6JG6qx+WIZI6qDG0gjkJO/Ppii6J3s0/ZfiTrNEkwWYkbrESSCOYaPUHywXU4TVquBSksTYYzuSzipUpOSYMFoExuD4RE+GcdXbOUqOWFXLMGDfn5neQRusG2nGTNjancV2bMWdRx8WJsvSzWXX+dTkLuykNp/wC0bYd5Hi1NxeDgfgnH21S5EkTq+kRgUUKbkk02DcyvhN+cC30xKWD2KvMu1JGv4Y1NtwIxl+0HE+8Yolkmw5kDmMeOVVCqd5cfmv8AphS5tJviuPH9mbNnUugLOVW+AXkXP6Yoy1HSDBG8GRO3PDGkoALMJJ299jgfO0IjSSOvnjVGRkk/Qty9WLXt58saXs2bsesATz5nGWQGfPGx4BpWmCxAMTMYpldIlVjekofUAVYL8Q6TMT5+W+Aq3CiWOlSY8Vx1Mcxe5GGuWWmqnMlNCFQUYADvGgx4fS6k7EE9DhFnOLVqn/8Aoy+SyLGWMjnvjHKJaCrot4bTFMFDaL32nc74nlM5rrKioSpN3YxJAtpHP12wF2T4kor9zmCWWsQqs19DWgieR2jaYMb4ecTopl2Rqis1IakLgxpN28QmRIBWV5+uEcPs0w7OcdvGFPMN+HeUMALpUn4QQZIiJB084vN8ZzTCLTABZmuYveZvz35g3xps0KeczrBiqloiCACgGkaf+a2IB3GLqXZRquap0AyagfE4VgWSfGxUiNQGxB3ti8JJJItJey/+F/ZSnmzma1W6BXy9MEX1MvifyKggC/PyxzqqHo1HVrMhamfIqSp+2P0Lw/gqcMyTw7MlMPUZnHxG7GYPQAY/PWYqNVqPUf46jlj6sSWPzOGg25SvojoKpkaadMC8y3tywRxCgW0EESBBHUm/2j54q4RTJcHof9+2Nd2eyKOpZ1J7zaRtP63A9sLknw2UhHkY5swdEX1Cyiwi5N7X3/dsGvp0HSohOrDUJ+4k8utxgirwYLnjSklV8UGxjkPXA3FwZIW6qbkjxf8AUxvvM4PJN1/Z3HVgb0SVIRS3gmB5SzH00gmemKuF1gPCSYYbxMe0iem+G3ZjPGlmBUADIqkVAb+BgUYATOxMRtifF+w+ayqCtAqUT4ldDfSYKkggFZBFvXFLXTJPsXNTsyPCspkGL7Wv08hjf8Ho1BRC1yS5XVc/GD1PMj5gdcJuE5bL6lqV3plhuq7L5HqepPUdMavJMtSnqVWMEyzKVG9oJ9eWJSnboaSqNg7UOigHyY2+uJ0JG2odbz98SKcwp+f+ceonkfn/AJw1GVsuSQNRJgRcx8owHSXW0DYXP+8eZurPhG+2CaNPRYXb8x5eWBewVUbKs1SBsCLXMfKMB0hcyAfU4t78I7KbargmL+VtvfEjTsBA6/P/AFinQghShLAA7kC/35/bGzNLTSIJkkhZF4k6ZAi9jIj5YR8Fo66gEGBef15xja08mGqZdIOktqY3/Kpb2v7Y7K90chVx0aCmXQylOmAsdTaSp5xvt6DCtz08yOm4UCeR3+uD+OvqzFQ8g2kSeSCZDec87b4VvIPObW52Ex/yucRZRC/iyQZEzuOovY9OUzhp287T99lcsijxvSNWoQYgqGplRznVP+cLOJGwHt9OnK5wN2orUxw6mkp3gqvYxq0nTAEiYLaj7HDVo0Y3tGZyyRTJaG6AiRPz38sdM/hNmSmXqvU+HWAtz0uBva645clUCnHU/aw3PvjqXCVGXylKj+YxUPmS2oj9PbHJNspmlUaG38WuJ91w2ouoBqxVFHUagz+wUfXHCxT8Os8/Co+5x0r+IPC63EKtNqLLoppp0k7MWliTsAbb3ttjn2Zoaavdl6c0iVuwAlTB3gbzfDISPQXkcuAoHNjojy3c/wD198b7hemm6CqDTKISTyaBuI9cY7hGQFeswUkog0KQfiPxM09Jm/pjW51qn4Wqai6mFPRTYDe/i+giR54zZtySLw0jN8FzBqV8xmZ+NyFMflG3pbFJURVYm5cxPy++FGVzJpU1CuoJ3ANxfpi4VXIOknUTeCD9OvP2w/DbDdJHyZbVWSBGt1UkRzIFvn9Mdi4plVrUmoOQV0iAxIB0/DqIvFr45FTp1abq1SmQQQZb2g9OW846DnOOhcmc0tM1GYDSg2UmdTMbnSsbDfyEnBkm6IT9MW9lq2Xy1Q0mWmKhMrKDUVIkE9B6X28yNXxHNoCF7yWcbWnrGnkvUnawnGG4Qhz+XavUb+Z3pDqhgFV0kADkBO0+eOgcBy1MZZ6FNKaVFU2VQJtY2udxhJKpUx/3RtCMqOn2xRmSVEwQDYHFeWzckq3xTb9cM3UEhSbDfnizZhqnsVZCkSdR25YNriBbbEKljYQOQxGbH9++Cl7FlK2A5+irwWmx+mIpmgJ1sRe1txti7NsAp9OmBsv8IBC26zzxVIUbdl6Eyd7i/wA+WNhk3C1aYDRZmNiYBgCeUEnc3tvjleW43UVIpnSDz538+XrbE/8A5StBh2VfzQSJ9b8zffHSxtsKH1ZwzORsxN9xDMWBK8jH3sNsCsdRck2JgWMXgeo5/PCvK14g6m1Hf15EdPQYvbPKFA2eZLXIkzAIHSeXXCcBrK+I1JdbHr159RysMD/xA4OqLlnMrNCnEj4gZbVY/FLEemPKNJndgX5CdFgR8ieQwRxzLA5CozM7GjVp6QSSIfVqBkyBabc4x04tJNF8MlypmW/AQiVJ1AHk3LciDMHpjc164AV4e4E6t/ELr9RtbGXyRGhCVlSw1Sb/AEIgE/bGgLaJZhJkEwPOYg8sdi3dlPIpUNzmGZdVkXeNgBv8IMk73JHpjlmcy5LuxVhqYxKnqSTO2NH2l4qPw2lWOqqY9FHxEx6gR64zmSd9KQbGfzHqehwGmtgxx0GurIlMKHFiTp1D4jaY5wBbAPEc41UhSSAsACSSbdSd5++GINpILWub3357fr9sLkXUDAAJEg8/ITy2wsa7LNaordHgalUtsSSeXUSBiaZVmMEgAf0gAfP/AHj7NA6xI+MzA2vc/rgyi3itf7e3LDNujoxVhGWyJDKoqMEJAZWPxCbieVpjpjo2VIpKqUwFCABR0A+/vjBZl/CGGNfSrAgETcA/MThYfJbJeVHjVA/crlc2KlFBSy9aA6gyqVN9cflT8sDqPLC7Odq6n4ljQmi2nQTMuw5GCIX6nDytTWorI4JVhBHWf2DjPcS4QyBtWl8xQUFiTPeU2JFI6RHisA2+4wJL7BhlZfw7UWcFpazSzSZMg+mG2UqsTDHr+yRvhElV3VTqXxU5GkATBnlzgHBuU1QLn0P9xjo7QMyHLDzxHTix1xSHIw9mSgXPL4QMQq0T0BxdVeWvyOLaluQw6ZxklQ2wZMKZBM2/fPEEp73/AH9/tiIeSCPb+/vjQzuyVMxYb845eQ8/PHg0gxuwOy3P0xF4UjeT0O04M7sDYActoxJj9FNEP4ioUc/Ew6babjlzw0zlKeEViSCzZkT/AOqiLfPCyYkBdhJPpgmqjGk6ybiQI8iLzeYY2xPLfGimJpTTFdHKnumC7BBrjYajZT8vPB3D8walMElQxtpIa+kaWPhm3hPMdce8OMvUonwySQABLqY8V7CBAj088Lu0GWFClHOozAEW0jUWdo/6sF98DE+OyuV83T9MzfGc6HqO6iFYkKN/CDv7m+CaC/y6XKV/U4WpQao0KL20jmZ+ED2jGk49kRlWo0GMsKakvykySI6A2wZjwasGrLAALCG3jePl6H2wA3geCwjkRthjRrrI8QEedsZ/PHxMQx3/AGemJwV6KZGlstSrrqfTDkhVPMHbl054RcOENq6Yd5dSx1EgdJj+/wCn+WloGPYZmiNAF/0xpuEOpRNRiKaH18I+WMzmagi9zzvb542HB8oVoI7LCmih9RoB+2BiJ+ZVI9pZhA2thCKJUM12PL3/ALYW5kKD+JFUd9TnUCJ1g/Eig2JKmABeRygT72ipgRTXfe/TpPS/0wACRFM3UxI5GBHzvbFJRtGfE1HYqy+ZipTVifCgJN9z58wNvnjVcEqagNQjyIO3IxykQY5YU0+EJTqLVUkG4BHKQQVgyNj9ORvg3LkqNoI5ybiel/vievRTI7NTWqiJi0dLTheMxq+HRPmb/IYmmYVgUY8rCJJNotMAeZ6G2MvmwdR5fpgRJKNj4AEklhPlb/OCA6gC4Hv/AJxkFnFwxWg/p/yQrNyPv/bpfH2VB1R1/Z+XLC1KjM0/X9f0w6y9K0zBPzA2jFZMRKkTFAFpHLr+/wDZwSco8yAIj29MMOH5Pr+/9YNalyi2JOQGI0paWk2J5A287cxgs0HeAoPmTb/P0wxyVHYkAE/f9jDBMvOJuYUJfwpZlB3AiVsVXpqNzz6bnrhPxzhyVNQiKNISxvYBdbAf1OxJ+d9sbTN5YKm4HWf3PsMANSmmVAJEExHqb8gTbCWNGbsznY3hZZ2zlVIAJ0COewHoohR5+mAu22TDVFcj4hG/w3Me2NetWERDCIBaLj5iI3G8b88JO0mX1AMJYDqN/O2BKVbLYfnkoxYyD7CPWbfa2FXEaJQ3IM9MamqkHY8unOQfPGX4kPGRh8cm2XywSRLI0pI3i2HtDLsT4VJjlEcsK8vSbuwfEALkjz2vywRlq5gtLELAkEjewFiCcGVs6DSDs6tTQQR7Rjqo4ctOigdnISmqwxtZQI0iB88cmpEsVgkamA3JkyLGbiZ+uOiVHzGbbRTVnaNlECOpkwPWemBj0R8t8qoQ1fHVLxabfYYOGRC35gj++CaXDHpSHUAyOYMR6SMEZoQob0B+W+HkzLfo+bLq4ggXF42nqBy64lmOAvTp96dLUyAJB2IsykEW5RvgLJZ1V/K1n0sOhItHkSGxqeH8RVGVKt6FU6X8pHgf52wjW7CpPr0c9r09LEDbFTrOHfbThLZauQT4D8J69D8rYS0kJ3Bjr/npglU9AtUchj7vQN8Ms/RUKGUCPv0wv/DzyOKJnWVZcASYsP3thplVLXYRPLoOXtgCmVIuec/LDp0NOCfhaxI+0fv9Mc2I0HpSJp+EkMvOelj5+eLKfC6ouKsE+RP3a+KOF5pdelWnUZFvnIjnAwzo59LCZI8xEcsTcmLTPstRqhYLISCbgGd/6bDn1wyo0iVJLzp3AAU2G15InANTP06banMKSYsTuF6A9MWZjO0qhRkqqPCZG08wJ+djhG7OpjdUSAQIsLkQfmZ+WF+eqX0lwFAOojoRA6+vti+lnVjYGf6WBE+vKekThRxZyFLcyyz8mkfbCrs6gMZLOGnrak2gfDqKqGHpIZhzkb4XcUrh8uwRNJUKWAHxXg85kTM72w/o8canRqavEKiMDeImwvzjoMK8rw/v8tVrKRIBXugDraw8Q5cyY3MYfIk4lcEqnZhO8JUsRUXkNQPr02+eM9WcEkGSfXGhqiKbBjpYMOkxDSI9Ywjr7eZJvaeWDjNOQ0tLIu9CkiCSaIY3sOhJ2GwF+cY+y/BqrK660WCtrwel4/THvBc2+mmoBINLSReCASSfbwY03CqQNFz4Z1DexO/Xf0xGeRxlRSMbjZn+C5QjNUKTrBDsxjYgAn0Pwj5jGxTO1KSimhgO51QYJCkiOumDhBXzZpZ2gkiGMEHmH8J9OWHw1d0fAtQh7iI0iVJJJ5Nqi+18UjJ1ZlyrY0q8SoZzMUKawtWCrn8kKJuZ385xZxnh3d+DUrrbxKCF6Rcn73xm+IZKmr6svMKCGCtuxn4SeW2+B+H8bzLL+FvNUwYuT1j7W5YduyEYBZyUE89JkjqDefX+2GzU5pMpIkCR5xce8iPfBKcOSmg72oz1SpKQCJ0jZtSyW9OkziPCsqtVdUgxyN4gfbE5NoDSYTTqfi8v3egGpQ8VFnFqgAIKxvzt7YyFXL1lPdsFUkGLm08h6W9iMbPLTT01FDHRYhbkRb9FOKOMqKgFVEKktInk08+gOOUhrMlRytSie7YqyydJ8/lz6YPOWcEiPPfrfpg/M8PZhUAILIZIBB2F9thynzwVS4VmGVdWgQLE1BJBuJib3xRNgZieHZFapP8AMXXyUA9BeTsLxgr8W8LSkGPEAwttJWI3thTwusoXVrKzvsdse5PNyxe5jfwyBNpJ2Hv54Zjpe2aDI5SpVKEslJX+FiD4pJWFVbkg9Yi0nCitw6qHcKpdQxGqNzO1zvi7O6QmqlE6ibEeEyJQH+kG48vQ49yXaRqVNqLqDqZmLaQzSQBaTHTHP+BU2E1KD9yisWUk3VvU7dBABwTTyxptEGwqRI+KAy2PO+CszTepSoldAeop1Bm+GCL84nlPKcF16ncirrKujVCYKzpJMNBnmLGN4HTE7+w2C8No6xIYgz06X2/TF+fy7934iSI+IiBuYg7HffFeWr91UCkMwJBISNWnlAIvtP8AbAnGM8lWm4oiqAlyajC4N1CoLBfOeeAouzvYMmkAh9RMArG031A/46Ysrq+XUVaZB0ur738N9MfLC3hWdQqwqMym2iANJmQZJIKwNo88fHOMWOgAktAO0fvrhnG00NHUrGvadNT1m8P5Tt6/XbHOOI02ZgInxY3+a421XXpoVarNTXUaSavEIuYuAb4xvFqLqGJo1aewbWjCNQkLcC5Fx5Yh40ZRWzblcWaLheapUslSqKgchCrN+ZGBOoDrY87WBwhq5+pJWm7ga5En/jPpgvKZGumVcdxU+IljobwWAIIjwm03NsK8qxPIAbX6j32g4dQ+TbFcvikad6urM06ulvBTk3EkssmI5DDL8S1QMqVPC8BlYEExym/rEx5YQ8MQhwNW436WtzwwAEmZ1jYgxA3Jnrh49GfJ2FvknpoUEmAWkXExfxbEQB0wloqajDuydQE3N72398McrxOrpKWYAQJGwJsPn59cWcIdabs5WdSkIRa8qSYkRH64K1Yj+0D9xVRkp1DpLmFJa3U8/S+NO/aJKdNKRYEooA7pdM8iGc7zzscZHj01KuskCTAUm9vtiwFfCSai1ARcAET15ReOuA4nPe2axatdtTClp7rRUdbNCyYZjNx4eQ9sFVOKUoALAiyVPOV1d4P+pLL7DGO4bma9CqSriqrjS4ViSVJvKMA3nYYuz2nuRc60P/5KzH6g/fCNUwqOhjn81+GbQLMWDLVXcgzabypm4H+vKXF6omWqEzfSRHpf9MLspmu8pfh2jvAJpkwQBM6CY9/3GKxxynQHdqgZgTraooJJ8ui9B8/KkW1oWUV2ZPKkDwmT1Exy98OBxFWVxEMUIssGeR8Pht/1nzwFw/I95WdQVjxwWsux0+lyIxBKZMjkIBj3/scUG7JIX0lVmDuRv5rE7X6c8X1SWKrBkwPT1wVlOHFqJJWYadQOw6H3FsDqCsczyv0wLOouy9fQWRnPQwJmRsfLDKtxlgAG8d5AIkC5N7eK94/TBHaTg9LL08vUUMWrqS2puYCG1v8AkcE8Y7OUV4fl80C5eqbgkabqxmIB3Uc+uOcdg0KqefYsfEdRmGm5ncE9D9D64toVGKuGEE02EeniPvIwzpcCorw78Yuo1A2m7Sp8YQmInYnngz/42meGtnTPeqStmsZIQmI30t15Y7i7A+jEVHBGmLjf++GORq0li/8AM3C+fQ/LGj4bwngtc00OarivUCjSFf4yBYE0o36nANPhOSyeefL52pUMAMlanMQwkK6aSdXORO+OlF0XjCMv29/X/DP8I4itHO0XclUFQF9JNxP9I3PkOptivhHaBEytenXprmKj5imwSqahsqOpOpWBBBIAk7E2xpeyHZLL5+rmRWLhKUGm6PfSSdxpPIDz3tiVPhHZsGfx1ef+tT/+OOgmognJNsn2i7R5crnKRdIqVjUovpZgRpCkDSQQTpEMQw3xzbKZy/isp5dPPG77E9kspnVrtUNQd2pKFX3EsACGB/pBtG+Ob1akpI3/AMYPHQL+jQ/irqwJIBHPbkfv++bqvm1ZVMANGkx1GzDlhp2h7D0MrTyr0nqfzF1OrkMDZDaw0/EcP+OdneGZXQtepmBrBZdPiECA2y9SMDgJKSezE1axWlvMmx9BcfUYUPVYsCSd9xy6Y3Q4bkcxWoU8uzlCwV9eoMdX5gSNNoA2588GcU7McJpZgZV3zZrHTCqC06hIuEj+3PDRiCznbkg6idXnzxac6y/EJBFr9drY03a/IpkK3d0CSr0/EHgn4iOQEC2Mo+YEaVAIsbgb+pGw2wJKiuHFzbtpL7Zcc8GPjQNzk/F6hhBwzrI4pyVMQrKzSVAI+Es220z7YTBQQCB7dMNKXFAQiVBARNJaSdW0WMwQPDa2JNJq0Nnwywz4yADWCmNWmG1eGDDdd5I6QcN8zklzUVaZXVEOB1HP3GFXFEllsCPvfFTBkJ0sRPQkfPDVe0RQipZrzIGGWSze5vpP6YzocHfDfhGbKysyOh+/rijRxo8tUJBCmxsf31wuzFU6o6RA9LYMynF9BbSFGogi1lIsR6HADllbvIvq8jeemEV2GzY8dds5k8u9LxNS3UbwQFaB1BUW6YepQNfhdCkLNTAIBnlqBsBMwTbyjHLclxqpQBK3Ba4PXnblhzV7b1W0aPAQZYnxBuQHpv57Ytp9k6Zq89m1TgrqoaBWAh1Ib/zLMrYj0x7wrOg8CqF9u9uBvHeL154zfEu1j16IQrB1Asd/ghlgebAb8gcLMlxir+GqZMiRUYFTzBLAkeYMYOjlYT2YqgZ3LdRVT/8AbDT+JxnPM3/FI9lEjARyRoV6NVgT3ZUlgZnSepAE22x92privXNUfCQLHlAgzhfQydSsZ9gc1mqdRzlVSqGp+OiWhmXqk/mEz0N8YRuHOrlGkEWYGxBG4IN8bfhVc93TpsquqyV21AySpBEMInriwdrKGsfissuYC2JfSXWDFqlmYW2eT/yxJfRplNZHfsf/AMKUWnSrEiR3QnzuxP3wjXO9nao/mZSvS/6hhFv+FQ/bHnB+1lGimYYU2COzBUWJUEsUG/IEC2MC+chAoHI++w/vh1Klohx27O49tqlM/g9K66YptpBaOSRJi9txg3tTxChTpUatXLpXVhpDGDpmDFwbH9MY7tDxNancL3qK1OneTsSFMesDb0x5wrj5CGizJUp7BWEecXm09cddNk/RLstV7vOoVAUVGJ08gCxKj2tGNpnM5lxmXhYzHdga2FiIsAZ9JAifPGMpnTVWoihAt9I0wd7zyN4tiji1VarNUYkPaIcCIFjI546LSObM7xniNVMzV/FoKrvEzYCPhKdF3j63nCurUUEmmG8p/KOnr54f5zOjMKErjxKDoqWv1uPa2x8sZtVUSoGqOckdOW3XHTV9GrxckISbmvx+RtwbML3bKUJO7RA8EQd+Y5Yvy2U71NOkahZXLgQJ2K8xff8AtGEWVqBampRabBr26HrhzlM7pJKjTO0E26xfY++w951QmWXKbdt/kkyGFpMBKsQD62ielrHFlekwYrq2/pII+Y9MEU2VyzuCx9bC25PP0xTVWmTZoHof744mYSuquC4hST8PL1B/TF/C4F2QEAeY+owJlbmGkWm30tzGC6NUiQIv0/tirAQzedjYYoytRievUnlfl54uzSiLi+B4RUmTrPy+0/XHKjhlloZZ00yd/FMz9sEZEaqi0hSTUxCiwIEmAT5e+M6tQzbDThf/ADDaTvpIB+oI+eOaCdKqZDKDNItOitSg6tGhtXiTUjGzgm6hygMw0DEOHcJod/mlaispHdkO+hLNuwIZRYeIghYIO84KTsNk9VAU81mWFYFgZQAAab2ot/VuYFtxi6l2OyJzD5c5jMAqSJPdwwCK0/BtLaY8h1waFFH4eici1QIvfmXUF5OhWFMsEJ6kt7Y+4jTpxkiaR0sVVl16XfwoWgltBDSSHBWJCkAicH5fslkGoVcwuYza92qeGUUsXUEIC1Mc2C6trztjziPYXh6mg34nNMtdNStNKASV0qf5e51H3Ec8dRxe+e7utVRUWEypqIsONJCiJVmJUz+Uk+pxmeJZDKnI9+o/nikheXPxVKxAePNVdSItAPPGkf8Ah/k1Nc/iczppBobVSliihiNOiw8UA32MxaQsx2AyfdJVNbMEuWOkFBAVGcnxUVMwhsQBteL4HEKdCXtNk6dOnRNGhoDISykksSqoZMsVYeIw6kAzEAqcK+I8BWmxLVlhTTtpgtrLTu2wCm95EHnjccQ/h9kaFLUmYzT6iqgeD8221NjboAcVn+HeSSuiGvmDIp6m1U7Fy6gQUuPAQTuJFrGO47DyFlXKZf8ABLVpgGqy+OSRpHfVF73TNyYVI5WJBmzRMtl++rUmQIndUgkMRD1BTGoyZI1sRHIHyxav8O8qVRkrVhrrNSgugjS7IG/8JkHST5W9vsz/AA+yi94FzGZLrW7tRqpgN4BU1E93YRPW488dxAD5eoj/AItaS6mp1qiDvHaKaKrlWETJJAALWtHPCw5pTwxavdgPqu7E+KajAabkWtKkBhBaSDho3YvICoiDM5r+ZTpurHuoOsnwkaPiAE+ftjLdtMjlso/cUalSq2kMxqaIWdgAqgkxffphqAKa2cZjJNxtAFsRoxvhSKu/nyxea2kAj7YDQwwq1I2+WIU871Men9sKzVBveT+x7YIyYDTPIW/vhaOHVDNxIJuLjz9MenMkmb+2BBTgC+L0RyLEwOlsA4RZTOEDTaOh2/fpiiqYMjfFGPjilbATqVidzife9b2xTj446gE6bQcHU6h3B9v8YAp74JOAxkNMvx6uoRdfhSyyAY/cDEq3aWuW1d54jN4E3ibx5D5YR1DfFbYKAzU5PtDWCxqEWtpEeGy8uUYjme0VUp8QhYgaRFiG6dQD64S5c2xS5scANDNe0teWYuJYQTpFwdxi2l2jrQFlSoMgFQRPW4xn8TpnfBYEaPN9pMy4JaoCJ20jlsdsUDtDXkPrGpRAMCQBty8zhI52xXgKzmP27VZgWWpENqEAbm5PrJOKW7TZg6gzyGMsIFyBE+thhNjzDAHr9pa5H/kuCCJAkRIEW5aj88Ls5nGdi7HUzXJPp/jAePsccTZ5xNKloxTj7HHEicG5FgbYBx6DgNHDyvWkWMR1xOimpQS8eQm31wtyJlhPT9MaYINC2G2Jy0Mf/9k=",
        price: 279,
        description: `
        Пришло время невиданного хаоса. 
        Могущественные силы сталкиваются в борьбе за власть и величие. 
        Северные королевства готовятся к войне.
        `
      },
      {
        id: "edfr6",
        title: "The Witcher® 3: Wild Hunt",
        image:
          "https://upload.wikimedia.org/wikipedia/ru/a/a2/The_Witcher_3-_Wild_Hunt_Cover.jpg",
        price: 539,
        description: `
        Когда в Северных королевствах бушует война, 
        вы заключаете величайший контракт своей жизни — отыскать Дитя предназначения, живое оружие, 
        которое может изменить облик мира.
        `
      }
    ],
    cartItems: [],
    total: 0,
    count: 0,
    cartTotal: 0
  };

  updateCartItems = (cartItems, item, idx) => {
    if (item.count === 0) {
      // cartItems.splice(idx, 1);
      // this.state.cartItems = 
      return [...cartItems.slice(0, idx), ...cartItems.slice(idx + 1)];
    }

    if (idx === -1) {
      return [...cartItems, item];
    }

    return [...cartItems.slice(0, idx), item, ...cartItems.slice(idx + 1)];
  };

  updateCartItem = (products, item = {}, quantity) => {
    const {id = products.id,count = 0,title = products.title,total = 0,image = products.image} = item;
    return {
      id,
      title,
      count: count + quantity,
      total: total + quantity * products.price,
      image
    };
  };

  updateOrder = (state, productId, quantity) => {
    const { products, cartItems } = state;
    const merch = products.find(({ id }) => id === productId);
    const itemIndex = cartItems.findIndex(({ id }) => id === productId);

    let item = cartItems[itemIndex];
    const newItem = this.updateCartItem(merch, item, quantity);
    const newCartItems = this.updateCartItems(cartItems, newItem, itemIndex);

    return {
      cartItems: newCartItems,
      cartTotal: newCartItems.reduce((acc, item) => acc + item.total, 0)
    };
  };

  handleAddedToCart = id => {
    this.setState(state => {
      return this.updateOrder(state, id, 1);
    });
  };

  handleRemoveFromCart = id => {
    this.setState(state => {
      return this.updateOrder(state, id, -1);
    });
  };

  allItemsRemovedFromCart = productId => {
    this.setState(state => {
      const item = state.cartItems.find(({ id }) => id === productId);
      return this.updateOrder(state, productId, -item.count);
    });
  };

  calcTotals = () => {};

  render() {
    return (
      <div className="App">
        <ShoppingCart
          handleAddedToCart={this.handleAddedToCart}
          handleRemoveFromCart={this.handleRemoveFromCart}
          allItemsRemovedFromCart={this.allItemsRemovedFromCart}
          {...this.state}
        />
        <ProductsGrid
          handleAddedToCart={this.handleAddedToCart}
          {...this.state}
        />
      </div>
    );
  }
}
