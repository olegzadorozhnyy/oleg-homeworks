import React, {useState} from "react";
import "./Accordion.css";

const Accordion = (props) => {

    const [isOpen, setOpen] = useState(false);
    const handleClick = () => {
        setOpen(!isOpen);
    }

    const {question, answer} = props;
    const className = isOpen ? "is-expanded" : null

    return (
        <div className="accordion">
            <div className="accordion-item" onClick={handleClick}>
                <p className={`title ${className}`}>
                    {question}
                </p>
                <div className={`content ${className}`}>
                    <p>
                        {answer}
                    </p>
                </div>
            </div>
        </div>
    )
}

export default Accordion;