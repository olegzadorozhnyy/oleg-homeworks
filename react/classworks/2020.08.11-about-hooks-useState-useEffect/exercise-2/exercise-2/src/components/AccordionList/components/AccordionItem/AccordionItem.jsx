import React, {useState} from "react";

const AccordionItem = (props) => {

  const [isOpen, setOpen] = useState(props.isOpen)

  const handleClick = () => {
    setOpen(!isOpen)
  }

  const {title, text, id} = props
  const className = isOpen ? "is-expanded" : null
    return(
        <div onClick={handleClick} className={`accordion-item ${className}` } key={id}>
          <p className="title">
            {title}
          </p>
          <div className={`content ${className}`}>
            <p>
              {text}
            </p>
          </div>
        </div>
    )
}

export default AccordionItem;