import React, {useState} from "react";
import Button from "../../../Button/Button";
import AccordionItem from "../AccordionItem";


const AccordionList = (props) => {

const {title, accordionItems} = props;

const accordionItemsComponents = accordionItems.map(item => <AccordionItem {...item}/>)
  return (
    <div className='accordion'>
         <h1>{title}</h1>
           <Button text="Expand All" />
           <Button text="Collapse All" />

          {accordionItemsComponents}

    </div>
  )
}

export default AccordionList;