import React from "react";

const Button = (props) => {

    const {text, handleClick} = props

    return (
        <button type="button" class="btn" onClick={handleClick}>{text}</button>
    )

}

export default Button;