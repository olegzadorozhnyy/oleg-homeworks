import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Navbar } from './client/Navbar/components/Navbar';
import {Provider} from 'react-redux'
import {store} from './store/store'
import {BrowserRouter as Router} from "react-router-dom";
import { Modal } from './shared/components/Modal/Modal';
import {LoginForm} from "./client/LoginForm/components/LoginForm"

function App() {

  return (
    <Provider store = {store}>
      <Router>
        <div className="App">
           <Navbar/>
           <Modal>
             <LoginForm />
           </Modal>                
        </div>
      </Router>
    
    </Provider>
  );
}



export default App;
