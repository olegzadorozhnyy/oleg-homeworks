import React from 'react'
import {shallowEqual, useSelector, useDispatch} from 'react-redux'
import {createLangAction} from './../../../store/creators/createLangAction'


export const LanguageSwitcher = () => {
    
    const langs = useSelector(({langs})=> langs, shallowEqual);

    const dispatch = useDispatch()
    const langElemnts = langs.map(({name, abbr, current}) => 
        <option value={abbr} selected = {current}>{name}</option>);

    const changeLang = ({target}) => {
        const newLang = target.value;
        dispatch(createLangAction(newLang))
    };

    return (
        <select onChange={changeLang}>
    {langElemnts}
        </select>
    )
};