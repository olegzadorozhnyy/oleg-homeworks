import React from 'react'

export const Input = (props) =>{
    const {type="text", onChange, ...inputProps} = props
    return(
        <input onChange={onChange} type={type} {...inputProps}/>
    )
}