import React from 'react';
import {useSelector, useDispatch, shallowEqual} from "react-redux";
import {createCloseLoginModal} from "../../../store/creators/createCloseLoginModal"

export const Modal = ({children}) => {
    const isOpenLoginModal = useSelector(({isOpenLoginModal}) => isOpenLoginModal);
    const dispatch = useDispatch();
    const style = {
        display: isOpenLoginModal ? "block": "none"
    };
return (
    <div style={style}>
        <button onClick={()=>dispatch(createCloseLoginModal())}>X</button>
        {children}
    </div>
    )
}