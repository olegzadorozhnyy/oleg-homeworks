import {OPEN_LOGIN_MODAL, CLOSE_LOGIN_MODAL} from '../action';
import {modalState as initialState} from '../initialState';

export const modalReducer = (state = initialState, {type, payload}) => {
    switch(type) {
        case OPEN_LOGIN_MODAL: 
            return true
        case CLOSE_LOGIN_MODAL: 
            return false
        default:
            return state;
    }

}