import {langsState as initialState} from '../initialState';
import {CHANGE_LANGUAGE} from '../action';

export const langsReducer = (state = initialState, {type, payload}) => {
    switch(type) {
        case CHANGE_LANGUAGE:
            const newLangs = state.map(item=> 
                ({...item, current: item.abbr === payload}))
                console.log(state)
            return newLangs
        default:
            return state;
    }
}