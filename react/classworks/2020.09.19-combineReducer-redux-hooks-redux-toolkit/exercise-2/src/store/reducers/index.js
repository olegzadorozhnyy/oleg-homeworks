import {langsReducer} from './langsReducer';
import {modalReducer} from './modalReducer';
import {userReducer} from './userReducer';

import {combineReducers} from 'redux';

export const rootReducer = combineReducers({
    langs: langsReducer,
    isOpenLoginModal: modalReducer,
    user: userReducer
})