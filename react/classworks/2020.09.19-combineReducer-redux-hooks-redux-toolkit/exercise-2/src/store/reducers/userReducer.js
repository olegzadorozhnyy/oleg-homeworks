import {LOGIN} from '../action';
import {userState as initialState} from '../initialState';

export const userReducer = (state = initialState, {type, payload}) => {
    switch(type) {
        case LOGIN: 
            const newUser ={
                name: payload.name,
                lastName: payload.lastName
            }
            return {...state, user: newUser, isOpenLoginModal: false}
        

        default:
            return state;
    }
}