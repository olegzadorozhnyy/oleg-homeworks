export const fields = {
    name: {
        type: "text",
        placeholder: "name"
    },
    lastName: {
        type: "text",
        placeholder: "Last Name"
    },
    submit: {
        type: "submit",
        value: "Log In"
    }
}