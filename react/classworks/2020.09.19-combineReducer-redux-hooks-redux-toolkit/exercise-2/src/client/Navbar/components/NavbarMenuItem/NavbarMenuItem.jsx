import React from 'react';
import {NavLink} from 'react-router-dom'

export const NavbarMenuItem = ({to, text}) => {

    return (
        <li><NavLink to = {to}>{text}</NavLink></li>
    )
} 