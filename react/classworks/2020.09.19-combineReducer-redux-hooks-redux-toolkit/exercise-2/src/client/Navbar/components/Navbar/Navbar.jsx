import React from 'react'
import {LanguageSwitcher} from '../../../../shared/components/LanguageSwitcher/LanguageSwitcher'
import {NavbarMenu} from "../NavbarMenu"
import {UserProfile } from "../UserProfile"

export const Navbar = () => {
return (
    <>
        <LanguageSwitcher />
        <NavbarMenu/>
        <UserProfile/>
    </>

)
}