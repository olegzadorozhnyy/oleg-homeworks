import React from 'react';
import {useSelector, useDispatch, shallowEqual} from "react-redux";
import {Link} from "react-router-dom";
import {createOpenLoginModal} from "../../../../store/creators/createOpenLoginModal"

export const UserProfile = () =>{
    const {name, lastName} = useSelector( ({user}) => user, shallowEqual);
    const dispatch = useDispatch();
    const element = name ? <Link>{name} {lastName}</Link> : 
        <button onClick= { ()=> dispatch(createOpenLoginModal())}>Log In</button>
    return(
        <>
            {element}
        </>
    )
}