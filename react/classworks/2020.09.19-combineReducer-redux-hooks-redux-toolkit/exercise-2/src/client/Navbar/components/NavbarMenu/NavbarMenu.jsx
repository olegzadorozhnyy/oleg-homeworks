import React from 'react'
import {menuItems} from './menuItems'

import {NavbarMenuItem} from "../NavbarMenuItem"

export const NavbarMenu = () => {

const menuElements = menuItems.map(item => <NavbarMenuItem {...item} />);

    return (
        <ul>
            {menuElements}
        </ul>

    )
};


