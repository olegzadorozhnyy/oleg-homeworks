import React from 'react';
import {useSelector, useDispatch, shallowEqual} from "react-redux";
import {createOpenLoginModal} from "../../../../store/actionsCreators/createOpenLoginModal"

export const UserProfile = () => {
    const {name, lastName} = useSelector( ({user}) => user, shallowEqual);
    const dispatch = useDispatch();
    const element = name ? <a>{name} {lastName}</a>:
        <button onClick= { ()=> dispatch(createOpenLoginModal())}>Log In</button>
    return(
        <>
            {element}
        </>
    )
}