import React from "react";
import {NavLink} from "react-router-dom"

const NavbarMenuItem = ({to, text}) => {
    return (
        <li>
            <NavLink ещ={to}>{text}</NavLink>
        </li>
    )
}

export default NavbarMenuItem