import React from 'react'
import {menuItems} from "./menuItems";
import NavbarMenuItem from "../NavbarMenuItem";

const NavbarMenu = () => {
    const menuElements = menuItems.map(item => <NavbarMenuItem {...item} />)

    return (
        <ul>
            {menuElements}
        </ul>
    )
}

export default NavbarMenu