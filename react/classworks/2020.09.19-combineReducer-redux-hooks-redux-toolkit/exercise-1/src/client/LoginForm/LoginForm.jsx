import React, {useReducer} from "react";
import {field} from "./fields";
import Input from "../../shared/components/Input";
import {useDispatch} from "react-redux";
import {createLogin} from "../../store/actionsCreators/createLogin";
import {initialValue} from "./initialValue";
import {reducer} from "./reducer";


export const LoginForm = () => {

    const [state, formDispatch] = useReducer(reducer, initialValue)
    const dispatch = useDispatch();
    const {name, lastName, submit} = field;
    const onSubmit = (event)=>{
        event.preventDefault();
        dispatch(createLogin(state))
    };

    const actions = {
        name: ({target}) => formDispatch({
            type:"CHANGE_NAME",
            value: target.value
        }),
        lastName: ({target}) => formDispatch({
            type:"CHANGE_LAST_NAME",
            value: target.value
        })
    }

    return(
        <form onSubmit={onSubmit}>
            <Input {...name}
                   onChange={actions.name}/>
            <Input {...lastName}
                   onChange={actions.lastName}/>
            <Input {...submit}/>
        </form>
    )
}
export default LoginForm