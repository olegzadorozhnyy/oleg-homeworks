export const reducer = (state, action)=>{
    switch (action.type) {
        case "CHANGE_NAME":
            return {...state, name: action.value}
        case "CHANGE_LAST_NAME":
            return {...state, lastName: action.value}
        default:
            return state
    }
}