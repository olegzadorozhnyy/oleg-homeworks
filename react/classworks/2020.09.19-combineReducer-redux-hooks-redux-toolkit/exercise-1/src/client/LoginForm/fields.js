export const field = {
    name: {
        type: "text",
        placeholder: "name"
    },
    lastName: {
        type: "text",
        placeholder: "lastName"
    },
    submit: {
        type: "submit",
        value: "Log in"
    }
}