import React from 'react';
import './App.css';
import {Navbar} from "./client/Navbar/components/Navbar";
import {Provider} from "react-redux";
import {store} from "./store/store"
import {BrowserRouter as Router} from "react-router-dom"
import AppRoutes from "./AppRoutes";
import Modal from "./shared/components/Modal";
import LoginForm from "./client/LoginForm";

function App() {
  return (
      <Provider store={store}>
          <Router>
              <div className="App">
                <Navbar />
                  <Modal>
                      <LoginForm />
                  </Modal>
              </div>
              <AppRoutes />
          </Router>
      </Provider>
  );
}

export default App;
