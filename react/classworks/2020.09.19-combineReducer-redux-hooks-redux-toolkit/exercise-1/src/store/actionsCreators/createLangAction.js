import {CHANGE_LANGUAGE} from "../actions";

export const createLangAction = (payload) =>  {
    return {
        type: CHANGE_LANGUAGE,
        payload
    }
}
