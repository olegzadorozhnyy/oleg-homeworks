import {OPEN_LOGIN_MODAL} from '../actions'

export const createOpenLoginModal = (payload) => {
    return {
        type: OPEN_LOGIN_MODAL,
        payload,
    }}
