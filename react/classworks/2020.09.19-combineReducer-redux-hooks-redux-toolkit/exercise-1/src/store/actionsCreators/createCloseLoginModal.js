import {CLOSE_LOGIN_MODAL} from '../actions'

export const createCloseLoginModal = (payload) => {
    return {
        type: CLOSE_LOGIN_MODAL,
        payload,
    }}
