import {LOGIN} from '../actions'

export const createLogin = (payload) => {
    return {
        type: LOGIN,
        payload,
    }}
