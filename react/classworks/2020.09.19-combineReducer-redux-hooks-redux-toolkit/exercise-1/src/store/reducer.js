import {CHANGE_LANGUAGE, CLOSE_LOGIN_MODAL, OPEN_LOGIN_MODAL,LOGIN} from "./actions"
import {initialState} from "./initialState"

export const reducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case CHANGE_LANGUAGE :
            const newLang = state.lang.map(item =>
                ({...item, current: item.abbr === payload}))
            return {...state, lang: newLang};
        case OPEN_LOGIN_MODAL:
            return {...state, isOpenLoginModal: true}
        case CLOSE_LOGIN_MODAL:
            return {...state, isOpenLoginModal: false}
        case LOGIN:
            const newUser ={
                name: payload.name,
                lastName: payload.lastName
            }
            return {...state, user: newUser, isOpenLoginModal: false}

        default :
            return state;
    }
}