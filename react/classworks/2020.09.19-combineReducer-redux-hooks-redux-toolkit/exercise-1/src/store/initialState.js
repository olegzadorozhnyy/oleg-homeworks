export const initialState = {
    lang : [
        {
            name: "Russian",
            abbr: 'ru',
            current: false
        },
        {
            name: "Ukraine",
            abbr: 'ua',
            current: false
        },
        {
            name: "English",
            abbr: 'en',
            current: true
        }
    ],
    user: {
        name: "",
        lastName: ""
    },
    isOpenLoginModal: false
}

