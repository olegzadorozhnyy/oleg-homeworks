import React from 'react'

const Input = (props) =>{
    const {type="text", onChange, ...inputProps} = props
    return(
        <input type={type} onChange={onChange} {...inputProps}/>
    )
}
export default Input