import React from 'react'
import {useSelector, useDispatch, shallowEqual} from "react-redux";
import {createLangAction} from "../../../store/actionsCreators/createLangAction";


export const LanguageSwitcher = () => {
    const lang = useSelector(state => state.lang, shallowEqual);
    const dispatch = useDispatch();

    const langElements = lang.map(({abbr, name, current}) => <option selected={current} value={abbr}>{name}</option>)

    const changeLang = ({target}) => {
        const newLang = target.value;
        dispatch(createLangAction(newLang));
    }
    return (
        <select onChange={changeLang}>
            {langElements}
        </select>


    )


}