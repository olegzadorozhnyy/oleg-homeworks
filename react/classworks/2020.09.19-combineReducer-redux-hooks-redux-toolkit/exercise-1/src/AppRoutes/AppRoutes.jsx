import React from "react";
import {Route, Switch} from "react-router-dom";
import HomePage from "../pages/HomePage";
import ContactsPage from "../pages/ContactsPage";
import AboutPage from "../pages/AboutPage";
import Error from "../pages/Error";


const AppRoutes = () => {
    return (
        <div>
            <Switch>
                <Route exact path="/">
                    <HomePage/>
                </Route>
                <Route exact path="/about-me">
                    <AboutPage/>
                </Route>
                <Route exact path="/contacts">
                    <ContactsPage/>
                </Route>
                <Route >
                    <Error />
                </Route>
            </Switch>
        </div>
    )
}

export default AppRoutes