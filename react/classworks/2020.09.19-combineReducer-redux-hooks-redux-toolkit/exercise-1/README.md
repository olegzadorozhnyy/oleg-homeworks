### Задание

Создайте приложение с Navbar и 3 страницами: 
- Home;
- About me;
- Contacts;

В левом верхнем углу Navbar у вас будет выпадающий список языков (ua, ru и eng), посередине - меню, а в правом углу - либо кнопка LoginForm, либо ссылка на профиль пользователя. 
При нажатии на кнопку LoginForm должно появлятся всплывающее окно авторизации с формой входа.
Если пользователь не авторизирован, то при заходе на страниицу "About me" тоже должна появлятся форма входа.
На странице "Home" должно быть приветствие на 3х языках.
На странице Contacts - контакты на 3х языках.