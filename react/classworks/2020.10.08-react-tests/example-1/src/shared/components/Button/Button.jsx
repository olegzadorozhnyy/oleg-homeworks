import React from 'react'

const Button = ({text}) => {
    return <button data-testid="button">{text}</button>
}

export default Button;