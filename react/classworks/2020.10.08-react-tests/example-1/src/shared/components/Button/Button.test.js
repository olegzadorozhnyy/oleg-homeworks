import React from 'react'
import {render} from "@testing-library/react";
import Button from "./Button";

describe("Button", ()=> {

    test("is element append in DOM", ()=> {
        const {getByText} = render(<Button text="Click me" />);
        const element = getByText("Click me");
        expect(element).toBeInTheDocument();

    });

    test("is element show text content", ()=> {
        const {getByTestId} = render(<Button text="Show me" />);
        const element = getByTestId("button");
        expect(element).toHaveTextContent("Show me");
    });
})

