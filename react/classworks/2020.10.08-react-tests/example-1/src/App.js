import React from 'react';

import './App.css';
import Button from "./shared/components/Button";

function App() {
  return (
    <div className="App">
      <Button text="Click me" />
    </div>
  );
}

export default App;
