### Задание

Напишите форму авторизации (email и пароль), и такие тесты:
- если не ввели Email – выводить сообщение об ошибке;
- Если не получили обработчик из `props` – не выводить форму;
- остальные тесты - по желанию.