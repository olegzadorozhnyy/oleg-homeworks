import React, {useState} from 'react'

const ToggleButton = ({firstText, secondText}) => {
    const [text, setText] = useState(firstText);

    const handleCLick = ()=> {
        const newText = (text === firstText) ? secondText : firstText;
        setText(newText)
    };

    return <button data-testid="toggle-button" onClick={handleCLick}>{text}</button>
}

export default ToggleButton; 