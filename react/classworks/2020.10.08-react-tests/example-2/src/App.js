import React from 'react';
import logo from './logo.svg';
import './App.css';

import ToggleButton from "./shared/components/ToggleButton";

function App() {
  return (
    <div className="App">
      <ToggleButton firstText="Click me!" secondText="Show me!" />
    </div>
  );
}

export default App;
