import React from 'react';
import style from './Buttons.module.css'
import Button from "./ButtonBasic";

const PrimaryButton = ({children}) => 
<button className={style.btnPrimary}>{children}</button>

const SecondaryButton = ({children}) => 
<button className={style.btnSecondary}>{children}</button>

const SuccessButton = ({children}) => 
<button className={style.btnSuccess}>{children}</button>

// const PrimaryButton = ({children}) => 
// <Button className = {style.btnPrimary}>{children}</Button>

// const SecondaryButton = ({children}) => 
// <Button className = {style.btnSecondary}>{children}</Button>
    
// const SuccessButton = ({children}) => 
// <Button className = {style.btnSuccess} >{children}</Button>
    

export {PrimaryButton, SecondaryButton, SuccessButton}