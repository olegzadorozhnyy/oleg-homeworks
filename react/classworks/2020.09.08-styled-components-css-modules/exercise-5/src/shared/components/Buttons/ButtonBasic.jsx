import React from 'react';
import style from './Buttons.module.css'

const Button = ({className='', children}) => {
    let fullClassName = `${style.btn} ${className}`
    return (
    <button className={fullClassName}>{children}</button>
    )
}

export default Button;