import React from 'react';
import './App.css';

import Input from "./shared/components/Input";

import LoginForm from "./clients/LoginForm/components/LoginForm";

function App() {
    return (
        <div className="App">
            <LoginForm />
            <Input />
        </div>
    );
}

export default App;
