import React from 'react';
import "./Buttons.css";

const ButtonBasic = ({className, children}) => {
    return <button className={`btn ${className}`}>{children}</button>
}

export default ButtonBasic;