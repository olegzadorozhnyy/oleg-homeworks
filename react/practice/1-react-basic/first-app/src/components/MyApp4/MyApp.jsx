import React, {Component} from "react";

class MyApp extends Component {
    constructor() {
        super()
        this.state = {
            isLoggedIn: true,
        }
    }
    render() {
        const lastWord = (this.state.isLoggedIn) ? "in" : "out"
        return (
            <div>
                <h1>You are currently logged {lastWord}</h1>
            </div>
        )
    }
}

export default MyApp