import React from "react";
import {BrowserRouter as Router, Link, Route, Switch, Redirect} from "react-router-dom"
import "./Routing2.css"

const NoMatchPage = () => {return (<h3>404 - Not found</h3>);};

const SearchPage = ({location }) => {

    return (
        <>
            <h3>Search Page</h3>
            <p>
                <strong>Query Params: </strong>
                {location.search}
            </p>
        </>
            );
}

const IndexPage = () => { return (<h3>Index Page</h3>); }

const PropsPage = ({title}) => {  return (
    <>
        <h3>{title}</h3>
    </>
);
};

const RedirectPage = () => {
    return (
        <h3>Redirect Page</h3>
    );
};


const Routing2 = () => {

    return (
        <section className="App">
            <Router>
                <Link to="/">Home</Link>
                <Link to="/search?q=react">Search</Link>
                <Link to="/props">Passing Props</Link>
                <Link to="/old-route">Redirecting to New page</Link>
                <Switch>
                    <Redirect from="/old-route" to="/new-route" />
                    <Route exact path="/new-route" component={RedirectPage} />
                    <Route exact path="/" component={IndexPage} />
                    <Route exact path="/search" component={SearchPage} />
                    {/*<Route exact path="/props" component={() => <PropsPage title={'Props through component'}/>} />*/}
                    <Route exact path="/props" render={(props) => <PropsPage {...props} title={`Props through render`} />} />
                    <Route component={NoMatchPage} />
                </Switch>
            </Router>
        </section>
    );
}

export default Routing2