import {Link} from "react-router-dom";
import React from "react";

const UsersPage = ({ match, location }) => {
    const { params: { userId } } = match;
    const users = [
        {
            name: `Param`,
        },
        {
            name: `Vennila`,
        },
        {
            name: `Afrin`,
        },
    ];

    return (
        <>
            <h3>Users Page</h3>
            {users.map((user, index) => (
                <h5 key={index}>
                    <Link to={`/user/${index + 1}`}>{user.name}'s Page</Link>
                </h5>
            ))}
        </>
    );
};
export default UsersPage