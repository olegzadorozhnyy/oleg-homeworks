import React from 'react';
import {Link, Route} from "react-router-dom"
import UsersPage from "./components/UsersPage";
import UserPage from "./components/UsersPage/component/UserPage";
import AboutPage from "./components/AboutPage";
import IndexPage from "./components/IndexPage"


const Roting = () => {
    return (
        <>
            <Link to="/">Home</Link>
            <Link to="/about">About</Link>
            <Link to="/users">Users</Link>
            <Route exact path="/" component={IndexPage} />
            <Route exact path="/users" component={UsersPage} />
            <Route exact path="/user/:userId" component={UserPage} />
            <Route exact path="/about" component={AboutPage} />
        </>
    );
}

export default Roting