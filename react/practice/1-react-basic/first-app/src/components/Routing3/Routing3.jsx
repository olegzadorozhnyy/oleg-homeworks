import React from "react";
import {BrowserRouter as Router, Link, Route} from "react-router-dom"
import {users} from "./Users"


const UserPage = ({ match }) => {
    console.log(match);
    const { params: { userName }, } = match;
    console.log(userName);

    const user = users.find(({ name }) => name === userName);
    console.log(user);

    return (
        <div>
            User Name: <strong>{user.name}</strong>
            <p>{user.description}</p>
        </div>
    );
};

const Routig3 = () => {
    return (
        <Router>
            <h3>Top level routes</h3>
            <ul className="unlist">
                {users.map((user, index) => {
                    return (
                        <li key={index}>
                            <Link to={`/user/${user.name}`}>{user.name}</Link>
                        </li>
                    );
                })}
            </ul>
            <Route path="/user/:userName" component={UserPage} />
        </Router>
    )
}

export default Routig3