import React, {Component} from "react";
import Header from "./Header";
import Greeting from "./Greeting";

class MyApp extends Component {
    render() {
        const headerProps = {
            username : "Oleh"
        }
        return (
            <div>
                <Header {...headerProps}/>
                <Greeting />
            </div>
        )
    }
}

export default MyApp