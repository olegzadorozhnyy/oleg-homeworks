import React, {Component} from "react";

class Greeting extends Component {

    timeday() {
        let timeOfDay
        const date = new Date()
        const hours = date.getHours()
        if (hours < 12) {
            timeOfDay = "morning"
        } else if (hours >= 12 && hours < 17) {
            timeOfDay = "afternoon"
        } else {
            timeOfDay = "night"
        }
        return timeOfDay
    }
    render() {
        return (
            <h1>Good {this.timeday()} to you, sir or madam!</h1>
        )
    }
}

export default Greeting

