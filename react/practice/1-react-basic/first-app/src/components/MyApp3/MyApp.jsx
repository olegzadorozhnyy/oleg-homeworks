import React, {Component} from "react";

class MyApp extends Component {
    constructor() {
        super()
        this.state = {
            age: 34,
            name: "Oleh"
        }
    }
    render() {
        return (
            <div>
                <h1>{this.state.name}</h1>
                <h3><font color="#3AC1EF">▍{this.state.age} years old</font></h3>
            </div>
        )
    }
}

export default MyApp