import React from "react";
import Routing3 from "./components/Routing3";


const App = () => {

    return (
        <section className="App">
                <Routing3 />
        </section>
    );
};

export default App;